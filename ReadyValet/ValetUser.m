//
//  Vehicle.m
//  Valet
//
//

#import "ValetUser.h"

@implementation ValetUser

- (id) initWithName:(NSString*)first_name last_name:(NSString*) last_name {
    self = [super init];
    self.first_name = first_name;
    self.last_name = last_name;
    return self;
}
@end
