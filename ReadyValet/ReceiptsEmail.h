//
//  ReceiptsEmail.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/15/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptsEmail : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)touchSend:(id)sender;
@property (strong, nonatomic) NSString *trans_id;

@end
