//
//  CreateAccountView2.m
//  ReadyValet
//
//  Created by Scott Leathers on 8/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "CreateAccountView3.h"
#import "BSKeyboardControls.h"
#import "ValetStorage.h"
#import "AccountProfile.h"
#import "UtilFunctions.h"

@interface CreateAccountView3 ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end

@implementation CreateAccountView3

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"NEXT" style:UIBarButtonItemStylePlain target:self action:@selector(didTapDone)];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = rightBarButton;

    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.make.leftView = paddingView;
    self.make.leftViewMode = UITextFieldViewModeAlways;
    paddingView = nil;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.model.leftView = paddingView2;
    self.model.leftViewMode = UITextFieldViewModeAlways;
    paddingView2 = nil;
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.color.leftView = paddingView4;
    self.color.leftViewMode = UITextFieldViewModeAlways;
    paddingView4 = nil;
    
    [self.make setHidden:TRUE];
    [self.model setHidden:TRUE];
    [self.color setHidden:TRUE];
    
    self.scrollView.scrollEnabled = TRUE;
    self.scrollView.contentSize = CGSizeMake(320, 568);
    
    self.make.text = [ValetStorage sharedModel].currentVehicle.vehicleManufacturer;
    self.model.text = [ValetStorage sharedModel].currentVehicle.vehicleModel;
    self.color.text = [ValetStorage sharedModel].currentVehicle.vehicleColor;

    
    NSArray *fields = @[self.make, self.model, self.license];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
}

- (void)didTapDone {
    if ([self.make.text length] <= 0) {
        NSString *msg = @"Invalid vehicle make. Please try again.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.model.text length] <= 0) {
        NSString *msg = @"Invalid vehicle model. Please try again.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.color.text length] <= 0) {
        NSString *msg = @"Invalid vehicle color. Please try again.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [AccountProfile instance].make  = self.make.text;
    [AccountProfile instance].model  = self.model.text;
    [AccountProfile instance].color  = self.color.text;
    //[AccountProfile instance].license  = self.license.text;
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //self.rightBarButton.enabled = FALSE;
}

- (IBAction) cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:FALSE];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.keyboardControls setActiveField:textField];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:TRUE];
}

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view = keyboardControls.activeField.superview.superview;
    [self.scrollView scrollRectToVisible:view.frame animated:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];//Dismiss the keyboard.
    
    //Add action you want to call here.
    return YES;
}


- (void)textFieldChangedAction:(UITextField *)textField {
}

-(void) dealloc {
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchSettings:(id)sender {
    if ([self.make.text length] <= 0) {
        NSString *msg = @"Invalid vehicle make. Please try again.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.model.text length] <= 0) {
        NSString *msg = @"Invalid vehicle model. Please try again.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.color.text length] <= 0) {
        NSString *msg = @"Invalid vehicle color. Please try again.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [AccountProfile instance].make  = self.make.text;
    [AccountProfile instance].model  = self.model.text;
    [AccountProfile instance].color  = self.color.text;
    //[AccountProfile instance].license  = self.license.text;
}
- (IBAction)touchMake:(id)sender {
}

- (IBAction)touchModel:(id)sender {
}

- (IBAction)touchColor:(id)sender {
}
@end
