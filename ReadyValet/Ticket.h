//
//  Ticket.h
//  Valet
//
//  Created by Matthew Schmidgall on 2/17/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vehicle.h"
#import "Driver.h"

@interface Ticket : NSObject

@property (strong,nonatomic) NSString *ticketNumber;
@property (strong,nonatomic) NSString *status;
@property (strong,nonatomic) Vehicle *vehicle;
@property (strong,nonatomic) Driver *driver;

- (id) initWithTicket:(NSString*)ticketId status:(NSString*)status vehicle:(Vehicle*)vehicle;
- (id) initWithTicket:(NSString*)ticketId status:(NSString*)status vehicle:(Vehicle*)vehicle driver:(Driver*)driver;

@end
