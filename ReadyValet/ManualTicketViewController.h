//
//  ManualTicketViewController.h
//  ReadyValet
//
//  Created by Scott Leathers on 12/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vehicle.h"

@interface ManualTicketViewController : UIViewController {
}

@property (strong, nonatomic) IBOutlet UILabel *title1;
@property (strong, nonatomic) IBOutlet UILabel *hikerTicketNumer;
@property (strong, nonatomic) IBOutlet UITextField *paperTicketNumber;

@property (strong, nonatomic) NSString *ticketNumber;

@end
