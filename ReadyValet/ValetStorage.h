//
//  ValetStorage.h
//  Valet
//
//  Created by Matthew Schmidgall on 1/4/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import "Models.h"
#import <Foundation/Foundation.h>


@interface ValetStorage : NSObject


@property (nonatomic,strong) Driver *currentDriver;
@property (nonatomic,strong) ValetOwner *valetOwner;


@property (nonatomic,strong) NSString *username;
@property (nonatomic,strong) NSString *password;
@property (nonatomic,strong) NSString *beaconUuid;
@property (nonatomic,strong) NSString *profileId;
@property (nonatomic,strong) NSString *role;

@property (strong, nonatomic) NSMutableDictionary *driverImages;

@property (nonatomic,strong) NSMutableDictionary *ticketQueues;
@property (nonatomic,strong) NSMutableArray *vehiclesInRange;

@property (nonatomic,strong) NSMutableArray *vehiclesCompleted;
@property (nonatomic,strong) NSMutableArray *vehiclesRequested;
@property (nonatomic,strong) NSMutableArray *vehiclesStored;
@property (nonatomic,strong) NSString *location;

@property (nonatomic,strong) NSString *currentValetId;

@property (nonatomic,strong) NSString *currentVehicleId;
@property (nonatomic,strong) NSString *currentTicketNumber;

@property (nonatomic,strong) NSString *currentVehicleName;
@property (nonatomic,strong) NSString *currentValetName;
@property (nonatomic,strong) NSString *currentValetFee;
@property (nonatomic,strong) NSString *currentServiceFee;
@property (nonatomic,strong) NSString *currentMerchantId;
@property (nonatomic,strong) NSString *currentMerchantStatus;

@property (strong,nonatomic) UIImage *currentTicketImage;
@property NSInteger currentVehicleStatus;
@property NSInteger autoDetect;


@property (nonatomic,strong) Vehicle* currentVehicle;
@property (nonatomic,strong) NSString *valetLocation;
@property (nonatomic,strong) NSDate *dateRequested;

- (void) saveLogin;
- (BOOL) loadLogin;

+ (ValetStorage*)sharedModel;

@end
