//
//  MakeSearch.m
//
//  Created by Scott Leathers on 7/25/13.
//  Copyright (c) 2013 Scott Leathers. All rights reserved.
//

#import "MakeSearch.h"
#import "MakeSearchCells.h"
#import "ModelSearch.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"

@interface MakeSearch ()

@end

@implementation MakeSearch

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    [self.myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.searchDisplayController.searchResultsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    isSearching = NO;
    filteredList = [[NSMutableArray alloc] init];
    
    NSString *destPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    destPath = [destPath stringByAppendingPathComponent:@"makesmodels.plist"];
    
    items = [[NSMutableArray alloc] initWithContentsOfFile:destPath];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    [self.myTable setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
}

- (void) cancel {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelNewVehicle" object:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (isSearching) {
        //If the user is searching, use the list in our filteredList array.
        return [filteredList count];
    } else {
        return [items count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MakeSearchCells";
    MakeSearchCells *cell = (MakeSearchCells *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MakeSearchCells" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor colorWithRed:(237/255.0) green:(237/255.0) blue:(242/255.0) alpha:1];
  
    // Configure the cell...
    if (isSearching && [filteredList count]) {
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", (NSString*)[[filteredList objectAtIndex:indexPath.row] objectForKey:@"makeName"]];
    } else {
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", (NSString*)[[items objectAtIndex:indexPath.row] objectForKey:@"makeName"]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *makeName;
    if (isSearching && [filteredList count]) {
        makeName = [NSString stringWithFormat:@"%@", (NSString*)[[filteredList objectAtIndex:indexPath.row] objectForKey:@"makeName"]];
    } else {
        makeName = [NSString stringWithFormat:@"%@", (NSString*)[[items objectAtIndex:indexPath.row] objectForKey:@"makeName"]];
    }
    
    NSInteger count = 0;
    for (NSDictionary *makeDict in items) {
        if ([[makeDict objectForKey:@"makeName"] isEqualToString:makeName]) {
            break;
        }
        count = count + 1;
    }

    [AccountProfile instance].make  = makeName;
    
    ModelSearch *ms = [[ModelSearch alloc] initWithNibName:@"ModelSearch" index:count];
    [self.navigationController pushViewController:ms animated:FALSE];
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"Can find the model of your car? Touch Here." forState:UIControlStateNormal];
    
    //NIKFontAwesomeIcon icon = NIKFontAwesomeIconCheck;
    //[button setImage:[self.factory createImageForIcon:icon] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    button.frame = CGRectMake(15, 0, tableView.frame.size.width, 44);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [view addSubview:button];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}*/

- (void)filterListForSearchText:(NSString *)searchText
{
    filteredList = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in items) {
        NSRange nameRange = [[dict objectForKey:@"makeName"] rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (nameRange.location != NSNotFound) {
            [filteredList addObject:dict];
        }
    }
}

#pragma mark - UISearchDisplayControllerDelegate

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
    //When the user taps the search bar, this means that the controller will begin searching.
    isSearching = YES;
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller {
    //When the user taps the Cancel Button, or anywhere aside from the view.
    isSearching = NO;
    [filteredList removeAllObjects];
    filteredList = nil;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterListForSearchText:searchString]; // The method we made in step 7
    
    
    [controller.searchResultsTableView setBackgroundColor:[UIColor colorWithRed:(37/255.0) green:(36/255.0) blue:(36/255.0) alpha:1]];
    controller.searchResultsTableView.bounces=FALSE;
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterListForSearchText:[self.searchDisplayController.searchBar text]]; // The method we made in step 7
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
