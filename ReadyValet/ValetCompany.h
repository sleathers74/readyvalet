//
//

#import <Foundation/Foundation.h>

@interface ValetCompany : NSObject

@property (strong,nonatomic) NSString *valet_id;
@property (strong,nonatomic) NSString *owner_id;
@property (strong,nonatomic) UIImage *profile_image;
@property (strong,nonatomic) NSString *first_name;
@property (strong,nonatomic) NSString *last_name;
@property (strong,nonatomic) NSString *company;
@property (strong,nonatomic) NSString *dbaName;
@property (strong,nonatomic) NSString *email;
@property (strong,nonatomic) NSString *password;
@property (strong,nonatomic) NSString *phone;
@property (strong,nonatomic) NSString *valet_fee;
@property (strong,nonatomic) NSString *service_fee;
@property (strong,nonatomic) NSString *address;
@property (strong,nonatomic) NSString *address2;
@property (strong,nonatomic) NSString *city;
@property (strong,nonatomic) NSString *state;
@property (strong,nonatomic) NSString *zip;
@property (strong,nonatomic) NSString *contact_info;
@property (strong,nonatomic) NSString *merchant_id;
@property (strong,nonatomic) NSString *merchant_status;
@property (strong,nonatomic) NSString *account_number;
@property (strong,nonatomic) NSString *routing_number;
@property (strong,nonatomic) NSString *tax_id;
@property (strong,nonatomic) NSString *terms;


- (id) initWithName:(NSString*)company_name;
- (void) StringToImage:(NSString*) imageString;
- (NSString*) ImageToString;

@end
