//
//  Driver.h
//  Valet
//
//  Created by Matthew Schmidgall on 2/15/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Driver : NSObject

@property (strong,nonatomic) NSString *emailAddress;
@property (strong,nonatomic) NSString *password;

@property (strong,nonatomic) NSString *firstName;
@property (strong,nonatomic) NSString *lastName;
@property (strong,nonatomic) NSString *phone;
@property (strong,nonatomic) NSString *defaultTip;

@property (strong,nonatomic) NSString *creditCardNumber;
@property (strong,nonatomic) NSString *creditCardExpiration;
@property (strong,nonatomic) NSString *creditCardSecurityCode;
@property (strong, nonatomic)  UIImage               *driverImage;


@property (strong,nonatomic) NSMutableArray *vehicles;


@end
