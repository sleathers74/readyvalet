//
//  InLotVC.h
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InLotVC : UIViewController {
     UIBarButtonItem         *logoutButton;
}
@property (strong, nonatomic) IBOutlet UITableView *mytableView;
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) NSMutableArray *listDetails;
@property (strong, nonatomic) IBOutlet UILabel *emptyLabel;
@property (strong, nonatomic) UILabel *headerLabel;
@end
