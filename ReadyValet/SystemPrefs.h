//
//
//  Created by Scott Leathers on 3/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

@interface SystemPrefs:NSObject {
    NSString				*url;
    NSString                *full_url;
    NSString                *secureUrl;
}

+(SystemPrefs *)instance;
-(id)init;

@property (nonatomic, strong) NSString   			*url;
@property (nonatomic, strong) NSString   			*full_url;
@property (nonatomic, strong) NSString   			*secureUrl;

-(NSString*) getURL;
- (void) sendGoogleView:(NSString*) title;
- (void) sendGoogleEvent:(NSString*) category action:(NSString*) action label:(NSString*) label;

@end