//
//  UserProfile.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/8/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetUserProfile.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "ValetStorage.h"
#import "Driver.h"
#import "AccountProfile.h"
#import "SystemPrefs.h"

@interface ValetUserProfile ()

@end

@implementation ValetUserProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:TRUE];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePhoto:)
                                                 name:@"updatePhoto"
                                               object:nil];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SAVE" style:UIBarButtonItemStylePlain target:self action:@selector(save)];

    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.firstTextfield.leftView = paddingView;
    self.firstTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView = nil;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.lastTextfield.leftView = paddingView1;
    self.lastTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView1 = nil;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.usernameTextfield.leftView = paddingView2;
    self.usernameTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView2 = nil;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.passwordTextfield.leftView = paddingView3;
    self.passwordTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView3 = nil;
    
    [self loadData];
}

-(void) loadData {
    if (self.valetUser) {
        self.firstTextfield.text = self.valetUser.first_name;
        self.lastTextfield.text = self.valetUser.last_name;
        self.usernameTextfield.text = self.valetUser.username;
        self.passwordTextfield.text = self.valetUser.password;
        
        if (self.valetUser.profile_image)
            self.userImage.image = self.valetUser.profile_image;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE]; 
}

-(void)save {
    [self.waitView setHidden:FALSE];
    if ([UtilFunctions NSStringIsValidEmail:self.usernameTextfield.text]) {
        [self validateUsername];
    }
    else {
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:@"Invalid email address."];
    }
}

-(void) processFinal {
    if ([self.usernameTextfield.text length] <= 0) {
        NSString *msg = @"Username must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.passwordTextfield.text length] <= 0) {
        NSString *msg = @"Password must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.firstTextfield.text length] <= 0) {
        NSString *msg = @"First name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.lastTextfield.text length] <= 0) {
        NSString *msg = @"Last name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    
    [self updateUser];
}

- (IBAction)touchPhoto:(id)sender {
    [self displayChooserActionSheeet];
    /*Camera *cam = [[Camera alloc] initWithNibName:@"Camera" bundle:nil];
    [self.navigationController pushViewController:cam animated:TRUE];*/
}

-(void) updateUser {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    //NSString *username = self.valetUser.username;
    //NSString *password = self.valetUser.password;
    NSString *newusername = self.usernameTextfield.text;
    NSString *newpassword = self.passwordTextfield.text;
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     newusername, @"username",
                                     newpassword, @"password",
                                     self.firstTextfield.text, @"first_name",
                                     self.lastTextfield.text, @"last_name",
                                     nil];
    
    if (self.userImage.image) {
        NSData *imageData = UIImageJPEGRepresentation(self.userImage.image, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"profile_image"];
    }
    
    if (self.valetCompany) {
        [postData setObject:self.valetCompany.valet_id forKey:@"valet_id"];
    }
    
    NSString *urlString;
    if (self.valetUser) {
        urlString = [NSString stringWithFormat:@"%@updateuser/%@", [SystemPrefs instance].getURL, self.valetUser.user_id];
    }
    else {
        urlString = [NSString stringWithFormat:@"%@adduser", [SystemPrefs instance].getURL];
    }
    
    [self.apiManager PUT:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
        NSLog(@"JSON: %@", responseObject);
#endif
        
        [self.waitView setHidden:TRUE];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"profileUpdated" object:nil];
        
        NSString *msg = @"This account has been updated.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Account Update" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.delegate = self;
        alert.tag = 10;
        [alert show];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.waitView setHidden:TRUE];
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

- (void) updatePhoto:(NSNotification *) notification {
    [self.editLabel setHidden:TRUE];
    UIImage *image = (UIImage*)notification.object;
    self.userImage.image = image;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Account Updated
    if (alertView.tag == 10) {
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

-(bool) validateUsername {
    
    NSString *username = @"";
    if (self.valetUser)
        username = self.valetUser.username;
    
    if ([username length] > 0 && ![username isEqualToString:self.usernameTextfield.text]) {
        self.apiManager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         self.usernameTextfield.text, @"username", nil];
        NSString *urlString = [NSString stringWithFormat:@"%@/checkUsername", [[SystemPrefs instance] full_url]];
        [self.apiManager POST:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            //[self.waitView setHidden:TRUE];
            if ([[responseObject objectForKey:@"username_exists"] intValue] == 1) {
                NSString *msg = @"The email address you have chosen already exists in our system. Please choose a different email address.";
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                return;
            }
            [self processFinal];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [self.waitView setHidden:TRUE];
            
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }];
    }
    else {
        [self processFinal];
    }
    return true;
}

// Photo Chooser
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.navigationItem.rightBarButtonItem.title= @"SUBMIT";
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (chosenImage == nil)
        chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.userImage.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void) displayChooserActionSheeet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Photo Selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        [actionSheet addButtonWithTitle:@"Camera"];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        [actionSheet addButtonWithTitle:@"Photo Library"];
    
    actionSheet.tag = 0;
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerControllerSourceType srcType;
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Camera"]) {
        srcType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Photo Library"]) {
        srcType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self openPhotoPicker:srcType];
    }];
}

-(void) openPhotoPicker:(UIImagePickerControllerSourceType) sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.mediaTypes = @[(NSString *)kUTTypeImage];
    picker.sourceType = sourceType;
    
    [self presentViewController:picker animated:YES completion:NULL];
}



@end
