//
//  ColorSearch
//
//  Created by Scott Leathers on 7/25/13.
//  Copyright (c) 2013 Scott Leathers. All rights reserved.
//

#import "ColorSearch.h"
#import "MakeSearchCells.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "LicensePhoto.h"

@interface ColorSearch ()

@end

@implementation ColorSearch

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];

    [self.myTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.searchDisplayController.searchResultsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    isSearching = NO;
    filteredList = [[NSMutableArray alloc] init];
    
    items = [NSArray arrayWithObjects:@"Black", @"Blue", @"Brown/Beige", @"Green", @"Red", @"Silver/Grey", @"White", @"Yellow/Gold", @"Other", nil];
}

- (void) next {
    
}

- (void) cancel {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelNewVehicle" object:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (isSearching) {
        //If the user is searching, use the list in our filteredList array.
        return [filteredList count];
    } else {
        return [items count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MakeSearchCells";
    MakeSearchCells *cell = (MakeSearchCells *)[aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MakeSearchCells" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor colorWithRed:(237/255.0) green:(237/255.0) blue:(242/255.0) alpha:1];
  
    // Configure the cell...
    if (isSearching && [filteredList count]) {
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", (NSString*)[filteredList objectAtIndex:indexPath.row]];
    } else {
        cell.descriptionLabel.text = [NSString stringWithFormat:@"%@", (NSString*)[items objectAtIndex:indexPath.row]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [AccountProfile instance].color = [items objectAtIndex:indexPath.row];
    
    LicensePhoto *lp = [[LicensePhoto alloc] initWithNibName:@"LicensePhoto" bundle:nil];
    [self.navigationController pushViewController:lp animated:FALSE];
}

- (void)filterListForSearchText:(NSString *)searchText
{
    filteredList = [[NSMutableArray alloc] init];
    
    for (NSString *modelName in items) {
        NSRange nameRange = [modelName rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (nameRange.location != NSNotFound) {
            [filteredList addObject:modelName];
        }
    }
}

#pragma mark - UISearchDisplayControllerDelegate

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
    //When the user taps the search bar, this means that the controller will begin searching.
    isSearching = YES;
}

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller {
    //When the user taps the Cancel Button, or anywhere aside from the view.
    isSearching = NO;
    [filteredList removeAllObjects];
    filteredList = nil;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterListForSearchText:searchString]; // The method we made in step 7
    
    
    [controller.searchResultsTableView setBackgroundColor:[UIColor colorWithRed:(37/255.0) green:(36/255.0) blue:(36/255.0) alpha:1]];
    controller.searchResultsTableView.bounces=FALSE;
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterListForSearchText:[self.searchDisplayController.searchBar text]]; // The method we made in step 7
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
