//
//  LoginViewCell.m
//  IllinoisPrepScores
//
//  Created by Scott Leathers on 7/31/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetCell.h"

@implementation ValetCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
