//
//  Models.h
//  Valet
//
//  Created by Matthew Schmidgall on 2/15/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#ifndef Valet_Models_h
#define Valet_Models_h

#import "Vehicle.h"
#import "Driver.h"
#import "Ticket.h"
#import "ValetOwner.h"

#endif
