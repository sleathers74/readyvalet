//
//  MainValetViewTVC.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "MainValetViewTVC.h"
#import "CheckInVCViewController.h"
#import "InLotVC.h"
#import "RequestedVC.h"
#import "RetrievedVC.h"
#import "CompletedVC.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import <AudioToolbox/AudioToolbox.h>
#import "SystemPrefs.h"

@interface MainValetViewTVC ()

@end

@implementation MainValetViewTVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [UIApplication sharedApplication].idleTimerDisabled = true;
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed: 0/255.0 green: 34.0/255.0 blue: 85.0/255.0 alpha: 1]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBackgroundColor:[UIColor colorWithRed: 0/255.0 green: 34.0/255.0 blue: 85.0/255.0 alpha: 1]];
    //[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:12.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (!self.refreshTimer) {
        self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(refresh) userInfo:nil repeats:YES];
        [self refresh];
    }
    
    self.delegate = self;
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed: 0/255.0 green: 34.0/255.0 blue: 85.0/255.0 alpha: 1]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBackgroundColor:[UIColor colorWithRed: 0/255.0 green: 34.0/255.0 blue: 85.0/255.0 alpha: 1]];
    
    CheckInVCViewController *checkinVC = [[CheckInVCViewController alloc] initWithNibName:@"CheckInVCViewController" bundle:nil];
    checkinVC.title = @"Check-In";
    //checkinVC.tabBarItem.image = [UIImage imageNamed:@"checkin.png"];
    
    UINavigationController *checkINnavVC = [[UINavigationController alloc] initWithRootViewController:checkinVC];
    
    InLotVC *LotVC = [[InLotVC alloc] initWithNibName:@"InLotVC" bundle:nil];
    LotVC.title = @"In-Lot";
    //LotVC.tabBarItem.image = [UIImage imageNamed:@"inlot.png"];
    
    UINavigationController *inLotNav = [[UINavigationController alloc] initWithRootViewController:LotVC];
    
    RequestedVC *requested = [[RequestedVC alloc] initWithNibName:@"RequestedVC" bundle:nil];
    requested.title = @"Requested";
    //requested.tabBarItem.image = [UIImage imageNamed:@"requested.png"];
    
    UINavigationController *requestedNVC = [[UINavigationController alloc] initWithRootViewController:requested];
    
    RetrievedVC *retrieved = [[RetrievedVC alloc] initWithNibName:@"RetrievedVC" bundle:nil];
    retrieved.title = @"Retrieved";
    //completed.tabBarItem.image = [UIImage imageNamed:@"requested.png"];
    
    UINavigationController *retrievedNVC = [[UINavigationController alloc] initWithRootViewController:retrieved];
    
    CompletedVC *completed = [[CompletedVC alloc] initWithNibName:@"CompletedVC" bundle:nil];
    completed.title = @"Completed";
    //completed.tabBarItem.image = [UIImage imageNamed:@"requested.png"];
    
    UINavigationController *completedNVC = [[UINavigationController alloc] initWithRootViewController:completed];

    [self setViewControllers:[NSArray arrayWithObjects:checkINnavVC, inLotNav, requestedNVC, retrievedNVC, completedNVC, nil]];
    self.delegate = self;
    
    NSUUID *myUUID = [[NSUUID alloc] initWithUUIDString:@"033C7CC8-2352-48F1-81CD-919924AF0CE4"]; //@"3EE147AE-A4D6-4B13-8EA1-B93F110BB991"];
    
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:myUUID
                                                                  major:[[ValetStorage sharedModel].profileId integerValue]
                                                             identifier:@"com.valet.stands"];
    self.myBeaconData = [self.myBeaconRegion peripheralDataWithMeasuredPower:nil];
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self
                                                                     queue:nil
                                                                   options:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.refreshTimer) {
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
    }
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        [self.peripheralManager startAdvertising:self.myBeaconData];
    } else if (peripheral.state == CBPeripheralManagerStatePoweredOff) {
        [self.peripheralManager stopAdvertising];
    } else if (peripheral.state == CBPeripheralManagerStateUnsupported) {
        NSLog(@"Bluetooth not supported");
        
        //self.msg.text = @"Bluetooth not supported";
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    // Loggout
    /*if ([tabBarController selectedIndex] == 4) {
        [ValetStorage sharedModel].currentDriver = nil;
        [ValetStorage sharedModel].role = nil;
        [ValetStorage sharedModel].password = nil;
        [[ValetStorage sharedModel] saveLogin];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
    }*/
}

- (void) refresh {
    [self updateQueues];
    [self updateVehiclesInRange];
}

- (void) updateQueues {
    // Ticket Queues
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@queue", [[SystemPrefs instance] getURL]];
    [self.apiManager GET:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [[ValetStorage sharedModel].ticketQueues removeAllObjects];
        
        
        if ([responseObject count] > 0) {
            long requestedBadge = 0;

            long totalCount = [responseObject count];
            for (long icount = 1; icount <= totalCount; icount++) {
                NSDictionary *ticket = [responseObject objectForKey:[NSString stringWithFormat:@"%ld", icount]];
                NSString *status = [ticket objectForKey:@"status"];
                NSString *ticket_id = [ticket objectForKey:@"ticket_id"];
                if ([status isEqualToString:@"2"])
                    requestedBadge += 1;
                
                if (![[ValetStorage sharedModel].ticketQueues objectForKey:status]) {
                    [[ValetStorage sharedModel].ticketQueues setObject:[[NSMutableDictionary alloc] init] forKey:status];
                }
                
                Vehicle *v = [[Vehicle alloc] initWithVehicle:[ticket objectForKey:@"make"]
                                                        model:[ticket objectForKey:@"model"]
                                                        color:[ticket objectForKey:@"color"]];
                v.vehicleId = [ticket objectForKey:@"vehicle_id"];
                v.license = [ticket objectForKey:@"license"];
                NSString *licenseImage = [ticket objectForKey:@"licenseImage"];
                if ([licenseImage length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:licenseImage options:0];
                    v.licenseImage = [UIImage imageWithData:imageData];
                }
                
                Driver *d = [[Driver alloc] init];
                d.firstName = [ticket objectForKey:@"firstname"];
                d.lastName = [ticket objectForKey:@"lastname"];
                d.emailAddress = [ticket objectForKey:@"email"];
                d.phone = [ticket objectForKey:@"phone"];
                
                NSMutableDictionary *statusdict = [[ValetStorage sharedModel].ticketQueues objectForKey:status];
                Ticket *tick = [[Ticket alloc] initWithTicket:ticket_id status:status vehicle:v driver:d];
                [statusdict setObject:tick forKey:ticket_id];
            }
            
            if (requestedBadge > 0) { //count) {
                // Ding
                AudioServicesPlaySystemSound(1003);
                [[self.tabBar.items objectAtIndex:2] setBadgeValue:[NSString stringWithFormat:@"%ld", requestedBadge]];
            }
            else if (requestedBadge == 0) {
             [[self.tabBar.items objectAtIndex:2] setBadgeValue:nil];
            }
        }
        else {
            [[ValetStorage sharedModel].ticketQueues removeAllObjects];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTickets" object:self];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];

}

-(void) updateVehiclesInRange {
    self.apiManager = [AFHTTPRequestOperationManager manager];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@range", [[SystemPrefs instance] getURL]];
    [self.apiManager GET:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        //NSUInteger count = [[ValetStorage sharedModel].vehiclesInRange count];
        long vehicleCountBadge = 0;
        
        [[ValetStorage sharedModel].vehiclesInRange removeAllObjects];
        
        for (NSString* vehicleId in responseObject) {
            NSDictionary *subObject = [responseObject objectForKey:vehicleId];
            if ([[subObject objectForKey:@"auto_detect"] intValue] == 1) {
                vehicleCountBadge += 1;
                Vehicle *v = [[Vehicle alloc] initWithVehicle:[subObject objectForKey:@"make"] model:[subObject objectForKey:@"model"] color:[subObject objectForKey:@"color"]];
                v.vehicleId = vehicleId;
                v.driverName = [subObject objectForKey:@"driver_name"];
                
                NSString *licenseImage = [subObject objectForKey:@"licenseImage"];
                if ([licenseImage length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:licenseImage options:0];
                    v.licenseImage = [UIImage imageWithData:imageData];
                }

                [[ValetStorage sharedModel].vehiclesInRange addObject:v];
            }
        }
        
        if (vehicleCountBadge > 0) { //count) {
            // Ding
            AudioServicesPlaySystemSound(1003);
            [[self.tabBar.items objectAtIndex:0] setBadgeValue:[NSString stringWithFormat:@"%ld", vehicleCountBadge]];
        }
        else if (vehicleCountBadge == 0) {
            [[self.tabBar.items objectAtIndex:0] setBadgeValue:nil];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadVehicles" object:self];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc {
    [UIApplication sharedApplication].idleTimerDisabled = false;
}

@end
