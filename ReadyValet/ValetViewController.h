//
//  ValetViewController.h
//  ReadyValet
//
//  Created by Scott Leathers on 7/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "ValetDetection.h"
#import <MapKit/MapKit.h>

@interface ValetViewController : UIViewController  <MKMapViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate> {
    UIBarButtonItem         *settingsButton; 
}
- (IBAction)returnHome:(id)sender;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property NSInteger vehicleStatus; // (0 = not checked in, 1 = checked in)
@property (weak, nonatomic) IBOutlet UILabel *vehicleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketLabel;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UILabel *welcomeText;
@property (weak, nonatomic) IBOutlet UIButton *pickupButton;
@property (weak, nonatomic) IBOutlet UIButton *callCarButton;
@property (strong, nonatomic) IBOutlet UILabel *checkinConf;
@property (strong, nonatomic) ValetDetection *valetDetection;

@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleTicketImage;
@property (weak, nonatomic) NSTimer *refreshTimer;
- (IBAction)pickedUpVehicle:(id)sender;
- (void) updateInfoView:(BOOL)refreshFromDb;
- (void) forceUpdateInfoView;
- (void) requeueInfoViewUpdate;
- (IBAction)touchCallCar:(id)sender;
- (IBAction)touchRegisterPaperTicket:(id)sender;

@end
