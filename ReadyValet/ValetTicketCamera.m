//
//  ValetSelectionVC.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetTicketCamera.h"
#import "ValetStorage.h"
#import "ReviewValetTicket.h"
#import "UtilFunctions.h"
#import "MainProfile.h"
#import "SystemPrefs.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ValetTicketCamera ()

@end

@implementation ValetTicketCamera

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Check-In"];
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    UIBarButtonItem *settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self snapPicture];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    bPicture = true;
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [ValetStorage sharedModel].currentTicketImage = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.ticketImage.image = chosenImage;
    self.ticketImage.hidden = FALSE;
    [self.photoButton setTitle:@"" forState:UIControlStateNormal];
    
}

- (void)snapPicture {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    //    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    [self presentViewController:picker animated:YES completion:NULL];
}



- (IBAction)fakeButton:(id)sender {
    [self snapPicture];
}

- (IBAction)submitTouched:(id)sender {
    if (bPicture) {
        [self generateTicket];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please take a picture of your paper ticket to continue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }

}

- (void) generateTicket {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if ([ValetStorage sharedModel].currentTicketImage != nil) {
        NSData *imageData = UIImageJPEGRepresentation([ValetStorage sharedModel].currentTicketImage, 0.1);
        NSString *baseString = [imageData base64Encoding];
        //params = [[NSDictionary alloc] initWithObjectsAndKeys:baseString, @"image", nil];
        [params setObject:baseString forKey:@"image"];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    [params setObject:username forKey:@"username"];
    [params setObject:password forKey:@"password"];
    
    NSString *url = [NSString stringWithFormat:@"%@ticket/%@/%@", [SystemPrefs instance].getURL, [ValetStorage sharedModel].currentVehicleId, [ValetStorage sharedModel].currentValetId];
    
    [self.apiManager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        [ValetStorage sharedModel].currentTicketNumber = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"ticket_id"]];
        
        [self changeVehicleStatus:1 ticketNum:[ValetStorage sharedModel].currentTicketNumber];
        
        ReviewValetTicket *rvt = [[ReviewValetTicket alloc] initWithNibName:@"ReviewValetTicket" bundle:nil];
        rvt.title = @"";
        [self.navigationController pushViewController:rvt animated:TRUE];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];

}

- (void) changeVehicleStatus:(long) currentStatus ticketNum:(NSString*) ticketNum {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    NSString *url = [NSString stringWithFormat:@"%@ticket/%@/%ld", [SystemPrefs instance].getURL, ticketNum, currentStatus];
    
    [self.apiManager PUT:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        //[self.navigationController popToRootViewControllerAnimated:true];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
        //[self.navigationController popToRootViewControllerAnimated:true];
    }];
}

@end
