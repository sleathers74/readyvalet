//
//  ValetStub.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/16/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ValetStub : UIViewController {
    
}

@property (strong, nonatomic) NSString *paidAmt;
@property (strong, nonatomic) IBOutlet UILabel *carLabel;
@property (strong, nonatomic) IBOutlet UILabel *ticketLabel;
@property (strong, nonatomic) IBOutlet UILabel *paidAmountLabel;
- (IBAction)touchDone:(id)sender;
@end
