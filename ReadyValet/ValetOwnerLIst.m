//
//  VehicleSelection.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetOwnerList.h"
#import "MakeSearch.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "Vehicle.h"
#import "CreateAccountView4.h"
#import "ValetStorage.h"
#import "ValetCompany.h"
#import "ValetUsersList.h"
#import "ValetCompanyProfile.h"
#import "SystemPrefs.h"

@interface ValetOwnerList ()

@end

@implementation ValetOwnerList

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:FALSE];
    
    [self setSelectedCell:0];
    
    self.valetArray = [[NSMutableArray alloc] init];
    
    [self.manageToolbar setBarTintColor:[UIColor colorWithRed: 0.0f/255.0f green:34.0f/255.0f blue:85.0f/255.0f alpha:1.0]];
    [self.manageToolbar setTranslucent:FALSE];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    self.factory.colors = @[[NIKColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    [self.valetTable setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    
    [self setupLayout];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self getValetOwners];
}

- (void) setupLayout {
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];

}

- (void) cancel {
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(__unused UITableView *)tableView numberOfRowsInSection:(__unused NSInteger)section
{
    return (self.valetArray ? [self.valetArray count] + 1 : 1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@" Valet Companies" forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon icon = NIKFontAwesomeIconAutomobile;
    [button setImage:[self.factory createImageForIcon:icon] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    button.frame = CGRectMake(15, 0, tableView.frame.size.width, 44);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [view addSubview:button];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *paymentMethodCellIdentifier = @"paymentMethodCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:paymentMethodCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:paymentMethodCellIdentifier];
    }
    cell.backgroundColor = [UIColor whiteColor];
    if ([self.valetArray count] == 0 || indexPath.row > [self.valetArray count] - 1) {
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:@"Add Valet Company"];
        [typeWithDescription addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f] range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;

        NIKFontAwesomeIcon icon = NIKFontAwesomeIconPlus;
        UIImage *image = [self.factory createImageForIcon:icon];
        cell.imageView.contentMode = UIViewContentModeCenter;
        cell.imageView.image = image;
        cell.accessoryView = nil;
    }
    else {
        ValetCompany *v = [self.valetArray objectAtIndex:indexPath.row];
        cell.accessoryView = nil;
        
        NSString *vString = [NSString stringWithFormat:@"%@", v.company];
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:vString];
        [typeWithDescription addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f] range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;
        cell.imageView.image = nil;
    }
    
    if (indexPath.row == self.selectedCell) {
        NIKFontAwesomeIcon icon = NIKFontAwesomeIconCheck;
        UIImage *image = [self.factory createImageForIcon:icon];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        button.frame = frame;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        cell.accessoryView = button;
    }
    else {
        cell.accessoryView = nil;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(__unused UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.valetArray count] > 0 && indexPath.row <= ([self.valetArray count] - 1)) {
        [self.editProfileBarButton setEnabled:TRUE];
        [self.manageUsersBarButton setEnabled:TRUE];
        [self.valetTable reloadData];
        self.selectedCell = indexPath.row;
    }
    else {
        ValetCompanyProfile *vcp = [[ValetCompanyProfile alloc] initWithNibName:@"ValetCompanyProfile" bundle:nil];
        [self.navigationController pushViewController:vcp animated:TRUE];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (indexPath.row > [self.valetArray count])
        return NO;
    else
        return YES;
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
}*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteValet:indexPath];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        ValetCompany *v = [self.valetArray objectAtIndex:actionSheet.tag];
        [self removeValet:v.valet_id];
        [self.valetArray removeObjectAtIndex:actionSheet.tag];
        [self.valetTable reloadData];
    }
}

-(void) getValetOwners {
        self.apiManager = [AFHTTPRequestOperationManager manager];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *username = [defaults stringForKey:@"username"];
        NSString *password = [defaults stringForKey:@"password"];
        
        NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  username, @"username",
                                  password, @"password",
                                  nil];
    
        NSString *url = [NSString stringWithFormat:@"%@getValets/%@", [SystemPrefs instance].getURL, [ValetStorage sharedModel].valetOwner.owner_id];
        [self.apiManager POST:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            [self.valetArray removeAllObjects];
            
            for (NSString *valetid in responseObject) {
                
                NSDictionary *valet = [responseObject objectForKey:valetid];
                ValetCompany *v = [[ValetCompany alloc] initWithName:[valet objectForKey:@"company"]];
                
                [v setValet_id:[valet objectForKey:@"valet_id"]];
                [v setOwner_id:[valet objectForKey:@"owner_id"]];
                
                // This function call will set the profile image.
                [v StringToImage:[valet objectForKey:@"profile_image"]];
                [v setCompany:[valet objectForKey:@"company"]];
                [v setDbaName:[valet objectForKey:@"dbaName"]];
                [v setEmail:[valet objectForKey:@"email"]];
                [v setPhone:[valet objectForKey:@"phone"]];
                [v setValet_fee:[valet objectForKey:@"valet_fee"]];
                [v setAddress:[valet objectForKey:@"address"]];
                [v setAddress2:[valet objectForKey:@"address2"]];
                [v setCity:[valet objectForKey:@"city"]];
                [v setState:[valet objectForKey:@"state"]];
                [v setZip:[valet objectForKey:@"zip"]];
                [v setContact_info:[valet objectForKey:@"contact_info"]];
                [v setMerchant_id:[valet objectForKey:@"merchant_id"]];
                [v setMerchant_status:[valet objectForKey:@"merchant_status"]];
                [v setMerchant_status:[valet objectForKey:@"merchant_status"]];
                [v setAccount_number:[valet objectForKey:@"account_number"]];
                [v setRouting_number:[valet objectForKey:@"routing_number"]];
                [v setTax_id:[valet objectForKey:@"tax_id"]];
                [v setTerms:[valet objectForKey:@"terms"]];
                
                
                [self.valetArray addObject:v];
                [self.valetTable reloadData];
            }
            [self.waitView setHidden:TRUE];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [self.waitView setHidden:TRUE];
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }];
}

- (void) deleteValet:(NSIndexPath *)indexPath {
    ValetCompany *v = [self.valetArray objectAtIndex:indexPath.row];
    NSString *valetMsg = [NSString stringWithFormat:@"Are you sure you want to delete valet company '%@'?", v.company];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:valetMsg
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Delete",nil];
    actionSheet.tag = indexPath.row;
    [actionSheet showInView:self.view];
}

-(void) removeValet:(NSString*) valetId {
    [self.waitView setHidden:FALSE];
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@deleteValetCompany/%@", [SystemPrefs instance].getURL, valetId];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.waitView setHidden:TRUE];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
        [self.waitView setHidden:TRUE];
    }];
}

/*-(void) addServerVehicle {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              [AccountProfile instance].make, @"make",
                              [AccountProfile instance].model, @"model",
                              [AccountProfile instance].color, @"color",
                              @"2014", @"year",
                              nil];
    if ([AccountProfile instance].licenseImage) {
        NSData *imageData = UIImageJPEGRepresentation([AccountProfile instance].licenseImage, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"licenseImage"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"http://hikervalet.com/valet/api/v1/vehicle"];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [self.valetArray removeAllObjects];
        NSDictionary *newVehicles = [responseObject objectForKey:@"vehicle"];
        for (NSString *vehicleID in newVehicles) {
            NSDictionary *vehicle = [newVehicles objectForKey:vehicleID];
            Vehicle *v = [[Vehicle alloc] initWithVehicle:[vehicle objectForKey:@"make"]
                                                    model:[vehicle objectForKey:@"model"]
                                                    color:[vehicle objectForKey:@"color"]];
            v.vehicleId = vehicleID;
            
            if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSString class]])
                v.vehicledefault = [vehicle objectForKey:@"defaultVehicle"];
            else if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSNumber class]])
                v.vehicledefault = [[vehicle objectForKey:@"defaultVehicle"] stringValue];
            else
                v.vehicledefault = @"0";
            
            [self.vehiclesArray addObject:v];
            [self.vehicleTable reloadData];
            [self checkForDefaults];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}*/

- (IBAction)touchEditProfile:(id)sender {
    ValetCompany *v = [self.valetArray objectAtIndex:self.selectedCell];
    ValetCompanyProfile *vcp = [[ValetCompanyProfile alloc] initWithNibName:@"ValetCompanyProfile" bundle:nil];
    vcp.valetCompany = v;
    [self.navigationController pushViewController:vcp animated:TRUE];
}

- (IBAction)touchManageUsers:(id)sender {
    ValetCompany *v = [self.valetArray objectAtIndex:self.selectedCell];
    ValetUsersList *vs = [[ValetUsersList alloc] initWithNibName:@"ValetUsersList" bundle:nil];
    vs.valetCompany = v;
    [self.navigationController pushViewController:vs animated:TRUE];
}
@end
