//
//  CreateAccountView2.h
//  ReadyValet
//
//  Created by Scott Leathers on 8/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"

@interface CreateAccountView3 : UIViewController <BSKeyboardControlsDelegate, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *make;
@property (strong, nonatomic) IBOutlet UITextField *model;
@property (strong, nonatomic) IBOutlet UITextField *license;
- (IBAction)touchSettings:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *color;

@property (strong, nonatomic) IBOutlet UIButton *makeButton;
@property (strong, nonatomic) IBOutlet UIButton *modelButton;
@property (strong, nonatomic) IBOutlet UIButton *colorButton;
- (IBAction)touchMake:(id)sender;
- (IBAction)touchModel:(id)sender;
- (IBAction)touchColor:(id)sender;



@end
