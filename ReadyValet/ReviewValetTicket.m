//
//  ReviewValetTicket.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ReviewValetTicket.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "MainProfile.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ReviewValetTicket ()

@end

@implementation ReviewValetTicket

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    self.valetLabel.text = [ValetStorage sharedModel].currentValetName;
    if ([self.valetLabel.text length] <= 0) {
        self.valetLabel.text = @"";
    }
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Check-In"];
    
    self.ticketNumberLabel.text = [ValetStorage sharedModel].currentTicketNumber;
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    UIBarButtonItem *settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)touchSubmit:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}
@end
