//
//  SettingsProfile.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/16/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsProfile : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *defaultTipLabel;
@property (strong, nonatomic) IBOutlet UIStepper *tipStepper;
- (IBAction)tipTouched:(id)sender;

@property (strong, nonatomic) IBOutlet UISwitch *autoCheckingSwitch;
@property (strong, nonatomic) IBOutlet UIView *waitView;

@end
