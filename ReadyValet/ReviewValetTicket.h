//
//  ReviewValetTicket.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewValetTicket : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UILabel *valetLabel;
@property (strong, nonatomic) IBOutlet UILabel *ticketNumberLabel;
- (IBAction)touchSubmit:(id)sender;

@end
