//
//  ValetSelectionVC.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetSelectionVC.h"
#import "ValetSelectionCell.h"
#import "ValetStorage.h"
#import "ValetTicketCamera.h"
#import "MainProfile.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ValetSelectionVC ()

@end

@implementation ValetSelectionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listDetails = [[NSMutableArray alloc] init];
    
    [self getCompaniesInRange];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Check-In"];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMonitoring" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"startMonitoring" object:self];
}

- (void) viewWillDisappear:(BOOL)animated {
    if (self.refreshTimer) {
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
    }
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)requeueInfoViewUpdate {
    
    if (self.refreshTimer) {
        [self.refreshTimer invalidate];
    }
    self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(getCompaniesInRange) userInfo:nil repeats:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0f;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.listDetails count] > 0)
        return [self.listDetails count];
    else
        return 1; // This should be changed for the future.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ValetSelectionCell";
    
    ValetSelectionCell *cell = (ValetSelectionCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetSelectionCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if ([self.listDetails count] > 0) {
        NSDictionary *stuff = [self.listDetails objectAtIndex:indexPath.row];
        NSString *company = [stuff objectForKey:@"company"];
        
        [cell.descLabel setText:company];
    }
    else
        [cell.descLabel setText:@"No Valet in Range"];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.listDetails count] > 0) {
        NSDictionary *stuff = [self.listDetails objectAtIndex:indexPath.row];
        NSString *company = [stuff objectForKey:@"company"];
        ValetTicketCamera *ct = [[ValetTicketCamera alloc] initWithNibName:@"ValetTicketCamera" bundle:nil];
        ct.title = @"";
        [ValetStorage sharedModel].currentValetId = [stuff objectForKey:@"company_id"];
        [ValetStorage sharedModel].currentValetName = company;
        
        [self.navigationController pushViewController:ct animated:TRUE];
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) getCompaniesInRange {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@range", [SystemPrefs instance].getURL];
    
    [self.apiManager GET:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.listDetails removeAllObjects];
        if ([responseObject count] == 0) {
            if (self.refreshTimer) {
                [self.refreshTimer invalidate];
                self.refreshTimer = nil;
            }
        }
        for (id object in responseObject) {
            NSMutableDictionary *subObject = [[responseObject objectForKey:object] mutableCopy];
            [subObject setObject:object forKey:@"company_id"];
            [self.listDetails addObject:subObject]; //[subObject objectForKey:@"company"]];
        }
        [self.valetTableView reloadData];
        [self requeueInfoViewUpdate];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [UtilFunctions displayError:self msg:error.localizedDescription];
        NSLog(@"Error: %@", error);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
