//
//  CompletedVC.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "RetrievedVC.h"
#import "ValetCell.h"
#import "ValetLicenseCell.h"
#import "Ticket.h"
#import "ValetStorage.h"
#import "ChangeStatus.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

@interface RetrievedVC ()

@end

@implementation RetrievedVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.listDetails = [[NSMutableArray alloc] init];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    logoutButton =[[UIBarButtonItem alloc] initWithTitle:@"LOGOUT" style:UIBarButtonItemStyleDone target:self action:@selector(logout:)];
    
    self.navigationItem.rightBarButtonItem = logoutButton;
    [self reloadTable];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTickets:)
                                                 name:@"reloadTickets"
                                               object:nil];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[ValetStorage sharedModel].currentValetName length] > 0)
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Retrieved", [ValetStorage sharedModel].currentValetName]];
    else
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Retrieved", [ValetStorage sharedModel].valetOwner.company_name]];
}

- (void)viewWillAppear:(BOOL)animated {
    //[self loadTable];
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)logout:(id) sender {
    [ValetStorage sharedModel].currentDriver = nil;
    [ValetStorage sharedModel].role = nil;
    [ValetStorage sharedModel].password = nil;
    [[ValetStorage sharedModel] saveLogin];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// Create custom header
    UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    viewFrame.backgroundColor = [UIColor colorWithRed:(237/255.0) green:(237/255.0) blue:(242/255.0) alpha:1];
    
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,320,22)];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
	label.textAlignment = NSTextAlignmentCenter;
	label.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin |  UIViewAutoresizingFlexibleRightMargin;
	[label setFont:[UIFont boldSystemFontOfSize:14]];
	label.userInteractionEnabled = NO;
	
    [viewFrame addSubview:label];
	label.text = @"Vehicle(s) retrieved";
    
    return viewFrame;
}

- (void) reloadTickets:(NSNotification *) notification {
    [self reloadTable];
}

- (void) reloadTable {
    [self.listDetails removeAllObjects];
    self.waitView.hidden = FALSE;
    NSString *status;
    for (status in [ValetStorage sharedModel].ticketQueues) {
        if ([status isEqualToString:@"3"]) {
            NSDictionary *tickets = [[ValetStorage sharedModel].ticketQueues objectForKey:status];
            NSString *ticketId;
            for (ticketId in tickets) {
                Ticket *ticket = [tickets objectForKey:ticketId];
                [self.listDetails addObject:ticket];
            }
        }
    }
    [self.mytableView reloadData];
    self.waitView.hidden = TRUE;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0f;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.listDetails count] > 0) {
        self.emptyLabel.hidden = TRUE;
        return [self.listDetails count];
    }
    self.emptyLabel.hidden = FALSE;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ValetCell";
    static NSString *CellLicenseIdentifier = @"ValetLicenseCell";
    
    
    ValetCell *cell = (ValetCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    ValetLicenseCell *licensecell = (ValetLicenseCell *)[tableView dequeueReusableCellWithIdentifier:CellLicenseIdentifier];
    if (licensecell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetLicenseCell" owner:self options:nil];
        licensecell = [nib objectAtIndex:0];
        [licensecell.middleLabel setHidden:TRUE];
        licensecell.licenseImage.layer.cornerRadius = 2.0f;
        licensecell.licenseImage.clipsToBounds = YES;
    }
    [cell.middleLabel setHidden:TRUE];
    
    Ticket *ticket = [self.listDetails objectAtIndex:indexPath.row];
    if (ticket) {
        
        if (ticket.driver) {
            Driver *driver = ticket.driver;
            cell.driverName.text = [NSString stringWithFormat:@"%@ %@", driver.firstName, driver.lastName];
        }
        
        Vehicle *vehicle = ticket.vehicle;
        [self getDriverImage:vehicle path:indexPath];
        if (vehicle.driverImage) {
            NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
            licensecell.descLabel.text = vehicleString;
            licensecell.licenseImage.image = vehicle.driverImage;
            if (ticket.driver) {
                Driver *driver = ticket.driver;
                licensecell.driverName.text = [NSString stringWithFormat:@"%@ %@", driver.firstName, driver.lastName];
            }
            return licensecell;
        }
        
        NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
        cell.descLabel.text = vehicleString;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[ValetStorage sharedModel].role isEqualToString:@"valet_owner"]) {
        [UtilFunctions displayError:self msg:@"You are currently logged in as a valet owner. Please login as a runner to perform these actions."];
    }
    else {
        Ticket *ticket = [self.listDetails objectAtIndex:indexPath.row];
        if (ticket) {
            [self displayChangeStatus:ticket];
        }
    }
}

-(void) displayChangeStatus:(Ticket*) ticket {
    ChangeStatus *changeStatus = [[ChangeStatus alloc] initWithNibName:@"ChangeStatus" bundle:nil];
    changeStatus.title = @"Vehicle";
    [changeStatus setTicket:ticket];
    [changeStatus setStatus:@"3"];
    
    [self.navigationController pushViewController:changeStatus animated:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getDriverImage:(Vehicle*) v path:(NSIndexPath *) path {
    if ([[[ValetStorage sharedModel].driverImages objectForKey:v.vehicleId] isKindOfClass:[UIImage class]]) {
        v.driverImage = [[ValetStorage sharedModel].driverImages objectForKey:v.vehicleId];
    }
    else {
        AFHTTPRequestOperationManager *apiManager;
        apiManager = [AFHTTPRequestOperationManager manager];
        
        NSString *url = [NSString stringWithFormat:@"%@driverimage/%@", [[SystemPrefs instance] getURL], v.vehicleId];
        
        [apiManager POST:url parameters:nil   success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            if (![[responseObject objectForKey:@"driverImage"] isKindOfClass:[NSNull class]]) {
                [v StringToImage:[responseObject objectForKey:@"driverImage"]];
                if (v.driverImage) {
                    [[ValetStorage sharedModel].driverImages setObject:v.driverImage forKey:v.vehicleId];
                    [self.mytableView beginUpdates];
                    [self.mytableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
                    [self.mytableView endUpdates];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}

@end
