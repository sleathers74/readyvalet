//
//  ValetDetection.h
//  ReadyValet
//
//  Created by Scott Leathers on 10/6/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface ValetDetection : UIViewController {
    
}
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;

- (IBAction)submitPaperTicket:(id)sender;
- (IBAction)submitAutoCheckin:(id)sender;



@end
