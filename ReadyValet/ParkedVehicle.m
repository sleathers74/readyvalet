//
//  ParkedVehicle.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/16/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ParkedVehicle.h"
#import "ValetStorage.h"
#import "ValetStub.h"
#import "UtilFunctions.h"
#import "MainProfile.h"
#import "SystemPrefs.h"
#import "WaitView.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ParkedVehicle ()

@end

@implementation ParkedVehicle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[SystemPrefs instance] sendGoogleView:@"Driver - Vehicle Parked"];
    
    self.waitView = [[WaitView alloc] initWithNibName:@"WaitView" bundle:nil];
    self.waitView.view.frame = self.view.bounds;
    [self.waitView.view setAutoresizingMask: UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.waitView.view setHidden:TRUE];
    [self.view addSubview:self.waitView.view];
    
    self.tipStepper.value = [[ValetStorage sharedModel].currentDriver.defaultTip doubleValue];
    [self.tipLabel setText:[NSString stringWithFormat:@"Tip $%.02f", [[ValetStorage sharedModel].currentDriver.defaultTip doubleValue]]];
    
    self.valetLabel.text = [ValetStorage sharedModel].currentValetName;
    self.carLabel.text = [ValetStorage sharedModel].currentVehicleName;
    self.valetServiceCharge.text = [NSString stringWithFormat:@"Valet Service $%@", [ValetStorage sharedModel].currentValetFee];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Parked In-Lot"];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    UIBarButtonItem *settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)touchCallHiker:(id)sender {
    [self.waitView.view setHidden:FALSE];
    [self processPayment];
}

- (void) changeVehicleStatus {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    //[self.apiManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[ValetStorage sharedModel].username password:[ValetStorage sharedModel].password];
    
    NSString *valetFee = [ValetStorage sharedModel].currentValetFee;
    NSString *paidAmt =  [NSString stringWithFormat:@"%.02f", ([valetFee integerValue] + self.tipStepper.value)];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@ticket/%@/2", [[SystemPrefs instance] getURL], [ValetStorage sharedModel].currentTicketNumber];
    [self.apiManager PUT:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        /*ValetStub *ct = [[ValetStub alloc] initWithNibName:@"ValetStub" bundle:nil];
        ct.paidAmt = paidAmt;
        ct.title = @"";*/
        [self.waitView.view setHidden:TRUE];
        //[self.navigationController pushViewController:ct animated:TRUE];
        [self.navigationController popToRootViewControllerAnimated:TRUE];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.waitView.view setHidden:TRUE];
        [UtilFunctions displayError:self msg:error.localizedDescription];
        NSLog(@"Error: %@", error);
    }];
}

- (void) processPayment {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSString *valet_id  = [ValetStorage sharedModel].currentValetId;
    NSString *valetName = [ValetStorage sharedModel].currentValetName;
    NSString *valetFee = [ValetStorage sharedModel].currentValetFee;
    NSString *serviceFee = [ValetStorage sharedModel].currentServiceFee;
    NSString *merchant_id = [ValetStorage sharedModel].currentMerchantId;
    NSString *merchant_status = [ValetStorage sharedModel].currentMerchantStatus;
    NSString *vehicle = [NSString stringWithFormat:@"%@ %@ %@", [ValetStorage sharedModel].currentVehicle.vehicleColor, [ValetStorage sharedModel].currentVehicle.vehicleManufacturer, [ValetStorage sharedModel].currentVehicle.vehicleModel];
    NSString *paidAmt =  [NSString stringWithFormat:@"%.02f", ([valetFee integerValue] + self.tipStepper.value)];
    NSString *tipAmt =  [NSString stringWithFormat:@"%.02f", self.tipStepper.value];
    NSString *ticketNumber = [ValetStorage sharedModel].currentTicketNumber;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    if (![merchant_status isEqualToString:@"active"]) {
        [self.waitView.view setHidden:TRUE];
        NSString *vendorMsg = [NSString stringWithFormat:@"We are sorry but this vendor account is under %@ status.", merchant_status];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Vendor Error" message:vendorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              valet_id, @"valet_id",
                              valetName, @"valetname",
                              vehicle, @"vehicle",
                              ticketNumber, @"ticketnumber",
                              paidAmt, @"amount",
                              valetFee, @"valetamount",
                              serviceFee, @"service_fee",
                              merchant_id, @"merchant_id",
                              merchant_status, @"merchant_status",
                              tipAmt, @"tipamount",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@payment", [[SystemPrefs instance] getURL]];
    [self.apiManager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
       if ([[responseObject objectForKey:@"success"] boolValue] == YES) {
            [self changeVehicleStatus];
        }
        else {
            [self.waitView.view setHidden:TRUE];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Failed to process your credit card. Please check your card settings in your profile." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self.waitView.view setHidden:TRUE];
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}


- (IBAction)tipTouched:(id)sender {
    [self.tipLabel setText:[NSString stringWithFormat:@"Tip $%.02f", self.tipStepper.value]];
}

/*-(void) paymentAuth {
    NSString *valetFee = [ValetStorage sharedModel].currentValetFee;
    NSString *msg = [NSString stringWithFormat:@"Your credit card will be billed $%.02f.", ([valetFee integerValue] + self.tipStepper.value)];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
    alert.tag = 200;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0) {
        // do something here...
    }
}*/
@end
