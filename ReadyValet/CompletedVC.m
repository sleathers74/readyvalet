//
//  CompletedVC.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "CompletedVC.h"
#import "ValetCell.h"
#import "ValetLicenseCell.h"
#import "Ticket.h"
#import "Vehicle.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "ValetReceiptsDetail.h"
#import "SystemPrefs.h"

@interface CompletedVC ()

@end

@implementation CompletedVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.listDetails = [[NSMutableArray alloc] init];
    
    logoutButton =[[UIBarButtonItem alloc] initWithTitle:@"LOGOUT" style:UIBarButtonItemStyleDone target:self action:@selector(logout:)];
    
    self.navigationItem.rightBarButtonItem = logoutButton;


    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTickets:)
                                                 name:@"reloadTickets"
                                               object:nil];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[ValetStorage sharedModel].currentValetName length] > 0)
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Completed", [ValetStorage sharedModel].currentValetName]];
    else
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Completed", [ValetStorage sharedModel].valetOwner.company_name]];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateQueues];
    
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)logout:(id) sender {
    [ValetStorage sharedModel].currentDriver = nil;
    [ValetStorage sharedModel].role = nil;
    [ValetStorage sharedModel].password = nil;
    [[ValetStorage sharedModel] saveLogin];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// Create custom header
    UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    viewFrame.backgroundColor = [UIColor colorWithRed:(237) green:(237) blue:(242) alpha:1];
    
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,320,22)];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
	label.textAlignment = NSTextAlignmentCenter;
	label.autoresizingMask =  UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin |  UIViewAutoresizingFlexibleRightMargin;
	[label setFont:[UIFont boldSystemFontOfSize:14]];
	label.userInteractionEnabled = NO;
	
    [viewFrame addSubview:label];
	label.text = @"Vehicle(s) completed";
    
    return viewFrame;
}

- (void) reloadTickets:(NSNotification *) notification {
    //[self updateQueues];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0f;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.listDetails count] > 0) {
        self.emptyLabel.hidden = TRUE;
        return [self.listDetails count];
    }
    self.emptyLabel.hidden = FALSE;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ValetCell";
    static NSString *CellLicenseIdentifier = @"ValetLicenseCell";
    
    
    ValetCell *cell = (ValetCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    ValetLicenseCell *licensecell = (ValetLicenseCell *)[tableView dequeueReusableCellWithIdentifier:CellLicenseIdentifier];
    if (licensecell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetLicenseCell" owner:self options:nil];
        licensecell = [nib objectAtIndex:0];
        [licensecell.middleLabel setHidden:TRUE];
        licensecell.licenseImage.layer.cornerRadius = 2.0f;
        licensecell.licenseImage.clipsToBounds = YES;
    }
    [cell.middleLabel setHidden:TRUE];
    
    Ticket *ticket = [self.listDetails objectAtIndex:indexPath.row];
    if (ticket) {
        
        if (ticket.driver) {
            Driver *driver = ticket.driver;
            cell.driverName.text = [NSString stringWithFormat:@"%@ %@", driver.firstName, driver.lastName];
        }
        
        Vehicle *vehicle = ticket.vehicle;
        [self getDriverImage:vehicle path:indexPath];
        if (vehicle.driverImage) {
            NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
            licensecell.descLabel.text = vehicleString;
            licensecell.licenseImage.image = vehicle.driverImage;
            if (ticket.driver) {
                Driver *driver = ticket.driver;
                licensecell.driverName.text = [NSString stringWithFormat:@"%@ %@", driver.firstName, driver.lastName];
            }
            return licensecell;
        }
        
        NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
        cell.descLabel.text = vehicleString;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Ticket *ticket = [self.listDetails objectAtIndex:indexPath.row];
    if (ticket) {
        [self displayChangeStatus:ticket];
    }
}

-(void) displayChangeStatus:(Ticket*) ticket {
    ValetReceiptsDetail *vrd = [[ValetReceiptsDetail alloc] initWithNibName:@"ValetReceiptsDetail" bundle:nil];
    [vrd setTicket:ticket];
    [self.navigationController pushViewController:vrd animated:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateQueues {
    // Ticket Queues
    self.waitView.hidden = FALSE;
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    [self.listDetails removeAllObjects];
    
    NSString *url = [NSString stringWithFormat:@"%@queue/4", [[SystemPrefs instance] getURL]];
    [self.apiManager GET:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if ([responseObject count] > 0) {
            long totalCount = [responseObject count];
            for (long icount = 1; icount <= totalCount; icount++) {
                NSDictionary *ticket = [responseObject objectForKey:[NSString stringWithFormat:@"%ld", icount]];
                NSString *ticketid = [ticket objectForKey:@"ticket_id"];
                Vehicle *v = [[Vehicle alloc] initWithVehicle:[ticket objectForKey:@"make"]
                                                            model:[ticket objectForKey:@"model"]
                                                            color:[ticket objectForKey:@"color"]];
                v.vehicleId = [ticket objectForKey:@"vehicle_id"];
                
                Driver *d = [[Driver alloc] init];
                d.firstName = [ticket objectForKey:@"firstname"];
                d.lastName = [ticket objectForKey:@"lastname"];
                d.emailAddress = [ticket objectForKey:@"email"];
                d.phone = [ticket objectForKey:@"phone"];
                
                Ticket *newTicket = [[Ticket alloc] initWithTicket:ticketid status:@"4" vehicle:v driver:d];
                [self.listDetails addObject:newTicket];
            }
            self.waitView.hidden = TRUE;
            [self.mytableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        self.waitView.hidden = TRUE;
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
    
}

-(void) getDriverImage:(Vehicle*) v path:(NSIndexPath *) path {
    if ([[[ValetStorage sharedModel].driverImages objectForKey:v.vehicleId] isKindOfClass:[UIImage class]]) {
        v.driverImage = [[ValetStorage sharedModel].driverImages objectForKey:v.vehicleId];
    }
    else {
        AFHTTPRequestOperationManager *apiManager;
        apiManager = [AFHTTPRequestOperationManager manager];
        
        NSString *url = [NSString stringWithFormat:@"%@driverimage/%@", [[SystemPrefs instance] getURL], v.vehicleId];
        
        [apiManager POST:url parameters:nil   success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            if (![[responseObject objectForKey:@"driverImage"] isKindOfClass:[NSNull class]]) {
                [v StringToImage:[responseObject objectForKey:@"driverImage"]];
                if (v.driverImage) {
                    [[ValetStorage sharedModel].driverImages setObject:v.driverImage forKey:v.vehicleId];
                    [self.mytableView beginUpdates];
                    [self.mytableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
                    [self.mytableView endUpdates];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}

@end
