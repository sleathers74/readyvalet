#import "myPaymentMethodVC.h"
#import "BTDropInUtil.h"
#import "BTUIViewUtil.h"
#import "BTUI.h"
#import "BTDropinViewController.h"
#import "BTDropInLocalizedString.h"
#import "BTUILocalizedString.h"
#import "BTUI.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "CreditCardProcessing.h"
#import "VehicleSelection.h"

@interface myPaymentMethodVC ()

@end

@implementation myPaymentMethodVC

- (instancetype)init {
    return [self initWithStyle:UITableViewStyleGrouped];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(addedCard:)
                                                     name:@"addedCard"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(paymentsFetched:)
                                                     name:@"paymentsFetched"
                                                   object:nil];
        [self setupLayout];
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"View background color is %@", self.view.backgroundColor);
    NSLog(@"View background color is %@", self.tableView.backgroundColor);
    
    self.factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    self.factory.colors = @[[NIKColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    
    [self.tableView reloadData];
}


- (void) setupLayout {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    if (loggedin == 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
    }
    else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"NEXT" style:UIBarButtonItemStylePlain target:self action:@selector(next)];
        
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
        self.navigationItem.leftBarButtonItem = leftBarButton;
    }
    
}

#pragma mark -

- (void) next {
    [AccountProfile instance].creditCardNum  = @"99999";
    [AccountProfile instance].expirationDate = @"1234";
    [AccountProfile instance].securityCode  = @"4434";
    
    if ([self.paymentMethods count] <= 0) {
        NSString *msg = @"You must add at least 1 credit card.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    //CreateAccountView3 *cav3 = [[CreateAccountView3 alloc] initWithNibName:@"CreateAccountView3" bundle:nil];
    VehicleSelection *vs = [[VehicleSelection alloc] initWithNibName:@"VehicleSelection" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
    //cav3 = nil;
}

- (IBAction) cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:FALSE];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(__unused UITableView *)tableView numberOfRowsInSection:(__unused NSInteger)section
{
    return [self.paymentMethods count] + 1 ? : 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@" denotes default payment method" forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon icon = NIKFontAwesomeIconCheck;
    [button setImage:[self.factory createImageForIcon:icon] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    button.frame = CGRectMake(15, 0, tableView.frame.size.width, 44);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [view addSubview:button];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *paymentMethodCellIdentifier = @"paymentMethodCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:paymentMethodCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:paymentMethodCellIdentifier];
    }
    
    if ([self.paymentMethods count] == 0 || indexPath.row > [self.paymentMethods count] - 1) {
        cell.textLabel.text = @"Add Payment";
        
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:@"Add Credit Card"];
        [typeWithDescription addAttribute:NSFontAttributeName value:self.theme.controlTitleFont range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;
        
        NIKFontAwesomeIcon icon = NIKFontAwesomeIconPlus;
        UIImage *image = [self.factory createImageForIcon:icon];
        cell.imageView.contentMode = UIViewContentModeCenter;
        cell.imageView.image = image;
    }
    else {
        BTPaymentMethod *paymentMethod = [self.paymentMethods objectAtIndex:indexPath.row];
        if ([paymentMethod isKindOfClass:[BTPayPalPaymentMethod class]]) {
            BTPayPalPaymentMethod *payPalPaymentMethod = (BTPayPalPaymentMethod *)paymentMethod;
            NSString *typeString = BTUILocalizedString(PAYPAL_CARD_BRAND);
            NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", typeString, (payPalPaymentMethod.description ?: @"")]];
            [typeWithDescription addAttribute:NSFontAttributeName value:self.theme.controlTitleFont range:NSMakeRange(0, [typeString length])];
            [typeWithDescription addAttribute:NSFontAttributeName value:self.theme.controlDetailFont range:NSMakeRange([typeString length], [payPalPaymentMethod.description length] + 1)];
            cell.textLabel.attributedText = typeWithDescription;
            
            
            BTUIVectorArtView *iconArt = [[BTUI braintreeTheme] vectorArtViewForPaymentMethodType:BTUIPaymentMethodTypePayPal];
            UIImage *icon = [iconArt imageOfSize:CGSizeMake(42, 23)];
            cell.imageView.contentMode = UIViewContentModeCenter;
            cell.imageView.image = icon;
            
        } else if([paymentMethod isKindOfClass:[BTCardPaymentMethod class]]) {
            BTCardPaymentMethod *card = (BTCardPaymentMethod *)paymentMethod;
            
            
            BTUIPaymentMethodType uiPaymentMethodType = [BTDropInUtil uiForCardType:card.type];
            NSString *typeString = [BTUIViewUtil nameForPaymentMethodType:uiPaymentMethodType];
            
            NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", typeString, card.description]];
            [typeWithDescription addAttribute:NSFontAttributeName value:self.theme.controlTitleFont range:NSMakeRange(0, [typeString length])];
            [typeWithDescription addAttribute:NSFontAttributeName value:self.theme.controlDetailFont range:NSMakeRange([typeString length], [card.description length])];
            cell.textLabel.attributedText = typeWithDescription;
            
            BTUIPaymentMethodType uiType = [BTDropInUtil uiForCardType:card.type];
            BTUIVectorArtView *iconArt = [[BTUI braintreeTheme] vectorArtViewForPaymentMethodType:uiType];
            UIImage *icon = [iconArt imageOfSize:CGSizeMake(42, 23)];
            cell.imageView.contentMode = UIViewContentModeCenter;
            cell.imageView.image = icon;
        } else {
            cell.textLabel.text = paymentMethod.description;
            cell.imageView.image = nil;
        }
    }
    
    
    if ([self.paymentMethods count] > 0 && indexPath.row == self.selectedPaymentMethodIndex) {
        NIKFontAwesomeIcon icon = NIKFontAwesomeIconCheck;
        UIImage *image = [self.factory createImageForIcon:icon];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        button.frame = frame;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.backgroundColor = [UIColor clearColor];
        cell.accessoryView = button;
    }
    else {
        cell.accessoryView = nil;
    }
    //cell.accessoryType = ([self.paymentMethods count] > 0 && indexPath.row == self.selectedPaymentMethodIndex) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(__unused UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.paymentMethods count] > 0 && indexPath.row <= ([self.paymentMethods count] - 1)) {
        self.selectedPaymentMethodIndex = indexPath.row;
        [self.delegate selectPaymentMethodViewController:self didSelectPaymentMethodAtIndex:indexPath.row];
    }
    else {
        [self.delegate selectPaymentMethodViewControllerDidRequestNew:self];
    }
    
    //[self.tableView reloadData];
}

- (void) addedCard:(NSNotification *) notification {
    [[CreditCardProcessing sharedModel] fetchPaymentMethods];
}

- (void) paymentsFetched:(NSNotification *) notification {
    NSMutableArray *newPaymentMethods = [NSMutableArray arrayWithArray:[CreditCardProcessing sharedModel].paymentMethods];
    self.paymentMethods = newPaymentMethods;
    [self.tableView reloadData];
}

@end
