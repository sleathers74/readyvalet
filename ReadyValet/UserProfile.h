//
//  UserProfile.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/8/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "SHSPhoneLibrary.h"

@interface UserProfile : UIViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate> {
}
@property (strong, nonatomic) IBOutlet UITextField *firstTextfield;
@property (strong, nonatomic) IBOutlet UITextField *lastTextfield;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextfield;
- (IBAction)touchSignout:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *editLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
- (IBAction)touchPhoto:(id)sender;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet SHSPhoneTextField *phoneNumberText;
@property (nonatomic, assign) BOOL valet_owner;
@end
