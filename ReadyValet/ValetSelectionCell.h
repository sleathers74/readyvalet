//
//  ValetSelectionCell.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ValetSelectionCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *descLabel;

@end
