//
//  ReceiptController.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface ReceiptController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
}
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UITableView *receiptTable;
@property (strong, nonatomic) NSMutableArray *listDetails;
@property (strong, nonatomic) IBOutlet UIView *waitView;

@end
