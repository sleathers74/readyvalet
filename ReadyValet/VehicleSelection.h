//
//  VehicleSelection.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTUI.h"
#import "AFNetworking.h"


#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface VehicleSelection : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate> {
    
}

@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UITableView *vehicleTable;
@property (nonatomic, strong) NSMutableArray *vehiclesArray;
@property (nonatomic, strong) BTUI *theme;
@property (nonatomic, assign) NSInteger selectedPaymentMethodIndex;
@property (strong, nonatomic) IBOutlet UIView *waitView;

@property (nonatomic, strong) NIKFontAwesomeIconFactory *factory;

@end
