//
//  Vehicle.h
//  Valet
//
//  Created by Matthew Schmidgall on 1/5/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vehicle : NSObject

@property (strong,nonatomic) NSString *vehicleId;
@property (strong,nonatomic) NSString *vehicleManufacturer;
@property (strong,nonatomic) NSString *vehicleModel;
@property (strong,nonatomic) NSString *vehicleColor;
@property (strong,nonatomic) NSString *license;
@property (strong,nonatomic) NSString *driverName;
@property (strong,nonatomic) NSString *vehicledefault;


// Legacy
@property (strong,nonatomic) NSString *vehicleName;


@property (strong,nonatomic) NSString *ticketNumber;
@property (strong,nonatomic) UIImage *ticketImage;
@property (strong,nonatomic) UIImage *licenseImage;
@property (strong,nonatomic) UIImage *driverImage;

- (id) initWithName:(NSString*)vehicleName;
- (id) initWithVehicle:(NSString*)vehicleManufacturer model:(NSString*)vehicleModel color:(NSString*)vehicleColor;
-(void) getDriverImage;
- (void) StringToImage:(NSString*) imageString;
- (NSString*) ImageToString;


@end
