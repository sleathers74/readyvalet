//
//  UserProfile.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/8/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "SHSPhoneLibrary.h"
#import "ValetCompany.h"
#import "BSKeyboardControls.h"

@interface ValetCompanyProfile : UIViewController <UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, BSKeyboardControlsDelegate, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate, UIAlertViewDelegate> {
}
- (IBAction)termsValueChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *termsSwitch;
- (IBAction)termsTouched:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *legalCompanyName;
@property (strong, nonatomic) IBOutlet UITextField *dbaName;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet SHSPhoneTextField *phoneNumberText;
@property (strong, nonatomic) IBOutlet SHSPhoneTextField *valetFeeTF;
@property (strong, nonatomic) IBOutlet UITextField *contactInfoTF;
@property (strong, nonatomic) IBOutlet UITextField *addressTF;
@property (strong, nonatomic) IBOutlet UITextField *cityTF;
@property (strong, nonatomic) IBOutlet UITextField *stateTF;
@property (strong, nonatomic) IBOutlet UITextField *zipTF;
@property (strong, nonatomic) IBOutlet SHSPhoneTextField *taxidTF;
@property (strong, nonatomic) IBOutlet UITextField *bankAccountTF;
@property (strong, nonatomic) IBOutlet UITextField *routingNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *merchantidTF;
@property (strong, nonatomic) IBOutlet UITextField *merchantstatusTF;

@property (strong, nonatomic) IBOutlet UILabel *editLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) ValetCompany *valetCompany;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)touchPhoto:(id)sender;
@end
