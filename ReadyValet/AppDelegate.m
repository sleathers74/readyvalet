//    //
//  AppDelegate.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/21/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "AppDelegate.h"

#import "DriverTabViewController.h"
#import "MainValetViewTVC.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "ValetStorage.h"
#import "SplashView.h"
#import "SystemPrefs.h"
#import "JSONKit.h"

#import "MainProfile.h"

// Google
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

// nellcote test
// jqknobloch@hotmail.com test
// nellcote test

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self setupGoogleStartSession];
    
    [self monitor];

    // create socket.io client instance
    //socketIO = [[SocketIO alloc] initWithDelegate:self];
    //[socketIO connectToHost:@"node.hikervalet.com" onPort:25988];

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [[UINavigationBar appearance] setTranslucent:FALSE];
    }
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed: 0.0f/255.0f green:34.0f/255.0f blue:85.0f/255.0f alpha:1.0]];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed: 0.0f/255.0f green:34.0f/255.0f blue:85.0f/255.0f alpha:1.0]];
    
    
    // Set BarButtonItemSize
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:10.0f]
       }
     forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UIToolbar class], nil] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                                                       NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0f]
                                                                                                       }
                                                                                            forState:UIControlStateNormal];
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    self.splashScreen = [[SplashView alloc] initWithNibName:@"SplashView" bundle:nil];
    UINavigationController *splashViewNav = [[UINavigationController alloc] initWithNibName:@"" bundle:nil];
    [splashViewNav pushViewController:self.splashScreen animated:TRUE];
    
    self.window.rootViewController = splashViewNav;
    [self.window makeKeyAndVisible];
    
    
    [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(onSlashScreenDone) userInfo:nil repeats:NO];
    
    if ([self updateApp]) {
        [self getMakesModels];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self detectBluetooth];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(logout:)
                                                 name:@"logout"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(login:)
                                                 name:@"login"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatus:)
                                                 name:@"updateStatus"
                                               object:nil];
    
    return YES;
}

- (void) setupGoogleStartSession {
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 10;
    
#ifdef DEBUG
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
#endif

    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-58879486-1"];

    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    [tracker set:kGAIAppName value:appDisplayName];
    [tracker set:kGAIAppVersion value:[NSString stringWithFormat:@"Version %@ (%@)", majorVersion, minorVersion]];
    [tracker set:kGAISessionControl value:@"start"];
}

-(void)onSlashScreenDone {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    
    if (loggedin == 1) {
        [self login];
    }
    else {
        [self processMainView];
    }
}

- (void) processMainView {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    
    if (loggedin == 1) {
        NSString *role = [defaults stringForKey:@"role"];
        if ([role isEqualToString:@"driver"]) {
            [self displayMainDriverView];
        }
        else {
            [self displayMainValetView];
        }
    }
    else {
        self.mainLoginView = [[MainLoginView alloc] initWithNibName:@"MainLoginView" bundle:nil];
        self.mainLoginView.title = @"HIKER";
        
        UINavigationController *pick = [[UINavigationController alloc] initWithNibName:@"" bundle:nil];
        [pick pushViewController:self.mainLoginView animated:TRUE];

        self.window.rootViewController = pick;
        pick = nil;
    }
    [self.window makeKeyAndVisible];
}

- (bool) login {
    __block bool areWeIn = false;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                username, @"username",
                                password, @"password",
                                nil];
     
    [manager GET:@"https://hikervalet.com/valet/api/v1/profile" parameters:postData
                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (responseObject) {
            [ValetStorage sharedModel].username = username;
            [ValetStorage sharedModel].password = password;
            
            [[ValetStorage sharedModel] saveLogin];
            
            
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"loggedin"];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"role"] forKey:@"role"];
            [[NSUserDefaults standardUserDefaults] setObject:[ValetStorage sharedModel].username forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setObject:[ValetStorage sharedModel].password forKey:@"password"];
            //[[NSUserDefaults standardUserDefaults] setInteger:[[responseObject objectForKey:@"auto_detect"] integerValue] forKey:@"autodetect"];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:@"&uid" value:username];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Main Sign-In View"
                                                                    action:@"sign-in"
                                                                    label:nil
                                                                    value:nil] build]];
            
            [[SystemPrefs instance] sendGoogleEvent:@"User" action:@"Role" label:[responseObject objectForKey:@"role"]];
            
            [ValetStorage sharedModel].role = [responseObject objectForKey:@"role"];
            [ValetStorage sharedModel].autoDetect = [[responseObject objectForKey:@"auto_detect"] integerValue];
            
            
            if ([(NSString*)[responseObject objectForKey:@"role"] isEqualToString:@"driver"]) {
                [ValetStorage sharedModel].profileId = [[responseObject objectForKey:@"driver"] objectForKey:@"driver_id"];
                Driver *d = [[Driver alloc] init];
                d.emailAddress = [[responseObject objectForKey:@"driver"] objectForKey:@"email"];
                d.firstName = [[responseObject objectForKey:@"driver"] objectForKey:@"first_name"];
                d.lastName = [[responseObject objectForKey:@"driver"] objectForKey:@"last_name"];
                d.phone = [[responseObject objectForKey:@"driver"] objectForKey:@"phone"];
                d.creditCardNumber = [[responseObject objectForKey:@"driver"] objectForKey:@"card_number"];
                d.defaultTip = [[responseObject objectForKey:@"driver"] objectForKey:@"defaultTip"];
                
                /*NSString *driverImage = [[responseObject objectForKey:@"driver"] objectForKey:@"driverImage"];
                if ([driverImage length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:driverImage options:0];
                    d.driverImage = [UIImage imageWithData:imageData];
                }*/
                [self getDriverProfileImage];

                d.creditCardExpiration = [[responseObject objectForKey:@"driver"] objectForKey:@"card_expiration"];
                [ValetStorage sharedModel].autoDetect = [[[responseObject objectForKey:@"driver"] objectForKey:@"auto_detect"] integerValue];
                NSString *vehicleId;
                NSDictionary *v;
                [ValetStorage sharedModel].currentVehicle = nil;
                for (vehicleId in [responseObject objectForKey:@"vehicle"]) {
                    v = [[responseObject objectForKey:@"vehicle"] objectForKey:vehicleId];
                    Vehicle *newVehicle =[[Vehicle alloc] initWithVehicle:[v objectForKey:@"make"] model:[v objectForKey:@"model"] color:[v objectForKey:@"color"]];
                    newVehicle.license = [v objectForKey:@"license"];
                    newVehicle.vehicleId = vehicleId;
                    
                    if ([[v objectForKey:@"defaultVehicle"] isKindOfClass:[NSString class]])
                        newVehicle.vehicledefault = [v objectForKey:@"defaultVehicle"];
                    else if ([[v objectForKey:@"defaultVehicle"] isKindOfClass:[NSNumber class]])
                        newVehicle.vehicledefault = [[v objectForKey:@"defaultVehicle"] stringValue];
                    else
                        newVehicle.vehicledefault = @"0";
                    
                    NSString *licenseImage = [v objectForKey:@"licenseImage"];
                    if ([licenseImage length] > 0) {
                        NSData* imageData = [[NSData alloc] initWithBase64EncodedString:licenseImage options:0];
                        newVehicle.licenseImage = [UIImage imageWithData:imageData];
                    }
                    [d.vehicles addObject:newVehicle];
                    if ([newVehicle.vehicledefault isEqualToString:@"1"]) {
                        [ValetStorage sharedModel].currentVehicle = newVehicle;
                        [ValetStorage sharedModel].currentVehicleId = newVehicle.vehicleId;
                    }
                }
                
                if ([responseObject objectForKey:@"valet"]) {
                    NSDictionary *valetDict = [responseObject objectForKey:@"valet"];
                    [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_id"];
                    [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company"];
                    [ValetStorage sharedModel].currentValetFee = [valetDict objectForKey:@"valet_fee"];
                    [ValetStorage sharedModel].currentServiceFee = [valetDict objectForKey:@"service_fee"];
                    [ValetStorage sharedModel].currentMerchantId = [valetDict objectForKey:@"merchant_id"];
                    [ValetStorage sharedModel].currentMerchantStatus = [valetDict objectForKey:@"merchant_status"];
                }
                
                if ([responseObject objectForKey:@"valet_owner"]) {
                    NSDictionary *valetDict = [responseObject objectForKey:@"valet_owner"];
                    [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_owner_id"];
                    [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company_name"];
                    [[ValetStorage sharedModel].valetOwner setCompany_name:[valetDict objectForKey:@"company_name"]];
                    [[ValetStorage sharedModel].valetOwner setOwner_id:[valetDict objectForKey:@"valet_owner_id"]];
                }

                areWeIn = true;
                [ValetStorage sharedModel].currentDriver = d;
            }
            else if ([(NSString*)[responseObject objectForKey:@"role"] isEqualToString:@"valet"]) {
                areWeIn = true;
                [ValetStorage sharedModel].profileId = [[responseObject objectForKey:@"valet"] objectForKey:@"valet_id"];
                NSDictionary *valetDict = [responseObject objectForKey:@"valet"];
                [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_id"];
                [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company"];
                [ValetStorage sharedModel].currentValetFee = [valetDict objectForKey:@"valet_fee"];
                [ValetStorage sharedModel].currentServiceFee = [valetDict objectForKey:@"service_fee"];
                [ValetStorage sharedModel].currentMerchantId = [valetDict objectForKey:@"merchant_id"];
                [ValetStorage sharedModel].currentMerchantStatus = [valetDict objectForKey:@"merchant_status"];
            }
            else {
                areWeIn = true;
                NSDictionary *valetDict = [responseObject objectForKey:@"valet_owner"];
                [ValetStorage sharedModel].profileId = [valetDict objectForKey:@"valet_owner_id"];
                [[ValetStorage sharedModel].valetOwner setCompany_name:[valetDict objectForKey:@"company_name"]];
                [[ValetStorage sharedModel].valetOwner setOwner_id:[valetDict objectForKey:@"valet_owner_id"]];
                [[ValetStorage sharedModel].valetOwner setFirst_name:[valetDict objectForKey:@"first_name"]];
                [[ValetStorage sharedModel].valetOwner setLast_name:[valetDict objectForKey:@"last_name"]];
                [[ValetStorage sharedModel].valetOwner setPhone_number:[valetDict objectForKey:@"phone_number"]];
                NSString *company_logo = [valetDict objectForKey:@"company_logo"];
                if ([company_logo length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:company_logo options:0];
                    [ValetStorage sharedModel].valetOwner.company_logo = [UIImage imageWithData:imageData];
                }
            }
            [self processMainView];
        }
        else {
            areWeIn = false;
            [self processMainView];
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        areWeIn = false;
        [self processMainView];
    }];
    return areWeIn;
}

- (void) getDriverProfileImage {
    AFHTTPRequestOperationManager *apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@getImage", [SystemPrefs instance].getURL];
    [apiManager POST:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject objectForKey:@"image"] length] > 0) {
            NSData* imageData = [[NSData alloc] initWithBase64EncodedString:[responseObject objectForKey:@"image"] options:0];
            [ValetStorage sharedModel].currentDriver.driverImage = [UIImage imageWithData:imageData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}

// Call this like so [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
- (void) logout:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"logout"]) {
        UINavigationController *pick = [[UINavigationController alloc] initWithNibName:@"" bundle:nil];
        if (self.mainLoginView == nil) {
            self.mainLoginView = [[MainLoginView alloc] initWithNibName:@"MainLoginView" bundle:nil];
            self.mainLoginView.title = @"HIKER";
        }
        [pick pushViewController:self.mainLoginView animated:TRUE];
        self.window.rootViewController = pick;
    }
}

- (void) login:(NSNotification *) notification {
    [self login];
}

- (void) displayMainDriverView {
    self.driverTabViewController = [[DriverTabViewController alloc] initWithNibName:@"DriverTabViewController" bundle:nil];
    self.window.rootViewController = self.driverTabViewController;
    //self.driverTabViewController = nil;
    
}

- (void) displayMainValetView {
    MainValetViewTVC *mv = [[MainValetViewTVC alloc] initWithNibName:@"MainValetViewTVC" bundle:nil];
    //UINavigationController *unc = [[UINavigationController alloc] initWithRootViewController:mv];
    self.window.rootViewController = mv;
    mv = nil;
}

- (void)detectBluetooth
{
    if(!self.bluetoothManager)
    {
        // Put on main queue so we can call UIAlertView from delegate callbacks.
        self.bluetoothManager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    }
    [self centralManagerDidUpdateState:self.bluetoothManager]; // Show initial state
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSString *stateString = nil;
    switch( self.bluetoothManager.state )
    {
        case CBCentralManagerStateResetting: stateString = @"The connection with the system service was momentarily lost, update imminent."; break;
        case CBCentralManagerStateUnsupported: stateString = @"The platform doesn't support Bluetooth Low Energy."; break;
        case CBCentralManagerStateUnauthorized: stateString = @"The app is not authorized to use Bluetooth Low Energy."; break;
        case CBCentralManagerStatePoweredOff: stateString = @"Bluetooth is currently powered off."; break;
        case CBCentralManagerStatePoweredOn: stateString = @"Bluetooth is currently powered on and available to use."; break;
        default: stateString = @""; break; //@"State unknown, update imminent."; break;
    }
    /*if ([stateString length] > 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth state." message:stateString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }*/
}

- (void) logout {
    self.window.rootViewController = self.loginView;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ReadyValet" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ReadyValet.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void) getMakesModels {
    NSString *myURL = @"http://api.edmunds.com/api/vehicle/v2/makes?fmt=json&year=2014&api_key=sgzw3rf2uba4qdnxax7k2bwk";
    
    NSURL           *url        = [NSURL URLWithString:myURL];
    NSURLRequest    *request    = [NSURLRequest requestWithURL:url];
    NSURLResponse   *response   = nil;
    NSError         *error      = nil;
    
    NSData          *data       = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (!data)
    {
        NSLog(@"Error: %@", error);
    }
    else
    {
        JSONDecoder *jsonKitDecoder = [JSONDecoder decoderWithParseOptions:JKParseOptionStrict];
        NSMutableArray *storedcontent = [[NSMutableArray alloc] init];
        NSDictionary *content = [jsonKitDecoder objectWithData:data error:&error];
        
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            if (content && [content count] > 0) {
                NSArray *makes = [content objectForKey:@"makes"];
                for (NSDictionary *make in makes) {
                    NSMutableArray *modelNames = [[NSMutableArray alloc] init];
                    NSString *name = [make objectForKey:@"name"];
                    NSArray *models = [make objectForKey:@"models"];
                    for (NSDictionary *model in models) {
                        NSString *ModelName = [model objectForKey:@"name"];
                        [modelNames addObject:ModelName];
                    }
                    
                    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:name, @"makeName", modelNames, @"models", nil];
                    [storedcontent addObject:dict];
                }
                
                NSString *destPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
                destPath = [destPath stringByAppendingPathComponent:@"makesmodels.plist"];
                [storedcontent writeToFile:destPath atomically:YES];
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"MakesAndModels"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
}

- (BOOL) updateApp {
    BOOL update = FALSE;
    NSString *url = [NSString stringWithFormat:@"%@version", [[SystemPrefs instance] getURL]];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30.0];
    // Set all cookies
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:[cookieJar cookies]];
    [urlRequest setAllHTTPHeaderFields:headers];
    [urlRequest setHTTPShouldHandleCookies:YES];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    
    if ([error code] == kCFURLErrorNotConnectedToInternet)
    {
        return update;
    }
    
    JSONDecoder *jsonKitDecoder = [JSONDecoder decoderWithParseOptions:JKParseOptionStrict];
    NSDictionary *results = [jsonKitDecoder objectWithData:data error:&error];
    NSString *version = [results objectForKey:@"version"];
    
    NSString *defaultsVersion = [[NSUserDefaults standardUserDefaults] stringForKey:@"version"];
    if (![version isEqualToString:defaultsVersion]) {
        update = TRUE;
        [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"version"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    return update;
}

- (void) monitor {
    NSURL *baseURL = [NSURL URLWithString:@"https://hikervalet.com/valet/api/v1/version"];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                [operationQueue setSuspended:YES];
                break;
        }
    }];
    
    [manager.reachabilityManager startMonitoring];
}

# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
}

- (void) socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveMessage >>> data: %@", packet.data);
}

// event delegate
- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveEvent >>> data: %@", packet.data);
    
    if ([packet.name isEqualToString:@"statusUpdated"]) {
        NSData *data = [packet.data dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSArray *test = json;
        NSDictionary *ticket = [test objectAtIndex:1];
        [self statusUpdated:ticket];
    }
}

- (void) statusUpdated:(NSDictionary*) ticket {
    NSString *status = [[ticket objectForKey:@"status"] stringValue];
    NSString *ticket_id = [[ticket objectForKey:@"ticket_id"] stringValue];
    
    if (![[ValetStorage sharedModel].ticketQueues objectForKey:status]) {
        [[ValetStorage sharedModel].ticketQueues setObject:[[NSMutableDictionary alloc] init] forKey:status];
    }
    
    Vehicle *v = [[Vehicle alloc] initWithVehicle:[ticket objectForKey:@"make"]
                                            model:[ticket objectForKey:@"model"]
                                            color:[ticket objectForKey:@"color"]];
    v.vehicleId = [[ticket objectForKey:@"vehicle_id"] stringValue];
    v.license = [ticket objectForKey:@"license"];
    //NSString *licenseImage = [ticket objectForKey:@"licenseImage"];
    //if ([licenseImage length] > 0) {
    //    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:licenseImage options:0];
    //    v.licenseImage = [UIImage imageWithData:imageData];
    //}
    
    Driver *d = [[Driver alloc] init];
    d.firstName = [ticket objectForKey:@"firstname"];
    d.lastName = [ticket objectForKey:@"lastname"];
    d.emailAddress = [ticket objectForKey:@"email"];
    d.phone = [ticket objectForKey:@"phone"];
    
    int oldStatus = [status intValue];
    if (oldStatus >= 1) {
        oldStatus = oldStatus-1;
        NSMutableDictionary *oldstatusdict = [[ValetStorage sharedModel].ticketQueues objectForKey:[NSString stringWithFormat:@"%d", oldStatus]];
        [oldstatusdict removeObjectForKey:ticket_id];
    }
    
    NSMutableDictionary *statusdict = [[ValetStorage sharedModel].ticketQueues objectForKey:status];
    Ticket *tick = [[Ticket alloc] initWithTicket:ticket_id status:status vehicle:v driver:d];
    [statusdict setObject:tick forKey:ticket_id];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTickets" object:self];
}

- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    if ([error code] == SocketIOUnauthorized) {
        NSLog(@"not authorized");
    } else {
        NSLog(@"onError() %@", error);
    }
    [socketIO connectToHost:@"node.hikervalet.com" onPort:25988];
}


- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
    [socketIO connectToHost:@"node.hikervalet.com" onPort:25988];
}


- (void) updateStatus:(NSNotification *) notification {
    NSDictionary *dict = [notification userInfo];
    
    /*NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:@"1249" forKey:@"status"];
    [dict setObject:@"1" forKey:@"ticketid"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateStatus" object:nil userInfo:dict];
     */
    
    [socketIO sendEvent:@"status" withData:dict];
}

- (void) driverinRange:(NSNotification *) notification {
    NSDictionary *dict = [notification userInfo];
    
    /*NSMutableDictionary *dict = [NSMutableDictionary dictionary];
     [dict setObject:@"1249" forKey:@"valet_id"];
     [dict setObject:@"1" forKey:@"driver_id"];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"updateStatus" object:nil userInfo:dict];
     */
    
    [socketIO sendEvent:@"status" withData:dict];
}

@end
