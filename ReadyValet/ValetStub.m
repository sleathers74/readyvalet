//
//  ValetStub.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/16/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetStub.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "MainProfile.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ValetStub ()

@end

@implementation ValetStub

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Requested Vehicle"];
    
    self.paidAmountLabel.text = [NSString stringWithFormat:@"Paid $%@", self.paidAmt];
    self.ticketLabel.text = [ValetStorage sharedModel].currentTicketNumber;
    self.carLabel.text = [ValetStorage sharedModel].currentVehicleName;
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    UIBarButtonItem *settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)touchDone:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}

@end
