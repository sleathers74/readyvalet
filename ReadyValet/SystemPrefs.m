//
//
//  Created by Scott Leathers on 3/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "SystemPrefs.h"

// Google
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@implementation SystemPrefs

@synthesize url;
@synthesize secureUrl;
@synthesize full_url;

static SystemPrefs *_instance = nil;

+(SystemPrefs *)instance
{
    if (!_instance) {
        @synchronized([SystemPrefs class])
        {
            if (!_instance) {
                _instance = [self new];
            }
        }
    }
    return _instance;
}

-(id)init
{
    self = [super init];
    if (self != nil) {
        url = @"https://hikervalet.com";
        full_url = [NSString stringWithFormat:@"%@/valet/api/v1/", url];
        secureUrl = @"https://hikervalet.com";
    }
    
    return self;
}

-(NSString*) getURL
{
    return full_url;
}

- (void) sendGoogleView:(NSString*) title {
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:title];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void) sendGoogleEvent:(NSString*) category action:(NSString*) action label:(NSString*) label {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:category
                                                          action:action
                                                           label:label
                                                           value:nil] build]];
}

-(void)dealloc
{
    //[super dealloc];
    self.url = nil;
    self.full_url = nil;
    self.secureUrl = nil;
}

@end