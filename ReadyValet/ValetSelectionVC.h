//
//  ValetSelectionVC.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface ValetSelectionVC : UIViewController  {
    UIBarButtonItem         *settingsButton;
}
@property (strong, nonatomic) IBOutlet UITableView *valetTableView;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) NSMutableArray *listDetails;
@property (weak, nonatomic) NSTimer *refreshTimer;

@end
