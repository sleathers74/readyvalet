//
//  MakeSearchCells
//
//  Created by Scott Leathers on 8/20/13.
//  Copyright (c) 2013 Scott Leathers. All rights reserved.
//

#import "MakeSearchCells.h"

@implementation MakeSearchCells

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
