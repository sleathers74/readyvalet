//
//  ReceiptController.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ReceiptController.h"
#import "ReceiptCell.h"
#import "UtilFunctions.h"
#import "ReceiptsDetail.h"
#import "SystemPrefs.h"

@interface ReceiptController ()

@end

@implementation ReceiptController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listDetails = [[NSMutableArray alloc] init];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    [self.waitView setHidden:FALSE];
    [self getReceipts];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)cancel {
    [self.waitView setHidden:TRUE];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.listDetails count] > 0)
        return [self.listDetails count];
    else
        return 1; // This should be changed for the future.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ReceiptCell";
    
    ReceiptCell *cell = (ReceiptCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if ([self.listDetails count] > 0) {
        NSDictionary *transDict = [self.listDetails objectAtIndex:indexPath.row];
        [cell.amtLabel setText:[NSString stringWithFormat:@"$%@", [transDict objectForKey:@"amount"]]];
        [cell.dateLabel setText:[transDict objectForKey:@"created"]];
        [cell.companyLabel setText:[transDict objectForKey:@"valet_name"]];
        [cell.companyLabel setTextAlignment:NSTextAlignmentLeft];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else {
        [cell.dateLabel setText:@""];
        [cell.companyLabel setTextAlignment:NSTextAlignmentCenter];
        [cell.companyLabel setText:@"No Transactions"];
        [cell.amtLabel setText:@""];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.listDetails count] > 0) {
        NSDictionary *detailDict = [self.listDetails objectAtIndex:indexPath.row];
        
        ReceiptsDetail *vs = [[ReceiptsDetail alloc] initWithNibName:@"ReceiptsDetail" bundle:nil];
        //vs.detailInfo = detailDict;
        vs.trans_id = [[self.listDetails objectAtIndex:indexPath.row] objectForKey:@"trans_id"];
        //[vs.amountLabel setText:[NSString stringWithFormat:@"$%@", [detailDict objectForKey:@"amount"]]];
        [self.navigationController pushViewController:vs animated:TRUE];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) getReceipts {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@hikertransactions", [[SystemPrefs instance] getURL]];
    [self.apiManager GET:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.listDetails removeAllObjects];

        for (int iarray = 1; iarray <= [responseObject count]; iarray++) {
            [self.listDetails addObject:[responseObject objectForKey:[NSString stringWithFormat:@"%d", iarray]]];
        }
        
        [self.receiptTable reloadData];
        [self.waitView setHidden:TRUE];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
        [self.waitView setHidden:TRUE];
    }];
}

@end
