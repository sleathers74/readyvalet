//
//  Vehicle.m
//  Valet
//
//

#import "ValetCompany.h"

@implementation ValetCompany

- (id) initWithName:(NSString*)company_name {
    self = [super init];
    self.company = company_name;
    return self;
}

- (void) StringToImage:(NSString*) imageString {
    if ([imageString isKindOfClass:[NSString class]]) {
        if ([imageString length] > 0) {
            NSData* imageData = [[NSData alloc] initWithBase64EncodedString:imageString options:0];
            [self setProfile_image:[UIImage imageWithData:imageData]];
        }
    }
}

- (NSString*) ImageToString {
    NSString *baseString = @"";
    if (self.profile_image) {
        NSData *imageData = UIImageJPEGRepresentation(self.profile_image, 0.1);
        baseString = [imageData base64Encoding];
    }
    return baseString;
}
@end
