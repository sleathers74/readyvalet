//
//  MainProfile.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "MainProfile.h"
#import "VehicleSelection.h"
#import "BTMainCC.h"
#import "UtilFunctions.h"
#import "myPaymentMethodVC.h"
#import "CreditCardProcessing.h"
#import "UserProfile.h"
#import "ValetStorage.h"
#import "Driver.h"
#import "ReceiptController.h"
#import "SettingsProfile.h"
#import "AFNetworking.h"
#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"
#import "SystemPrefs.h"


@interface MainProfile ()

@end

@implementation MainProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileUpdated:)
                                                 name:@"profileUpdated"
                                               object:nil];
    
    //UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithTitle:@"\u2699" style:UIBarButtonItemStylePlain target:self action:@selector(showAccountSettings:)];
    
    UIFont *customFont = [UIFont fontWithName:@"Helvetica" size:24.0];
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:customFont, NSFontAttributeName, nil];
    
    //[settingsButton setTitleTextAttributes:dict forState:UIControlStateNormal];
    
    //self.navigationItem.leftBarButtonItem = settingsButton;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    //[self.buttonBackground setBackgroundColor:[UIColor colorWithRed: (0.0f/255.0f) green:(34.0f/255.0f) blue:(85.0f/255.0f) alpha:1.0]];
    
    self.profileImageView.layer.borderWidth = 1.7f;
    self.profileImageView.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2.0;
    //self.profileImageView.layer.cornerRadius = 10.0f;
    
    self.profileImageView.clipsToBounds = YES;
    
    Driver *d = [ValetStorage sharedModel].currentDriver;
    if (d != nil) {
        [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@", d.firstName, d.lastName]];
    
        if (d.driverImage)
            self.profileImageView.image = d.driverImage;
        //[self getProfileImage];
    }
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    
    NIKFontAwesomeIcon icon = NIKFontAwesomeIconLocationArrow;
    [self.findValetButton setImage:[factory createImageForIcon:icon] forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon paymenticon = NIKFontAwesomeIconCreditCard;
    [self.paymentButton setImage:[factory createImageForIcon:paymenticon] forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon caricon = NIKFontAwesomeIconCar;
    [self.vehicleButton setImage:[factory createImageForIcon:caricon] forState:UIControlStateNormal];

    NIKFontAwesomeIcon printicon = NIKFontAwesomeIconPrint;
    [self.receiptsButton setImage:[factory createImageForIcon:printicon] forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon settingsicon = NIKFontAwesomeIconGear;
    [self.settingsButton setImage:[factory createImageForIcon:settingsicon] forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchValet:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)touchPayment:(id)sender {
    myPaymentMethodVC *selectPaymentMethod = [[myPaymentMethodVC alloc] init];
    selectPaymentMethod.title = @"Test";
    selectPaymentMethod.theme = [BTUI braintreeTheme];
    selectPaymentMethod.paymentMethods = [CreditCardProcessing sharedModel].paymentMethods;
    //selectPaymentMethod.selectedPaymentMethodIndex = 0;
    selectPaymentMethod.delegate = self;
    selectPaymentMethod.client = [CreditCardProcessing sharedModel].braintree.client;
    [self.navigationController pushViewController:selectPaymentMethod animated:TRUE];
}

- (void)selectPaymentMethodViewController:(myPaymentMethodVC *)viewController
            didSelectPaymentMethodAtIndex:(NSUInteger)index {
    
}

- (void)selectPaymentMethodViewControllerDidRequestNew:(myPaymentMethodVC *)viewController {
    BTMainCC *bt = [[BTMainCC alloc] init];
    [self.navigationController pushViewController:bt animated:TRUE];
    
}

- (IBAction)touchVehicle:(id)sender {
    VehicleSelection *vs = [[VehicleSelection alloc] initWithNibName:@"VehicleSelection" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (IBAction)touchProfile:(id)sender {
    UserProfile *vs = [[UserProfile alloc] initWithNibName:@"UserProfile" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (IBAction)touchBottomButton:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil]; 
}

- (IBAction)touchReceipts:(id)sender {
    ReceiptController *vs = [[ReceiptController alloc] initWithNibName:@"ReceiptController" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (IBAction)touchSettings:(id)sender {
    SettingsProfile *vs = [[SettingsProfile alloc] initWithNibName:@"SettingsProfile" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

-(void)showAccountSettings:(id) sender {
    UserProfile *vs = [[UserProfile alloc] initWithNibName:@"UserProfile" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (void) profileUpdated:(NSNotification *) notification {
    Driver *d = [ValetStorage sharedModel].currentDriver;
    if (d != nil) {
        [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@", d.firstName, d.lastName]];
        
        if (d.driverImage)
            self.profileImageView.image = d.driverImage;
    }
}

- (void) getProfileImage {
    AFHTTPRequestOperationManager *apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@getImage", [SystemPrefs instance].getURL];
    [apiManager POST:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject objectForKey:@"image"] length] > 0) {
            NSData* imageData = [[NSData alloc] initWithBase64EncodedString:[responseObject objectForKey:@"image"] options:0];
            self.profileImageView.image = [UIImage imageWithData:imageData];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];


}


@end
