//
//  TicketView.m
//  ReadyValet
//
//  Created by Scott Leathers on 8/12/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "TicketView.h"
#import "AFNetworking.h"
#import "ValetStorage.h"
#import "WaitView.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

@interface TicketView ()

@end

@implementation TicketView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    //self.view.backgroundColor = [UIColor colorWithRed:(37/255.0) green:(36/255.0) blue:(36/255.0) alpha:1];
    
    WaitView *wv = [[WaitView alloc] initWithNibName:@"WaitView" bundle:nil];
    [self.view addSubview:wv.view];
    
    [self generateTicket];
    
    [wv.view removeFromSuperview];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.navigationController popToRootViewControllerAnimated:true];
}

-(void) generateTicket {
    AFHTTPRequestOperationManager *apiManager = [AFHTTPRequestOperationManager manager];
    //[apiManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[ValetStorage sharedModel].username password:[ValetStorage sharedModel].password];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSLog(@"JSON: %@", [ValetStorage sharedModel].currentVehicleId);
    NSString *url = [NSString stringWithFormat:@"%@ticket/%@", [[SystemPrefs instance] getURL], [ValetStorage sharedModel].currentVehicleId];
    [apiManager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSString *ticket_id = [NSString stringWithFormat:@"%d", [[responseObject objectForKey:@"ticket_id"] integerValue]];
        [ValetStorage sharedModel].currentTicketNumber = ticket_id;
        
        self.vehicleLabel.text = [ValetStorage sharedModel].currentVehicleName;
        self.ticketLabel.text = ticket_id;
        [ValetStorage sharedModel].currentVehicleStatus = 2;
        
        self.waitView.hidden = TRUE;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.waitView.hidden = TRUE;
        [UtilFunctions displayError:self msg:error.localizedDescription];
        NSLog(@"Error: %@", error);
    }];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchDone:(id)sender {
    [self.navigationController popViewControllerAnimated:FALSE];
}
@end
