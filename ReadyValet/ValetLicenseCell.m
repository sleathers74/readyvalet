//
//  LoginViewCell.m
//  IllinoisPrepScores
//
//  Created by Scott Leathers on 7/31/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetLicenseCell.h"

@implementation ValetLicenseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
