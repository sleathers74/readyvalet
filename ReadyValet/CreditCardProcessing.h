//
//  CreditCardProcessing.h
//
//  Created by Scott Leathers on 10/4/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Braintree.h"

@interface CreditCardProcessing : NSObject

@property (strong, nonatomic)   Braintree *braintree;
@property (nonatomic,strong)    NSString *clientToken;
@property (nonatomic,strong)    NSArray *paymentMethods;

+ (CreditCardProcessing*)sharedModel;
- (void) loadBriantree;
- (void) fetchPaymentMethods;
- (void)postNonceToServer:(NSString *)paymentMethodNonce;

@end