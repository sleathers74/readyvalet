//
//  MainLoginView.h
//  ReadyValet
//
//  Created by Scott Leathers on 8/10/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "BSKeyboardControls.h"

@interface MainLoginView : UIViewController <BSKeyboardControlsDelegate, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)touchCreateAccount:(id)sender;


@end
