//
//  Ticket.m
//  Valet
//
//  Created by Matthew Schmidgall on 2/17/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import "Ticket.h"

@implementation Ticket

- (id) initWithTicket:(NSString *)ticketId status:(NSString *)status vehicle:(Vehicle*)vehicle {
    self = [super init];
    self.ticketNumber = ticketId;
    self.status = status;
    self.vehicle = vehicle;
    self.driver = nil;
    return self;
    
}

- (id) initWithTicket:(NSString*)ticketId status:(NSString*)status vehicle:(Vehicle*)vehicle driver:(Driver*)driver {
    self = [super init];
    self.ticketNumber = ticketId;
    self.status = status;
    self.vehicle = vehicle;
    self.driver = driver;
    return self;
}
@end
