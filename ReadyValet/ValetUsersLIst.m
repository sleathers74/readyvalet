//
//  VehicleSelection.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetUsersList.h"
#import "MakeSearch.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "Vehicle.h"
#import "CreateAccountView4.h"
#import "ValetStorage.h"
#import "ValetCompany.h"
#import "ValetUser.h"
#import "ValetUserProfile.h"
#import "SystemPrefs.h"

@interface ValetUsersList ()

@end

@implementation ValetUsersList

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:FALSE];
    
    [self setSelectedCell:0];
    
    self.userArray = [[NSMutableArray alloc] init];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    self.factory.colors = @[[NIKColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    [self.valetTable setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    
    [self setupLayout];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self getValetUsers];
}

- (void) setupLayout {
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
}

- (void) cancel {
    [self.navigationController popViewControllerAnimated:TRUE]; 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(__unused UITableView *)tableView numberOfRowsInSection:(__unused NSInteger)section
{
    return (self.userArray ? [self.userArray count] + 1 : 1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@" Valet Runners" forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon icon = NIKFontAwesomeIconUsers;
    [button setImage:[self.factory createImageForIcon:icon] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    button.frame = CGRectMake(15, 0, tableView.frame.size.width, 44);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [view addSubview:button];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *paymentMethodCellIdentifier = @"paymentMethodCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:paymentMethodCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:paymentMethodCellIdentifier];
    }
    cell.backgroundColor = [UIColor whiteColor];
    if ([self.userArray count] == 0 || indexPath.row > [self.userArray count] - 1) {
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:@"Add Valet Runner"];
        [typeWithDescription addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f] range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;

        NIKFontAwesomeIcon icon = NIKFontAwesomeIconPlus;
        UIImage *image = [self.factory createImageForIcon:icon];
        cell.imageView.contentMode = UIViewContentModeCenter;
        cell.imageView.image = image;
        cell.accessoryView = nil;
    }
    else {
        ValetUser *v = [self.userArray objectAtIndex:indexPath.row];
        cell.accessoryView = nil;
        
        NSString *vString;
        if ([v.first_name length] > 0) {
            vString = [NSString stringWithFormat:@"%@ %@", v.first_name, v.last_name];
        }
        else {
            vString = [NSString stringWithFormat:@"%@", v.username];
        }
        
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:vString];
        [typeWithDescription addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f] range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;
        cell.imageView.image = nil;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(__unused UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.manageUsersBarButton.enabled = FALSE;
    self.editProfileBarButton.enabled = FALSE;
    ValetUserProfile *vs = [[ValetUserProfile alloc] initWithNibName:@"ValetUserProfile" bundle:nil];
    if ([self.userArray count] > 0 && indexPath.row <= ([self.userArray count] - 1)) {
        ValetUser *vu = [self.userArray objectAtIndex:indexPath.row];
        vs.valetUser = vu;
        self.selectedCell = indexPath.row;
        self.manageUsersBarButton.enabled = TRUE;
        self.editProfileBarButton.enabled = TRUE;
    }
    vs.valetCompany = self.valetCompany;
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (indexPath.row > [self.userArray count])
        return NO;
    else
        return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteUser:indexPath];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        ValetUser *v = [self.userArray objectAtIndex:actionSheet.tag];
        [self removeUser:v.user_id];
        [self.userArray removeObjectAtIndex:actionSheet.tag];
        [self.valetTable reloadData];
    }
}

-(void) getValetUsers {
        self.apiManager = [AFHTTPRequestOperationManager manager];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *username = [defaults stringForKey:@"username"];
        NSString *password = [defaults stringForKey:@"password"];
        
        NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  username, @"username",
                                  password, @"password",
                                  nil];
    
        NSString *url = [NSString stringWithFormat:@"%@getValetUsers/%@", [SystemPrefs instance].getURL, self.valetCompany.valet_id];
        [self.apiManager POST:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            [self.userArray removeAllObjects];
            
            for (NSString *valetid in responseObject) {
                
                NSDictionary *valet = [responseObject objectForKey:valetid];
                ValetUser *v = [[ValetUser alloc] initWithName:[valet objectForKey:@"first_name"] last_name:[valet objectForKey:@"last_name"]];
                
                [v setFirst_name:[valet objectForKey:@"first_name"]];
                [v setLast_name:[valet objectForKey:@"last_name"]];
                [v setUser_id:[valet objectForKey:@"user_id"]];
                [v setUsername:[valet objectForKey:@"username"]];
                [v setPassword:[valet objectForKey:@"password"]];
                
                NSString *profile_image = [valet objectForKey:@"profile_image"];
                if ([profile_image length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:profile_image options:0];
                    [v setProfile_image:[UIImage imageWithData:imageData]];
                }
                
                [self.userArray addObject:v];
                [self.valetTable reloadData];
            }
            [self.waitView setHidden:TRUE];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [self.waitView setHidden:TRUE];
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }];
}

- (void) deleteUser:(NSIndexPath *)indexPath {
    ValetUser *v = [self.userArray objectAtIndex:indexPath.row];
    NSString *valetMsg = [NSString stringWithFormat:@"Are you sure you want to delete valet runner '%@ %@'?", v.first_name, v.last_name];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:valetMsg
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Delete",nil];
    actionSheet.tag = indexPath.row;
    [actionSheet showInView:self.view];
}

-(void) removeUser:(NSString*) userId {
    [self.waitView setHidden:FALSE];
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@deleteuser/%@", [SystemPrefs instance].getURL, userId];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.waitView setHidden:TRUE];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
        [self.waitView setHidden:TRUE];
    }];
}
@end
