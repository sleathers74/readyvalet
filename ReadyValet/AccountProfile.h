//
//
//  Created by Scott Leathers on 3/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

@interface AccountProfile:NSObject {
    NSString				*firstName;
    NSString                *lastName;
    NSString                *username;
    NSString                *password;
    NSString                *creditCardNum;
    NSString                *securityCode;
    NSString                *expirationDate;
    NSString                *make;
    NSString                *model;
    NSString                *color;
    NSString                *license;
    NSInteger               autoDetect;
    UIImage                 *licenseImage;
    UIImage                 *driverImage;
}

+(AccountProfile *) instance;
-(id)init;

@property (nonatomic, strong) NSString   			*firstName;
@property (nonatomic, strong) NSString   			*lastName;
@property (nonatomic, strong) NSString   			*username;
@property (nonatomic, strong) NSString   			*password;
@property (nonatomic, strong) NSString   			*creditCardNum;
@property (nonatomic, strong) NSString   			*securityCode;
@property (nonatomic, strong) NSString   			*expirationDate;
@property (nonatomic, strong) NSString   			*make;
@property (nonatomic, strong) NSString   			*model;
@property (nonatomic, strong) NSString   			*color;
@property (nonatomic, strong) NSString   			*license;
@property (strong, nonatomic)  UIImage               *licenseImage;
@property (strong, nonatomic)  UIImage               *driverImage;
@property NSInteger autoDetect;

@end