//
//  AppDelegate.h
//  ReadyValet
//
//  Created by Scott Leathers on 7/21/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "MainLoginView.h"
#import "DriverTabViewController.h"
#import "SplashView.h"
#import "SocketIO.h"

@class LoginView;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CBCentralManagerDelegate, SocketIODelegate> {
    SocketIO *socketIO;
    
}

@property (strong, nonatomic) UIWindow *window;


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;

@property (strong, nonatomic) LoginView *loginView;
@property (strong, nonatomic) MainLoginView *mainLoginView;
@property (strong, nonatomic) DriverTabViewController *driverTabViewController;
@property (nonatomic, strong) CBCentralManager* bluetoothManager;
@property (strong, nonatomic) SplashView         *splashScreen;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void) displayMainDriverView;
- (void) displayMainValetView;
@end
