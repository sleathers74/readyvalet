//
//  ConfirmTicket.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/15/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ConfirmTicket.h"
#import "ValetStorage.h"
#import "ParkedVehicle.h"
#import "UtilFunctions.h"
#import "MainProfile.h"
#import "SystemPrefs.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ConfirmTicket ()

@end

@implementation ConfirmTicket

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[SystemPrefs instance] sendGoogleView:@"Driver - Check-In"];
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    self.ticketNumber.text = [ValetStorage sharedModel].currentTicketNumber;
    self.companyName.text = [ValetStorage sharedModel].currentValetName;
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Check-In"];
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    UIBarButtonItem *settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchOK:(id)sender {
    ParkedVehicle *ct = [[ParkedVehicle alloc] initWithNibName:@"ParkedVehicle" bundle:nil];
    ct.title = @"";
    [self.navigationController pushViewController:ct animated:TRUE];
}

@end
