//
//

#import <Foundation/Foundation.h>

@interface ValetUser : NSObject

@property (strong,nonatomic) NSString *user_id;
@property (strong,nonatomic) NSString *first_name;
@property (strong,nonatomic) NSString *last_name;
@property (strong,nonatomic) NSString *username;
@property (strong,nonatomic) NSString *password;
@property (strong,nonatomic) UIImage *profile_image;

- (id) initWithName:(NSString*)first_name last_name:(NSString*) last_name;
@end
