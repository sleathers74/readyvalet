//
//  ChangeStatus.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/1/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ticket.h"
#import "AFNetworking.h"

@interface ChangeStatus : UIViewController {
}
@property (strong, nonatomic) IBOutlet UILabel *vehicleDesc;
@property (strong, nonatomic) IBOutlet UILabel *ticketId;
@property (strong, nonatomic) Ticket *ticket;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
- (IBAction)touchedSubmit:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *vehicleTicketImage;
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet UIButton *statusButton;
@end
