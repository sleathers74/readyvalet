//
//  MainLoginView.m
//  ReadyValet
//
//  Created by Scott Leathers on 8/10/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "MainLoginView.h"
#import "CreateAccountView.h"
#import "TermsView.h"
#import "AppDelegate.h"
#import "WaitView.h"
#import "ValetStorage.h"
#import <QuartzCore/QuartzCore.h>
#import "BSKeyboardControls.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"


// Google
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface MainLoginView ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end

@implementation MainLoginView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [[SystemPrefs instance] sendGoogleView:@"Sign-In"];
    
    [UtilFunctions addLeftPrefix:self.username prefix:@"Email:"];
    [UtilFunctions addLeftPrefix:self.password prefix:@"Password:"];
    
    /*UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.username.leftView = paddingView;
    self.username.leftViewMode = UITextFieldViewModeAlways;
    paddingView = nil;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.password.leftView = paddingView2;
    self.password.leftViewMode = UITextFieldViewModeAlways;
    paddingView2 = nil;*/
    
    self.scrollView.scrollEnabled = TRUE;
    self.scrollView.contentSize = CGSizeMake(320, 568);
    
    NSArray *fields = @[ self.username, self.password];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    //[self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.keyboardControls setActiveField:textField];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:TRUE];
}

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view = keyboardControls.activeField.superview.superview;
    [self.scrollView scrollRectToVisible:view.frame animated:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];//Dismiss the keyboard.
    
    //[self touchCreate:0];
    //Add action you want to call here.
    return YES;
}

- (IBAction)touchLogin:(id)sender {
    [self.username resignFirstResponder];
    [self.password resignFirstResponder];
    
    if ([self.username.text length] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter a username." message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    else if ([self.password.text length] <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter a password." message:nil delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    WaitView *wv = [[WaitView alloc] initWithNibName:@"WaitView" bundle:nil];
    wv.view.frame = self.view.bounds;
    [wv.view setAutoresizingMask: UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [self.view addSubview:wv.view];
    
    self.apiManager = [AFHTTPRequestOperationManager manager];

    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              self.username.text, @"username",
                              self.password.text, @"password",
                              nil];

    [self.apiManager GET:@"https://hikervalet.com/valet/api/v1/profile" parameters:postData
                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
                     
        if (responseObject) {
            [ValetStorage sharedModel].username = self.username.text;
            [ValetStorage sharedModel].password = self.password.text;
            
            [[ValetStorage sharedModel] saveLogin];
            
            // Store settings in user defaults
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"loggedin"];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"role"] forKey:@"role"];
            [[NSUserDefaults standardUserDefaults] setInteger:[[responseObject objectForKey:@"auto_detect"] integerValue] forKey:@"autodetect"];
            [[NSUserDefaults standardUserDefaults] setObject:[ValetStorage sharedModel].username forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setObject:[ValetStorage sharedModel].password forKey:@"password"];
            
            [ValetStorage sharedModel].role = [responseObject objectForKey:@"role"];
            
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:@"&uid" value:self.username.text];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Main Sign-In View"
                                                                  action:@"sign-in"
                                                                   label:nil
                                                                   value:nil] build]];
            
            [[SystemPrefs instance] sendGoogleEvent:@"User" action:@"Role" label:[responseObject objectForKey:@"role"]];
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if ([(NSString*)[responseObject objectForKey:@"role"] isEqualToString:@"driver"]) {
                [ValetStorage sharedModel].profileId = [[responseObject objectForKey:@"driver"] objectForKey:@"driver_id"];
                [ValetStorage sharedModel].currentVehicleId = [[[responseObject objectForKey:@"vehicle"] allKeys] objectAtIndex:0];
                Driver *d = [[Driver alloc] init];
                d.emailAddress = [[responseObject objectForKey:@"driver"] objectForKey:@"email"];
                d.firstName = [[responseObject objectForKey:@"driver"] objectForKey:@"first_name"];
                d.lastName = [[responseObject objectForKey:@"driver"] objectForKey:@"last_name"];
                d.phone = [[responseObject objectForKey:@"driver"] objectForKey:@"phone"];
                d.defaultTip = [[responseObject objectForKey:@"driver"] objectForKey:@"defaultTip"];
                
                NSString *driverImage = [[responseObject objectForKey:@"driver"] objectForKey:@"driverImage"];
                if ([driverImage length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:driverImage options:0];
                    d.driverImage = [UIImage imageWithData:imageData];
                }
                
                [ValetStorage sharedModel].autoDetect = [[[responseObject objectForKey:@"driver"] objectForKey:@"auto_detect"] integerValue];
                NSString *vehicleId;
                NSDictionary *v;
                for (vehicleId in [responseObject objectForKey:@"vehicle"]) {
                    v = [[responseObject objectForKey:@"vehicle"] objectForKey:vehicleId];
                    Vehicle *newVehicle =[[Vehicle alloc] initWithVehicle:[v objectForKey:@"make"] model:[v objectForKey:@"model"] color:[v objectForKey:@"color"]];
                    newVehicle.license = [v objectForKey:@"license"];
                    newVehicle.vehicleId = vehicleId;
                    
                    if ([[v objectForKey:@"defaultVehicle"] isKindOfClass:[NSString class]])
                        newVehicle.vehicledefault = [v objectForKey:@"defaultVehicle"];
                    else if ([[v objectForKey:@"defaultVehicle"] isKindOfClass:[NSNumber class]])
                        newVehicle.vehicledefault = [[v objectForKey:@"defaultVehicle"] stringValue];
                    else
                        newVehicle.vehicledefault = @"0";
                    
                    NSString *licenseImage = [v objectForKey:@"licenseImage"];
                    if ([licenseImage length] > 0) {
                        NSData* imageData = [[NSData alloc] initWithBase64EncodedString:licenseImage options:0];
                        newVehicle.licenseImage = [UIImage imageWithData:imageData];
                    }
                    
                    [d.vehicles addObject:newVehicle];
                    if ([newVehicle.vehicledefault isEqualToString:@"1"]) {
                        [ValetStorage sharedModel].currentVehicle = newVehicle;
                        [ValetStorage sharedModel].currentVehicleId = newVehicle.vehicleId;
                    }
                }
                if ([responseObject objectForKey:@"valet"]) {
                    NSDictionary *valetDict = [responseObject objectForKey:@"valet"];
                    [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_id"];
                    [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company"];
                    [ValetStorage sharedModel].currentValetFee = [valetDict objectForKey:@"valet_fee"];
                    [ValetStorage sharedModel].currentServiceFee = [valetDict objectForKey:@"service_fee"];
                    [ValetStorage sharedModel].currentMerchantId = [valetDict objectForKey:@"merchant_id"];
                    [ValetStorage sharedModel].currentMerchantStatus = [valetDict objectForKey:@"merchant_status"];
                }
                
                [ValetStorage sharedModel].currentDriver = d;
                [wv.view removeFromSuperview];
                [appDelegate displayMainDriverView];
            }
            else if ([(NSString*)[responseObject objectForKey:@"role"] isEqualToString:@"valet"]) {
                [wv.view removeFromSuperview];
                [ValetStorage sharedModel].profileId = [[responseObject objectForKey:@"valet"] objectForKey:@"valet_id"];
                NSDictionary *valetDict = [responseObject objectForKey:@"valet"];
                [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_id"];
                [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company"];
                [ValetStorage sharedModel].currentValetFee = [valetDict objectForKey:@"valet_fee"];
                [ValetStorage sharedModel].currentServiceFee = [valetDict objectForKey:@"service_fee"];
                [ValetStorage sharedModel].currentMerchantId = [valetDict objectForKey:@"merchant_id"];
                [ValetStorage sharedModel].currentMerchantStatus = [valetDict objectForKey:@"merchant_status"];
                [appDelegate displayMainValetView];
            }
            else {
                [wv.view removeFromSuperview];
                
                NSDictionary *valetDict = [responseObject objectForKey:@"valet_owner"];
                [ValetStorage sharedModel].profileId = [valetDict objectForKey:@"valet_owner_id"];
                [[ValetStorage sharedModel].valetOwner setCompany_name:[valetDict objectForKey:@"company_name"]];
                [[ValetStorage sharedModel].valetOwner setOwner_id:[valetDict objectForKey:@"valet_owner_id"]];
                [[ValetStorage sharedModel].valetOwner setFirst_name:[valetDict objectForKey:@"first_name"]];
                [[ValetStorage sharedModel].valetOwner setLast_name:[valetDict objectForKey:@"last_name"]];
                [[ValetStorage sharedModel].valetOwner setPhone_number:[valetDict objectForKey:@"phone_number"]];
                NSString *company_logo = [valetDict objectForKey:@"company_logo"];
                if ([company_logo length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:company_logo options:0];
                    [ValetStorage sharedModel].valetOwner.company_logo = [UIImage imageWithData:imageData];
                }
                
                [appDelegate displayMainValetView];
            }
        }
        else {
            [wv.view removeFromSuperview];
            [UtilFunctions displayError:self msg:@"Sign-In Failed. Please try again."];
        }
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [wv.view removeFromSuperview];
        if (operation.response.statusCode == 401) {
            [wv.view removeFromSuperview];
            [UtilFunctions displayError:self msg:@"Sign-In Failed. Please try again."];
        } else {
            [wv.view removeFromSuperview];
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchCreateAccount:(id)sender {
    CreateAccountView *lv = [[CreateAccountView alloc] init];
    lv.title = @"Login Info";
    [self.navigationController pushViewController:lv animated:TRUE];
}
@end
