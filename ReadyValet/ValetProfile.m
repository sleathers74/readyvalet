//
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetProfile.h"
#import "VehicleSelection.h"
#import "BTMainCC.h"
#import "UtilFunctions.h"
#import "myPaymentMethodVC.h"
#import "CreditCardProcessing.h"
#import "UserProfile.h"
#import "ValetStorage.h"
#import "Driver.h"
#import "ReceiptController.h"
#import "SettingsProfile.h"
#import "ValetOwnerList.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"


@interface ValetProfile ()

@end

@implementation ValetProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(profileUpdated:)
                                                 name:@"profileUpdated"
                                               object:nil];

    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.profileImageView.layer.borderWidth = 1.7f;
    self.profileImageView.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2.0;
    //self.profileImageView.layer.cornerRadius = 10.0f;
    
    self.profileImageView.clipsToBounds = YES;
    
    [self.nameLabel setText:[ValetStorage sharedModel].valetOwner.company_name];
    if ([ValetStorage sharedModel].valetOwner.company_logo) {
        self.profileImageView.image = [ValetStorage sharedModel].valetOwner.company_logo;
    }
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    
    NIKFontAwesomeIcon icon = NIKFontAwesomeIconLocationArrow;
    [self.findValetButton setImage:[factory createImageForIcon:icon] forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon valeticon = NIKFontAwesomeIconAutomobile;
    [self.manageButton setImage:[factory createImageForIcon:valeticon] forState:UIControlStateNormal];

    NIKFontAwesomeIcon printicon = NIKFontAwesomeIconPrint;
    [self.receiptsButton setImage:[factory createImageForIcon:printicon] forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon settingsicon = NIKFontAwesomeIconGear;
    [self.settingsButton setImage:[factory createImageForIcon:settingsicon] forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchValet:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)touchManage:(id)sender {
    ValetOwnerList *vol = [[ValetOwnerList alloc] init];
    
    [self.navigationController pushViewController:vol animated:TRUE];
}

- (void)selectPaymentMethodViewController:(myPaymentMethodVC *)viewController
            didSelectPaymentMethodAtIndex:(NSUInteger)index {
    
}

- (void)selectPaymentMethodViewControllerDidRequestNew:(myPaymentMethodVC *)viewController {
    BTMainCC *bt = [[BTMainCC alloc] init];
    [self.navigationController pushViewController:bt animated:TRUE];
    
}

- (IBAction)touchProfile:(id)sender {
    UserProfile *vs = [[UserProfile alloc] initWithNibName:@"UserProfile" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (IBAction)touchBottomButton:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil]; 
}

- (IBAction)touchReceipts:(id)sender {
    ReceiptController *vs = [[ReceiptController alloc] initWithNibName:@"ReceiptController" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (IBAction)touchSettings:(id)sender {
    SettingsProfile *vs = [[SettingsProfile alloc] initWithNibName:@"SettingsProfile" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

-(void)showAccountSettings:(id) sender {
    UserProfile *vs = [[UserProfile alloc] initWithNibName:@"UserProfile" bundle:nil];
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (void) profileUpdated:(NSNotification *) notification {
    [self.nameLabel setText:[ValetStorage sharedModel].valetOwner.company_name];
    if ([ValetStorage sharedModel].valetOwner.company_logo) {
        self.profileImageView.image = [ValetStorage sharedModel].valetOwner.company_logo;
    }
}


@end
