//
//  Vehicle.m
//  Valet
//
//  Created by Matthew Schmidgall on 1/5/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import "ValetOwner.h"

@implementation ValetOwner

- (id) initWithName:(NSString*)company_name {
    self = [super init];
    self.company_name = company_name;
    return self;
}
@end
