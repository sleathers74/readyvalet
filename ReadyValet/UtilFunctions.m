#import "UtilFunctions.h"
#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@implementation UtilFunctions

/*+ (UILabel*) buildCustomNav {
    NSString* titleText = @"HIKER";
    UIFont* titleFont = [UIFont fontWithName:@"Helvetica" size:22];
    CGSize requestedTitleSize = [titleText sizeWithFont:titleFont];
    CGFloat titleWidth = MIN(400, requestedTitleSize.width);
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont boldSystemFontOfSize:22];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text = titleText;
    return navLabel;
}*/

+ (void) addLeftPrefix:(UITextField*) tv prefix:(NSString*) prefix {
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(tv.frame.origin.x, tv.frame.origin.y, 85, tv.frame.size.height)];
    
    //paddingView.layer.borderColor = [UIColor redColor].CGColor;
    //paddingView.layer.borderWidth = 1.0f;
    
    UILabel *prefixLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 80, tv.frame.size.height)];
    prefixLabel.text = prefix;
    
    //prefixLabel.layer.borderColor = [UIColor redColor].CGColor;
    //prefixLabel.layer.borderWidth = 1.0f;
    prefixLabel.textColor = [UIColor blackColor];
    prefixLabel.textAlignment = NSTextAlignmentLeft;
    prefixLabel.contentMode = UIViewContentModeCenter;
    [prefixLabel setFont:[UIFont systemFontOfSize:14]];
    
    //CGRectMake(0, 0, tableView.frame.size.width, 44)
    
    //prefixLabel.frame = CGRectOffset(prefixLabel.frame, 0, -1);
    [prefixLabel sizeToFit];
    [prefixLabel setFrame:CGRectMake(5, 0, prefixLabel.frame.size.width+10, prefixLabel.frame.size.height)];
    prefixLabel.frame = CGRectOffset(prefixLabel.frame, 0, -1);
    paddingView.frame = CGRectMake(prefixLabel.frame.origin.x+5, prefixLabel.frame.origin.y, prefixLabel.frame.size.width, prefixLabel.frame.size.height);
    //paddingView.frame = CGRectOffset(paddingView.frame, 40, -1);
    
    [paddingView addSubview:prefixLabel];
    
    [tv setLeftView:paddingView];
    [tv setLeftViewMode:UITextFieldViewModeAlways];
    
    //[tv setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    //[tv setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
}

+ (UIView*) buildCustomNav {
    
    NSString* titleText = @"HIKER";
    UIFont* titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0f];
    CGSize requestedTitleSize = [titleText sizeWithFont:titleFont];
    CGFloat titleWidth = MIN(400, requestedTitleSize.width);

    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory textlessButtonIconFactory];
    factory.colors = @[[UIColor whiteColor]];
    
    UIImage *user = [factory createImageForIcon:NIKFontAwesomeIconCar];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:user];
    
    CGFloat newSize = titleWidth + imageView.bounds.size.width;
    
    imageView.frame = CGRectMake(titleWidth + 4, 3, imageView.frame.size.width, imageView.frame.size.height);
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, newSize, 20.0)];
    titleView.backgroundColor = [UIColor clearColor];
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleWidth, 20)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0f];//[UIFont boldSystemFontOfSize:22];
    
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text = titleText;
    
    [titleView addSubview:imageView];
    [titleView addSubview:navLabel];
    
    return titleView;
}

+ (UIView*) buildCustomNav:(NSString*) statusString {
    NSString* titleText = @"HIKER";
    
    UIFont* titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0f];
    CGSize requestedTitleSize = [titleText sizeWithFont:titleFont];
    CGFloat titleWidth = MIN(400, requestedTitleSize.width);
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory textlessButtonIconFactory];
    factory.colors = @[[UIColor whiteColor]];
    
    UIImage *user = [factory createImageForIcon:NIKFontAwesomeIconCar];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:user];
    
    CGFloat newSize = titleWidth + 20 + imageView.bounds.size.width;
    imageView.frame = CGRectMake(titleWidth + 14, 1.5f, imageView.frame.size.width, imageView.frame.size.height);
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, newSize, 35.0)];
    titleView.backgroundColor = [UIColor clearColor];
    
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, titleWidth, 18)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0f];
    navLabel.textAlignment = NSTextAlignmentCenter;
    navLabel.text = titleText;
    
    UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 19.0f, newSize, 15)];
    statusLabel.backgroundColor = [UIColor clearColor];
    statusLabel.textColor = [UIColor whiteColor];
    statusLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0f];
    statusLabel.textAlignment = NSTextAlignmentCenter;
    statusLabel.text = statusString;
    
    [titleView addSubview:imageView];
    [titleView addSubview:navLabel];
    [titleView addSubview:statusLabel];
    
    return titleView;
}

+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void) displayError:(NSObject*) delegate msg:(NSString*) msg {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end