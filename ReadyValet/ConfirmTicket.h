//
//  ConfirmTicket.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/15/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmTicket : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UILabel *companyName;
@property (strong, nonatomic) IBOutlet UILabel *ticketNumber;
- (IBAction)touchOK:(id)sender;

@end
