//
//  CheckInVCViewController.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "CheckInVCViewController.h"

#import "ValetCell.h"
#import "ValetLicenseCell.h"
#import "ManualCell.h"

#import "ValetStorage.h"
#import "ValetProfile.h"
#import "TicketView.h"
#import "UtilFunctions.h"
#import "AFNetworking.h"
#import "ManualTicketViewController.h"
#import "SystemPrefs.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface CheckInVCViewController ()

@end

@implementation CheckInVCViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[[SystemPrefs instance] sendGoogleView:@"Check-In"];
    self.listDetails = [[NSMutableArray alloc] init];
    self.listIndex = [[NSMutableArray alloc] init];
    self.listTicketNumber = [[NSMutableArray alloc] init];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadVehicles:)
                                                 name:@"reloadVehicles"
                                               object:nil];
    
    if ([[ValetStorage sharedModel].role isEqualToString:@"valet"]) {
        logoutButton =[[UIBarButtonItem alloc] initWithTitle:@"LOGOUT" style:UIBarButtonItemStyleDone target:self action:@selector(logout:)];
        
        self.navigationItem.rightBarButtonItem = logoutButton;
    }
    else {
        NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
        factory.colors = @[[NIKColor whiteColor]];
        UIBarButtonItem *settingsButton = [UIBarButtonItem new];
        settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
        settingsButton.action = @selector(showAccountSettings:);
        settingsButton.target = self;
        settingsButton.enabled = YES;
        settingsButton.style = UIBarButtonItemStylePlain;
        self.navigationItem.rightBarButtonItem = settingsButton;
    }

    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    // Do any additional setup after loading the view from its nib.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[ValetStorage sharedModel].currentValetName length] > 0)
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Check-In", [ValetStorage sharedModel].currentValetName]];
    else
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Check-In", [ValetStorage sharedModel].valetOwner.company_name]];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getLastPaperTicketNumber];
    
    //[[self.tabBarController.tabBar.items objectAtIndex:0] setBadgeValue:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    /*if (self.refreshTimer) {
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
    }*/
}

-(void)showAccountSettings:(id) sender {
    ValetProfile *valetProfile = [[ValetProfile alloc] initWithNibName:@"ValetProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:valetProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

-(void)logout:(id) sender {
    [ValetStorage sharedModel].currentDriver = nil;
    [ValetStorage sharedModel].role = nil;
    [ValetStorage sharedModel].password = nil;
    [[ValetStorage sharedModel] saveLogin];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	// Create custom header
    UIView *viewFrame = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 22)];
    viewFrame.backgroundColor = [UIColor colorWithRed:(237/255.0) green:(237/255.0) blue:(242/255.0) alpha:1];
    
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,320,22)];
	label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
	label.textAlignment = NSTextAlignmentCenter;
	label.autoresizingMask =  UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin |  UIViewAutoresizingFlexibleRightMargin;
	[label setFont:[UIFont boldSystemFontOfSize:14]];
	label.userInteractionEnabled = NO;
	
    [viewFrame addSubview:label];
	
	label.text = @"Vehicle Check-In";
    
    return viewFrame;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 22.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0f;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if ([[ValetStorage sharedModel].vehiclesInRange count] > 0)
            return [[ValetStorage sharedModel].vehiclesInRange count] + 1;
        else
            return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ValetCell";
    static NSString *CellLicenseIdentifier = @"ValetLicenseCell";
    static NSString *manualIdentifier = @"ManualCell";
    
    ValetCell *cell = (ValetCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    ValetLicenseCell *licensecell = (ValetLicenseCell *)[tableView dequeueReusableCellWithIdentifier:CellLicenseIdentifier];
    if (licensecell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ValetLicenseCell" owner:self options:nil];
        licensecell = [nib objectAtIndex:0];
        
        licensecell.licenseImage.layer.cornerRadius = 2.0f;
        licensecell.licenseImage.clipsToBounds = YES;
    }
    
    if (indexPath.row == 0) {
        ManualCell *manualCell = (ManualCell *)[tableView dequeueReusableCellWithIdentifier:manualIdentifier];
        if (manualCell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ManualCell" owner:self options:nil];
            manualCell = [nib objectAtIndex:0];
        }
        
        [manualCell.descLabel setText:@"Manual Car"];
        NSString *formattedString = [NSString stringWithFormat:@"Next Paper Ticket Number: %ld ", last_ticket_number];
        [manualCell.countLabel setText:formattedString];
        return manualCell;
    }
    else {
        if ([[ValetStorage sharedModel].vehiclesInRange count] > 0) {
            Vehicle* vehicle = [[ValetStorage sharedModel].vehiclesInRange objectAtIndex:indexPath.row-1];
            [self getDriverImage:vehicle path:indexPath];
            if ([vehicle.driverName length] > 0) {
                cell.driverName.text = vehicle.driverName;
            }
            if (vehicle.driverImage) {
                NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
                licensecell.descLabel.text = vehicleString;
                licensecell.licenseImage.image = vehicle.driverImage;
                if ([vehicle.driverName length] > 0) {
                    licensecell.driverName.text = vehicle.driverName;
                }
                return licensecell;
            }
            
            NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
            cell.descLabel.text = vehicleString;
        }
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Manual Car
    if (indexPath.row == 0) {
        ManualTicketViewController *manual = [[ManualTicketViewController alloc] initWithNibName:@"ManualTicketViewController" bundle:nil];
        NSString *last_ticket = [NSString stringWithFormat:@"%ld", last_ticket_number];
        [manual setTicketNumber:last_ticket];
        [self.navigationController pushViewController:manual animated:TRUE];
        
        //[self displayManualCarActionSheeet:indexPath.row];
    }
    else {
        Vehicle* vehicle = [[ValetStorage sharedModel].vehiclesInRange objectAtIndex:indexPath.row-1];
        NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@",vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
        [ValetStorage sharedModel].currentVehicleName = vehicleString;
        
        [ValetStorage sharedModel].currentVehicleId = vehicle.vehicleId;
        [self displayVehicleActionSheet:vehicle row:indexPath.row];
    }
}

- (void)displayVehicleActionSheet:(Vehicle*) vehicle row:(NSInteger) row
{
	NSString *title = [NSString stringWithFormat:@"Assign ticket to %@?", vehicle.driverName];
	NSString *header = [NSString stringWithFormat:@"Park %@ %@", vehicle.vehicleManufacturer, vehicle.vehicleModel];
    
    [self.listIndex addObject:vehicle.vehicleId];
    [self.listDetails addObject:vehicle.driverName];
	
	
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:header
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:title, nil];
    
    actionSheet.tag = row;
    
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        //NSLog(@"Tag: %d", actionSheet.tag);
        if ([[ValetStorage sharedModel].role isEqualToString:@"valet_owner"]) {
            [UtilFunctions displayError:self msg:@"You are currently logged in as a valet owner. Please login as a runner to perform these actions."];
        } else if ([[ValetStorage sharedModel].vehiclesInRange count] > 0) {
            long currentVehicle = actionSheet.tag-1;
            Vehicle* vehicle = [[ValetStorage sharedModel].vehiclesInRange objectAtIndex:currentVehicle];
            [ValetStorage sharedModel].currentVehicleId = vehicle.vehicleId;
            
            TicketView *tv = [[TicketView alloc] initWithNibName:@"TicketView" bundle:nil];
            tv.title = @"Electronic Ticket";
            [self.navigationController pushViewController:tv animated:TRUE];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) reloadVehicles:(NSNotification *) notification {
    [self.tableView reloadData];
}

-(void) getDriverImage:(Vehicle*) v path:(NSIndexPath *) path {
    if ([[[ValetStorage sharedModel].driverImages objectForKey:v.vehicleId] isKindOfClass:[UIImage class]]) {
        v.driverImage = [[ValetStorage sharedModel].driverImages objectForKey:v.vehicleId];
    }
    else {
        AFHTTPRequestOperationManager *apiManager;
        apiManager = [AFHTTPRequestOperationManager manager];
        
        NSString *url = [NSString stringWithFormat:@"%@driverimage/%@", [[SystemPrefs instance] getURL], v.vehicleId];
        [apiManager POST:url parameters:nil   success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            if (![[responseObject objectForKey:@"driverImage"] isKindOfClass:[NSNull class]]) {
                [v StringToImage:[responseObject objectForKey:@"driverImage"]];
                if (v.driverImage) {
                    [[ValetStorage sharedModel].driverImages setObject:v.driverImage forKey:v.vehicleId];
                    [self.tableView beginUpdates];
                    [self.tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
                    [self.tableView endUpdates];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}

-(void) getLastPaperTicketNumber {
    AFHTTPRequestOperationManager *apiManager;
    apiManager = [AFHTTPRequestOperationManager manager];
    
    NSString *url = [NSString stringWithFormat:@"%@paper_ticket_number/%@", [[SystemPrefs instance] getURL], [ValetStorage sharedModel].currentValetId];
    
    [apiManager POST:url parameters:nil   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject objectForKey:@"paper_ticket_number"] isKindOfClass:[NSNull class]])
            last_ticket_number = 0;
        else
            last_ticket_number = [[responseObject objectForKey:@"paper_ticket_number"] intValue];
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        last_ticket_number = 0;
    }];
}


@end
