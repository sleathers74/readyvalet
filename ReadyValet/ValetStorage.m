//
//  ValetStorage.m
//  Valet
//
//  Created by Matthew Schmidgall on 1/4/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import "ValetStorage.h"

@implementation ValetStorage


+ (ValetStorage*)sharedModel {
    static ValetStorage *sharedValetStorage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedValetStorage = [[self alloc] init];
        sharedValetStorage.vehiclesRequested = [[NSMutableArray alloc] init];
        sharedValetStorage.vehiclesStored = [[NSMutableArray alloc] init];
        sharedValetStorage.vehiclesCompleted = [[NSMutableArray alloc] init];
        sharedValetStorage.currentDriver = [[Driver alloc] init];
        sharedValetStorage.valetOwner = [[ValetOwner alloc] init];
        sharedValetStorage.ticketQueues = [[NSMutableDictionary alloc] init];
        sharedValetStorage.vehiclesInRange = [[NSMutableArray alloc] init];
        sharedValetStorage.driverImages = [[NSMutableDictionary alloc] init];
    });
    return sharedValetStorage;
}

- (void) logout
{
    self.currentDriver = nil;
}

- (void) saveLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:self.username forKey:@"username"];
    [defaults setObject:self.password forKey:@"password"];
    
    [defaults synchronize];
}

- (BOOL) loadLogin {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.username = [defaults objectForKey:@"username"];
    self.password = [defaults objectForKey:@"password"];
    
    if ((self.username == nil) || (self.password == nil) || ([self.username isEqualToString:@""] == YES) || ([self.password isEqualToString:@""] == YES)) {
        return NO;
    }
    return YES;

}


@end
