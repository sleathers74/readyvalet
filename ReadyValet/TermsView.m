//
//  TermsView.m
//  ReadyValet
//
//  Created by Scott Leathers on 8/10/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "TermsView.h"

@interface TermsView ()

@end

@implementation TermsView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
