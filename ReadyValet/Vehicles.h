//
//  Vehicles.h
//  Valet
//
//  Created by Matthew Schmidgall on 1/5/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vehicles : NSObject
@property (weak,nonatomic) NSString *vehicleName;
@property (weak,nonatomic) NSString *ticketNumber;
@property (weak,nonatomic) UIImage *ticketImage;

@end
