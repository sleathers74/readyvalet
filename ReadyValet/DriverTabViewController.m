//
//  DriverTabViewController.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "DriverTabViewController.h"
#import "ValetViewController.h"
#import "CreditCardProcessing.h"
#import "UtilFunctions.h"

// No Default beacon
#import "ValetSelectionVC.h"
#import "SystemPrefs.h"
#import "ValetStorage.h"

@interface DriverTabViewController ()

@end

@implementation DriverTabViewController

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[CreditCardProcessing sharedModel] loadBriantree];
    
    self.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeMyControllers:)
                                                 name:@"changeControllers"
                                               object:nil];
    
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed: 0/255.0 green: 34.0/255.0 blue: 85.0/255.0 alpha: 1]];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBackgroundColor:[UIColor colorWithRed: 0/255.0 green: 34.0/255.0 blue: 85.0/255.0 alpha: 1]];
    //[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:20.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.valetAutoDetect = [[ValetViewController alloc] initWithNibName:@"ValetViewController" bundle:nil];
    self.valetAutoDetect.title = @"Valet";
    //self.valetVC.tabBarItem.image = [UIImage imageNamed:@"checkin.png"];
    
    //self.valetSelection = [[ValetSelectionVC alloc] initWithNibName:@"ValetSelectionVC" bundle:nil];
    //self.valetSelection.title = @"Valet";
    //vsc.tabBarItem.image = [UIImage imageNamed:@"checkin.png"];
    UINavigationController *driverNVC;
    
    long autoD = [ValetStorage sharedModel].autoDetect;
    if (autoD == 1) {
        driverNVC = [[UINavigationController alloc] initWithRootViewController:self.valetAutoDetect];
    }
    else {
        driverNVC = [[UINavigationController alloc] initWithRootViewController:self.valetSelection];
    }
    
    [self setViewControllers:[NSArray arrayWithObjects:driverNVC, nil]];
    self.delegate = self;
    
    
    self.lastCheckIn = 0;
    NSUUID *myUUID = [[NSUUID alloc] initWithUUIDString:@"033C7CC8-2352-48F1-81CD-919924AF0CE4"]; //@"3EE147AE-A4D6-4B13-8EA1-B93F110BB991"];
    
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:myUUID
                                                             identifier:@"com.valet.stands"];
    self.myBeaconRegion.notifyEntryStateOnDisplay = true;
    self.locationManager = [[CLLocationManager alloc] init];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager setDelegate:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startMonitoring:)
                                                 name:@"startMonitoring"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopMonitoring:)
                                                 name:@"stopMonitoring"
                                               object:nil];
    
    //if (autoD == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startMonitoring" object:self];
    //}
    //else {
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMonitoring" object:self];
    //}
    [self.tabBar setHidden:TRUE];
}
- (void) startMonitoring:(NSNotification *) notification {
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

- (void) stopMonitoring:(NSNotification *) notification {
    [self.locationManager stopMonitoringForRegion:self.myBeaconRegion];
    [self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
}

- (void) changeMyControllers:(NSNotification *) notification
{
    self.valetAutoDetect = [[ValetViewController alloc] initWithNibName:@"ValetViewController" bundle:nil];
    self.valetAutoDetect.title = @"Valet";
    //self.valetVC.tabBarItem.image = [UIImage imageNamed:@"checkin.png"];
    
    //self.valetSelection = [[ValetSelectionVC alloc] initWithNibName:@"ValetSelectionVC" bundle:nil];
    //self.valetSelection.title = @"Valet";
    //vsc.tabBarItem.image = [UIImage imageNamed:@"checkin.png"];
    UINavigationController *driverNVC;
    
    //long autoD = [ValetStorage sharedModel].autoDetect;
    //if (autoD == 1) {
        driverNVC = [[UINavigationController alloc] initWithRootViewController:self.valetAutoDetect];
    //}
    //else {
    //    driverNVC = [[UINavigationController alloc] initWithRootViewController:self.valetSelection];
    //}
    
    ValetViewController *logoutFake = [[ValetViewController alloc] initWithNibName:@"ValetViewController" bundle:nil];
    logoutFake.title = @"Logout";
    //logoutFake.tabBarItem.image = [UIImage imageNamed:@"checkin.png"];
    
    
    [self setViewControllers:[NSArray arrayWithObjects:driverNVC, logoutFake, nil]];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        
        [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    self.lastCheckIn = 0;
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        [manager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        if (state == CLRegionStateInside) {
            [manager startRangingBeaconsInRegion:(CLBeaconRegion *)region];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    
    if (beacons.count > 0) {
        CLBeacon *beacon = [beacons objectAtIndex:0];
        
        if ((time(NULL) - self.lastCheckIn) > 330) {
            self.lastCheckIn = time(NULL);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *username = [defaults stringForKey:@"username"];
            NSString *password = [defaults stringForKey:@"password"];
            
            NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                      username, @"username",
                                      password, @"password",
                                      nil];
            
            self.apiManager = [AFHTTPRequestOperationManager manager];
            NSLog(@"Beacon: %@", beacon.major);
            
            NSString *url = [NSString stringWithFormat:@"%@range/%@", [[SystemPrefs instance] getURL], beacon.major];
            
            
            [self.apiManager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [UtilFunctions displayError:self msg:error.localizedDescription];
            }];
        }
        
        switch (beacon.proximity) {
            case CLProximityImmediate:
                break;
            case CLProximityNear:
                break;
            default:
                break;
        }
    }
}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        [self.peripheralManager startAdvertising:self.myBeaconData];
        self.valetAutoDetect.spinner.hidden = TRUE;
    } else if (peripheral.state == CBPeripheralManagerStatePoweredOff) {
        self.valetAutoDetect.welcomeText.text = @"Service Stopped. Trying to reconnect.";
        self.valetAutoDetect.spinner.hidden = FALSE;
        [self.peripheralManager stopAdvertising];
    } else if (peripheral.state == CBPeripheralManagerStateUnsupported) {
        NSLog(@"Bluetooth not supported");
        self.valetAutoDetect.welcomeText.text = @"Bluetooth not supported";
        self.valetAutoDetect.spinner.hidden = TRUE;
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    // Loggout
    if ([tabBarController selectedIndex] == 1) {
        [ValetStorage sharedModel].currentDriver = nil;
        [ValetStorage sharedModel].role = nil;
        [ValetStorage sharedModel].password = nil;
        [[ValetStorage sharedModel] saveLogin];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end