//
//  CheckInVCViewController.h
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface CheckInVCViewController : UIViewController <UIActionSheetDelegate> {
    UIBarButtonItem         *logoutButton;
    long                    last_ticket_number;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSTimer *refreshTimer;
@property (strong, nonatomic) NSMutableArray *listDetails;
@property (strong, nonatomic) NSMutableArray *listIndex;
@property (strong, nonatomic) NSMutableArray *listTicketNumber;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;

@end
