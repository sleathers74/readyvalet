//
//  DriverTabViewController.h
//  ReadyValet
//
//  Created by Scott Leathers on 7/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ValetViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "AFNetworking.h"
#import "ValetSelectionVC.h"

@interface DriverTabViewController : UITabBarController <UITabBarControllerDelegate,CBPeripheralManagerDelegate,CLLocationManagerDelegate> {
}

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) NSDictionary *myBeaconData;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CBPeripheralManager *peripheralManager;
@property int lastCheckIn;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) ValetViewController *valetAutoDetect;
@property (strong, nonatomic) ValetSelectionVC *valetSelection;

@end
