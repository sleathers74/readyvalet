//
//  SettingsProfile.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/16/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "SettingsProfile.h"
#import "AFNetworking.h"
#import "UtilFunctions.h"
#import "ValetStorage.h"
#import "SystemPrefs.h"

@interface SettingsProfile ()

@end

@implementation SettingsProfile

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.waitView setHidden:TRUE];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SAVE" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    if (loggedin == 1) {
        if ([ValetStorage sharedModel].autoDetect == 1)
            [self.autoCheckingSwitch setOn:TRUE];
        else
            [self.autoCheckingSwitch setOn:FALSE];
    }
    else {
        [self.autoCheckingSwitch setOn:TRUE];
    }
    
    self.tipStepper.value = [[ValetStorage sharedModel].currentDriver.defaultTip doubleValue];
    [self.defaultTipLabel setText:[NSString stringWithFormat:@"$%.02f", [[ValetStorage sharedModel].currentDriver.defaultTip doubleValue]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)save {
    [self.waitView setHidden:FALSE];
    [self updateUser];
}

-(void) updateUser {
    if (self.autoCheckingSwitch.isOn) {
        [ValetStorage sharedModel].autoDetect = 1;
    }
    else {
        [ValetStorage sharedModel].autoDetect = 0;
    }
    
    
    NSNumber *autoD = [NSNumber numberWithInteger:[ValetStorage sharedModel].autoDetect];

    
    AFHTTPRequestOperationManager *apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     username, @"username",
                                     password, @"password",
                                     username, @"newusername",
                                     password, @"newpassword",
                                     nil];
    [postData setObject:autoD forKey:@"auto_detect"];
    [postData setObject:[NSString stringWithFormat:@"%.02f", self.tipStepper.value] forKey:@"defaultTip"];
    
    NSString *url = [NSString stringWithFormat:@"%@updatedriver", [[SystemPrefs instance] getURL]];
    [apiManager POST:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
        NSLog(@"JSON: %@", responseObject);
#endif
        [ValetStorage sharedModel].currentDriver.defaultTip = [NSString stringWithFormat:@"%.02f", self.tipStepper.value];
        [self.waitView setHidden:TRUE];
        [self.navigationController popViewControllerAnimated:TRUE];
        
    }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [self.waitView setHidden:TRUE];
                 NSLog(@"Error: %@", error);
                 [UtilFunctions displayError:self msg:error.localizedDescription];
             }];
}


- (IBAction)tipTouched:(id)sender {
    NSString *paidAmt =  [NSString stringWithFormat:@"$%.02f", self.tipStepper.value];
    [self.defaultTipLabel setText:paidAmt];
}
@end
