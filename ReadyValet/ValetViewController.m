//
//  ValetViewController.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetViewController.h"
#import "ValetStorage.h"
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "ConfirmTicket.h"
#import "ValetDetection.h"
#import "ValetSelectionVC.h"
#import "UtilFunctions.h"
#import "MainProfile.h"
#import "SystemPrefs.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"


// Google
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface ValetViewController ()

@end

@implementation ValetViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.registerButton.titleLabel.textAlignment = UITextAlignmentCenter;
    
    //self.view.backgroundColor = [UIColor colorWithRed:(37/255.0) green:(36/255.0) blue:(36/255.0) alpha:1];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.valetDetection = nil;
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    self.vehicleStatus = 0;
    [self.vehicleLabel setText:@""];
    
    if ([ValetStorage sharedModel].currentVehicleId != nil) {
        [self.vehicleLabel setText:[ValetStorage sharedModel].currentVehicleName];
        [self.ticketLabel setText:[ValetStorage sharedModel].currentTicketNumber];
    }
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory barButtonItemIconFactory];
    factory.colors = @[[NIKColor whiteColor]];
    settingsButton = [UIBarButtonItem new];
    settingsButton.image = [factory createImageForIcon:NIKFontAwesomeIconUser];
    settingsButton.action = @selector(showAccountSettings:);
    settingsButton.target = self;
    settingsButton.enabled = YES;
    settingsButton.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = settingsButton;
    
    [self.welcomeText setText:@""];
    [self.callCarButton setHidden:true];
    [self.registerButton setHidden:true];
    [self.vehicleTicketImage setHidden:true];
    [self.ticketLabel setHidden:true];
    [self.checkinConf setHidden:TRUE];
}

-(void)showAccountSettings:(id) sender {
    MainProfile *mainProfile = [[MainProfile alloc] initWithNibName:@"MainProfile" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainProfile];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    
    // Reset all states in view before displaying
    
    self.mapView.userTrackingMode = YES;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMonitoring" object:self];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"startMonitoring" object:self];
    
    [self.welcomeText setText:@""];
    [self.callCarButton setHidden:true];
    [self.registerButton setHidden:true];
    [self.vehicleTicketImage setHidden:true];
    [self.ticketLabel setHidden:true];
    [self.pickupButton setHidden:true];
    
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [self updateInfoView:true];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    if (self.refreshTimer) {
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    [self.mapView setCenterCoordinate:userLocation.location.coordinate animated:YES];
}

- (void)requeueInfoViewUpdate {
    
    if (self.refreshTimer) {
        [self.refreshTimer invalidate];
    }
    self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(forceUpdateInfoView) userInfo:nil repeats:NO];
}

- (void) displayConfirmTicket {
    ConfirmTicket *ct = [[ConfirmTicket alloc] initWithNibName:@"ConfirmTicket" bundle:nil];
    ct.title = @"";
    [self.navigationController pushViewController:ct animated:TRUE];
}

- (IBAction)touchCallCar:(id)sender {
    
    // SDL Here
    /*SubmitPayment *submitPayment = [[SubmitPayment alloc] initWithNibName:@"SubmitPayment" bundle:nil];
    submitPayment.title = @"Payment";
    [self.navigationController pushViewController:submitPayment animated:TRUE];*/
}

- (IBAction)touchRegisterPaperTicket:(id)sender {
    ValetSelectionVC *valetSelection = [[ValetSelectionVC alloc] initWithNibName:@"ValetSelectionVC" bundle:nil];
    valetSelection.title = @"Valet";
    [self.navigationController pushViewController:valetSelection animated:TRUE];
}

- (void)forceUpdateInfoView {
    [self updateInfoView:true];
}

- (void)updateInfoView:(BOOL)refreshFromDb {
    
    if ([[ValetStorage sharedModel].role isEqualToString:@"driver"]) {
        
        if (refreshFromDb) {
            
            self.apiManager = [AFHTTPRequestOperationManager manager];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *username = [defaults stringForKey:@"username"];
            NSString *password = [defaults stringForKey:@"password"];
            
            NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                      username, @"username",
                                      password, @"password",
                                      nil];
            
            NSString *url = [NSString stringWithFormat:@"%@profile", [SystemPrefs instance].getURL];
            [self.apiManager GET:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
                
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:@"&uid" value:username];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Main Sign-In View"
                                                                      action:@"sign-in"
                                                                       label:nil
                                                                       value:nil] build]];
                
                [[SystemPrefs instance] sendGoogleEvent:@"User" action:@"Role" label:[responseObject objectForKey:@"role"]];
                
                NSString *vehicleId;
                for (vehicleId in [responseObject objectForKey:@"vehicle"]) {
                    [ValetStorage sharedModel].currentVehicleId = vehicleId;
                    self.vehicleStatus = [[[[responseObject objectForKey:@"vehicle"] objectForKey:[ValetStorage sharedModel].currentVehicleId] objectForKey:@"status"] integerValue];
                    
                    NSString *color = [[[responseObject objectForKey:@"vehicle"] objectForKey:[ValetStorage sharedModel].currentVehicleId] objectForKey:@"color"];
                    NSString *make = [[[responseObject objectForKey:@"vehicle"] objectForKey:[ValetStorage sharedModel].currentVehicleId] objectForKey:@"make"];
                    NSString *model = [[[responseObject objectForKey:@"vehicle"] objectForKey:[ValetStorage sharedModel].currentVehicleId] objectForKey:@"model"];
                    
                    NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@", color, make, model];
                    
                    [ValetStorage sharedModel].currentVehicleName = vehicleString;
                    [ValetStorage sharedModel].currentTicketNumber = [[[responseObject objectForKey:@"vehicle"] objectForKey:[ValetStorage sharedModel].currentVehicleId] objectForKey:@"ticket_id"];
                    
                    if ([responseObject objectForKey:@"valet"]) {
                        NSDictionary *valetDict = [responseObject objectForKey:@"valet"];
                        [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_id"];
                        [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company"];
                        [ValetStorage sharedModel].currentValetFee = [valetDict objectForKey:@"valet_fee"];
                        [ValetStorage sharedModel].currentServiceFee = [valetDict objectForKey:@"service_fee"];
                        [ValetStorage sharedModel].currentMerchantId = [valetDict objectForKey:@"merchant_id"];
                        [ValetStorage sharedModel].currentMerchantStatus = [valetDict objectForKey:@"merchant_status"];
                    }
                    if (self.vehicleStatus > 0) {
                        break;
                    }
                }
                
                
                
                // Get the image
                if(self.vehicleStatus!=0) {
                    self.apiManager = [AFHTTPRequestOperationManager manager];
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSString *username = [defaults stringForKey:@"username"];
                    NSString *password = [defaults stringForKey:@"password"];
                    
                    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                              username, @"username",
                                              password, @"password",
                                              nil];
                    
                    [self.apiManager GET:[NSString stringWithFormat:@"%@ticket/image/%@", [SystemPrefs instance].getURL, [ValetStorage sharedModel].currentTicketNumber] parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        NSLog(@"JSON: %@", responseObject);
                        
                        if ([[responseObject objectForKey:@"image"] length] > 0) {
                            NSData* imageData = [[NSData alloc] initWithBase64EncodedString:[responseObject objectForKey:@"image"] options:0];
                            self.vehicleTicketImage.image = [UIImage imageWithData:imageData];
                            [self.ticketLabel setHidden:true];
                            [self.vehicleTicketImage setHidden:false];
                            self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"In-Lot"];
                        } else {
                            [self.ticketLabel setText:[ValetStorage sharedModel].currentTicketNumber];
                            [self.vehicleTicketImage setHidden:true];
                            [self.ticketLabel setHidden:false];
                            self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"In-Lot"];
                        }
                        [self updateInfoView:false];
                        [self requeueInfoViewUpdate];
                        
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSLog(@"Error: %@", error);
                        [self requeueInfoViewUpdate];
                    }];
                } else {
                    [self updateInfoView:false];
                    [self requeueInfoViewUpdate];
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                [self requeueInfoViewUpdate];
                
            }];
            
            
        } else {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            switch(self.vehicleStatus) {
                case 0: // Vehicle not parked
                    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Check-In"];
                    NSLog(@"%d", appDelegate.driverTabViewController.lastCheckIn);
                    if (appDelegate.driverTabViewController.lastCheckIn) {
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMonitoring" object:self];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"startMonitoring" object:self];
                        
                        long autoD = [ValetStorage sharedModel].autoDetect;
                        if (autoD == 1) {
                            self.welcomeText.text = @"You’re vehicle is waiting to be checked-in by a Hiker™ enabled Valet Service.";
                            [self.ticketLabel setHidden:true];
                            [self.vehicleTicketImage setHidden:true];
                            self.callCarButton.hidden = true;
                            [self.checkinConf setHidden:TRUE];
                            self.registerButton.hidden = false;
                            self.pickupButton.hidden = true;
                        }
                        else {
                            if (self.valetDetection == nil)
                                self.valetDetection = [[ValetDetection alloc] initWithNibName:@"ValetDetection" bundle:nil];
                            self.valetDetection.title = @"";
                            
                            if ([self.navigationController visibleViewController] != self.valetDetection)
                                [self.navigationController pushViewController:self.valetDetection animated:TRUE];
                        }
                    } else {
                        self.navigationItem.titleView = [UtilFunctions buildCustomNav];
                        self.welcomeText.text = @"You are not within range of a Hiker™ enabled Valet Service";
                        [self.ticketLabel setHidden:true];
                        [self.vehicleTicketImage setHidden:true];
                        [self.callCarButton setHidden:true];
                        [self.checkinConf setHidden:TRUE];
                        [self.registerButton setHidden:true];
                        [self.pickupButton setHidden:true];
                    }
                    break;
                    
                case 1: // Vehicle Parked
                    [self displayConfirmTicket];
                    break;
                    
                case 2: // Vehicle Requested
                    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Vehicle Requested"];
                    self.welcomeText.text = [NSString stringWithFormat:@"Your %@ has been requested. Please make your way towards the valet stand.  Show them the ticket number below.",[ValetStorage sharedModel].currentVehicleName];
                    [self.checkinConf setHidden:TRUE];
                    self.callCarButton.hidden = true;
                    self.registerButton.hidden = true;
                    self.pickupButton.hidden = true;
                    break;
                    
                case 3: // Vehicle Retrieved
                    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Vehicle Retrieved"];
                    self.welcomeText.text = [NSString stringWithFormat:@"Your %@ is ready for pickup. Show the Valet attendants the ticket number below.  When you've picked up your vehicle, please confirm by clicking below.", [ValetStorage sharedModel].currentVehicleName];
                    self.callCarButton.hidden = true;
                    [self.checkinConf setHidden:TRUE];
                    self.registerButton.hidden = true;
                    self.pickupButton.hidden = false;
                    break;
            }
        }
        
    } else {
        [self.ticketLabel setHidden:false];
        [self.vehicleLabel setHidden:false];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)returnHome:(id)sender {
    //[self.navigationController popToRootViewControllerAnimated:TRUE];
    //    [self.navigationController popToViewController: [self.navigationController.viewControllers objectAtIndex: 1] animated:true];
}

- (IBAction)pickedUpVehicle:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Confirm Pick-Up"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Yes, I've Picked Up",nil];
    [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        self.vehicleStatus = 0;
        [self updateInfoView:false];
        self.apiManager = [AFHTTPRequestOperationManager manager];
        //[self.apiManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[ValetStorage sharedModel].username password:[ValetStorage sharedModel].password];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *username = [defaults stringForKey:@"username"];
        NSString *password = [defaults stringForKey:@"password"];
        
        NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  username, @"username",
                                  password, @"password",
                                  nil];
        
        [self.apiManager PUT:[NSString stringWithFormat:@"%@ticket/%@/4", [SystemPrefs instance].getURL, [ValetStorage sharedModel].currentTicketNumber] parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            [self requeueInfoViewUpdate];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [self requeueInfoViewUpdate];
        }];
        
    }
}
@end
