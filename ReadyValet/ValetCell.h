//
//  LoginViewCell.h
//  IllinoisPrepScores
//
//  Created by Scott Leathers on 7/31/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ValetCell : UITableViewCell {
    
}
@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) IBOutlet UILabel *driverName;
@property (strong, nonatomic) IBOutlet UILabel *middleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImage;


@end
