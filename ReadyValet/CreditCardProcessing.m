//
//  CreditCardProcessing.m
//
//  Created by Scott Leathers on 10/4/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "CreditCardProcessing.h"
#import "AFNetworking.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

@implementation CreditCardProcessing


+ (CreditCardProcessing*)sharedModel {
    static CreditCardProcessing *sharedCreditCardProcessing = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCreditCardProcessing = [[self alloc] init];
        sharedCreditCardProcessing.paymentMethods = [[NSMutableArray alloc] init];
    });
    return sharedCreditCardProcessing;
}

- (void) loadBriantree {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
							  
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@generateToken", [[SystemPrefs instance] getURL]];
    [manager GET:url parameters:postData
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.clientToken = responseObject[@"client_token"];
             self.braintree = [Braintree braintreeWithClientToken:responseObject[@"client_token"]];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"brainTreeLoaded" object:nil];
             [self fetchPaymentMethods];
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [[NSNotificationCenter defaultCenter] postNotificationName:@"brainTreeLoadFailed" object:nil];
             [UtilFunctions displayError:self msg:[NSString stringWithFormat:@"Failed to register your Credit Card. Please update your credit card information"]];
         }];
}

- (void)fetchPaymentMethods {
    BOOL networkActivityIndicatorState = [[UIApplication sharedApplication] isNetworkActivityIndicatorVisible];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [self.braintree.client fetchPaymentMethodsWithSuccess:^(NSArray *paymentMethods) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:networkActivityIndicatorState];
        
        NSMutableArray *newPaymentMethods = [NSMutableArray arrayWithArray:paymentMethods];
        self.paymentMethods = newPaymentMethods;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"paymentsFetched" object:nil];
    } failure:^(__unused NSError *error) {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:networkActivityIndicatorState];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        /*self.fetchPaymentMethodsErrorAlert = [[BTDropInErrorAlert alloc] initWithCancel:^{
            [self informDelegateDidCancel];
            self.fetchPaymentMethodsErrorAlert = nil;
        } retry:^{
            [self fetchPaymentMethods];
            self.fetchPaymentMethodsErrorAlert = nil;
        }];*/
        
        //[self.fetchPaymentMethodsErrorAlert show];
    }];
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              paymentMethodNonce, @"nonce",
                              nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@payment-method-nonce", [[SystemPrefs instance] getURL]];
    [manager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
              // Handle success
              if ([[responseObject objectForKey:@"success"] boolValue] == YES) {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"noncePostedToServer" object:nil];
              }
              else if ([responseObject objectForKey:@"_attributes"]) {
                  NSDictionary *attribs = [responseObject objectForKey:@"_attributes"];
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWait" object:nil];
                  NSString *msg = [attribs objectForKey:@"message"];
                  [UtilFunctions displayError:self msg:msg];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [[NSNotificationCenter defaultCenter] postNotificationName:@"hideWait" object:nil];
              [UtilFunctions displayError:self msg:error.localizedDescription];
          }];
}

@end