//
//  ValetDetection.m
//  ReadyValet
//
//  Created by Scott Leathers on 10/6/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetDetection.h"
#import "ValetSelectionVC.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

@interface ValetDetection ()

@end

@implementation ValetDetection

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setHidesBackButton:TRUE];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav:@"Check-In"];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    self.navigationController.navigationItem.leftBarButtonItem.enabled = FALSE;
    [super viewWillAppear:animated];
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitPaperTicket:(id)sender {
    ValetSelectionVC *ct = [[ValetSelectionVC alloc] initWithNibName:@"ValetSelectionVC" bundle:nil];
    ct.title = @"";
    
    [self.navigationController pushViewController:ct animated:TRUE];
}

- (IBAction)submitAutoCheckin:(id)sender {
    NSNumber *autoD = [NSNumber numberWithInteger:1];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              username, @"newusername",
                              password, @"newpassword",
                              autoD, @"auto_detect",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@updatedriver", [SystemPrefs instance].getURL];
    [self.apiManager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [ValetStorage sharedModel].autoDetect = 1;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeControllers" object:self];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ValetStorage sharedModel].currentDriver = nil;
        NSLog(@"Error: %@", error);
        
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}


@end
