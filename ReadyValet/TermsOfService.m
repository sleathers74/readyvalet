//
//  TermsOfService.m
//  ReadyValet
//
//  Created by Scott Leathers on 2/23/15.
//  Copyright (c) 2015 Scott Leathers. All rights reserved.
//

#import "TermsOfService.h"
#import "UtilFunctions.h"

@interface TermsOfService ()

@end

@implementation TermsOfService

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"ACCEPT" style:UIBarButtonItemStylePlain target:self action:@selector(accept)];
    // Do any additional setup after loading the view from its nib.
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)accept {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"1" forKey:@"acceptedTerms"];
    [defaults synchronize];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
