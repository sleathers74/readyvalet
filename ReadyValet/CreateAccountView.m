//
//  CreateAccountView.m
//  ReadyValet
//
//  Created by Scott Leathers on 8/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "CreateAccountView.h"
#import "BSKeyboardControls.h"
#import "ValetStorage.h"
#import "SystemPrefs.h"
#import "AccountProfile.h"
#import "UtilFunctions.h"
#import "myPaymentMethodVC.h"
#import "CreditCardProcessing.h"
#import "BTMainCC.h"
#import "UserPhoto.h"
#import "UIColor+BTUI.h"

#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface CreateAccountView ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end

@implementation CreateAccountView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didTapDone {
    [self.waitView setHidden:FALSE];
    [self validateUsername];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.phoneNumberText.formatter setDefaultOutputPattern:@"(###) ###-####"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updatePhoto:)
                                                 name:@"updatePhoto"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(brainTreeLoaded:)
                                                 name:@"brainTreeLoaded"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(brainTreeLoaded:)
                                                 name:@"brainTreeLoadFailed"
                                               object:nil];
    
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"NEXT" style:UIBarButtonItemStylePlain target:self action:@selector(didTapDone)];
    
    [self.waitView setHidden:TRUE];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationController.navigationBar.topItem.title = @"";
    
    if ([self.currentHeaderText length] <= 0)
        self.currentHeaderText = @"New User";
    [self.userText setText:self.currentHeaderText];
    
    [UtilFunctions addLeftPrefix:self.emailTextField prefix:@"Email:"];
    [UtilFunctions addLeftPrefix:self.passwordTextField prefix:@"Password:"];
    [UtilFunctions addLeftPrefix:self.firstName prefix:@"First:"];
    [UtilFunctions addLeftPrefix:self.lastName prefix:@"Last:"];
    [UtilFunctions addLeftPrefix:self.phoneNumberText prefix:@"Phone:"];
    
        
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    if (loggedin == 1) {
        self.usernameTextField.text = [ValetStorage sharedModel].username;
        self.passwordTextField.text = [ValetStorage sharedModel].password;
        self.firstName.text = [ValetStorage sharedModel].currentDriver.firstName;
        self.lastName.text = [ValetStorage sharedModel].currentDriver.lastName;
    }

    self.scrollView.scrollEnabled = TRUE;
    self.scrollView.contentSize = CGSizeMake(320, 568);
    
    NSArray *fields = @[self.firstName, self.lastName, self.emailTextField, self.passwordTextField];
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    [self.navigationItem setHidesBackButton:TRUE];
    
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //self.rightBarButton.enabled = FALSE;
    //[self.emailTextField becomeFirstResponder];
    
    [self.emailTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
}

- (IBAction) cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:FALSE];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.keyboardControls setActiveField:textField];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:TRUE];
}

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view = keyboardControls.activeField.superview.superview;
    [self.scrollView scrollRectToVisible:view.frame animated:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];//Dismiss the keyboard.
    
    //[self touchCreate:0];
    //Add action you want to call here.
    return YES;
}

- (IBAction)nextView:(id)sender
{
}

- (void)textFieldChangedAction:(UITextField *)textField {
    if (self.emailTextField.text.length && self.passwordTextField.text.length) {
        self.rightBarButton.enabled = TRUE;
    } else {
        self.rightBarButton.enabled = FALSE;
    }
}

-(void) dealloc {
    self.rightBarButton = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchPayment:(id)sender {
     [self.waitView setHidden:FALSE];
    if ([UtilFunctions NSStringIsValidEmail:self.usernameTextField.text]) {
        [self validateUsername];
    }
    else {
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:@"Invalid email address."];
    }
}

-(bool) validateUsername {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    
    if (![username isEqualToString:self.usernameTextField.text]) {
        self.apiManager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         self.usernameTextField.text, @"username", nil];
        NSString *urlString = [NSString stringWithFormat:@"%@/checkUsername", [[SystemPrefs instance] full_url]];
        [self.apiManager POST:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            //NSLog(@"JSON: %@", responseObject);
            //[self.waitView setHidden:TRUE];
            if ([[responseObject objectForKey:@"username_exists"] intValue] == 1) {
                [self.waitView setHidden:TRUE];
                NSString *msg = @"The email address you have chosen already exists in our system. Please choose a different email address.";
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                return;
            }
            [self processFinal];
            
            //[self.waitView setHidden:TRUE];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
             [self.waitView setHidden:TRUE];
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }];
    }
    else {
        [self.waitView setHidden:TRUE];
        [self processFinal];
    }
    
    return true;
}

-(void) addUser {
    //[self.waitView setHidden:FALSE];
    
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    [postData setObject:self.firstName.text forKey:@"first_name"];
    [postData setObject:self.lastName.text forKey:@"last_name"];
    
    if ([self.phoneNumberText.text length] > 0) {
        [postData setObject:self.phoneNumberText.text forKey:@"phone"];
    }
    else {
         [postData setObject:@"" forKey:@"phone"];
    }
    
    [postData setObject:@"0000000000" forKey:@"card_number"];
    [postData setObject:@"0000" forKey:@"card_expiration"];
    
    if ([AccountProfile instance].driverImage) {
        NSData *imageData = UIImageJPEGRepresentation([AccountProfile instance].driverImage, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"driverImage"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/", [[SystemPrefs instance] full_url]];
    urlString = [NSString stringWithFormat:@"%@driver", [[SystemPrefs instance] full_url]];
    [postData setObject:self.usernameTextField.text forKey:@"username"];
    [postData setObject:self.passwordTextField.text forKey:@"password"];
    [postData setObject:self.usernameTextField.text forKey:@"email"];
    
    [self.apiManager POST:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
        [[NSUserDefaults standardUserDefaults] setObject:@"driver" forKey:@"role"];
        [[NSUserDefaults standardUserDefaults] setObject:self.usernameTextField.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:self.passwordTextField.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self createBTRecord];
        
        Driver *d = [[Driver alloc] init];
        d.emailAddress = [[responseObject objectForKey:@"driver"] objectForKey:@"email"];
        d.firstName = [[responseObject objectForKey:@"driver"] objectForKey:@"first_name"];
        d.lastName = [[responseObject objectForKey:@"driver"] objectForKey:@"last_name"];
        d.phone = [[responseObject objectForKey:@"driver"] objectForKey:@"phone"];
        d.creditCardNumber = [[responseObject objectForKey:@"driver"] objectForKey:@"card_number"];
        d.creditCardExpiration = [[responseObject objectForKey:@"driver"] objectForKey:@"card_expiration"];
        
        NSString *driverImage = [[responseObject objectForKey:@"driver"] objectForKey:@"driverImage"];
        if ([driverImage length] > 0) {
            NSData* imageData = [[NSData alloc] initWithBase64EncodedString:driverImage options:0];
            d.driverImage = [UIImage imageWithData:imageData];
        }
        [ValetStorage sharedModel].currentDriver = d;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ValetStorage sharedModel].currentDriver = nil;
        NSLog(@"Error: %@", error);
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

-(void) createBTRecord {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    [postData setObject:self.usernameTextField.text forKey:@"username"];
    [postData setObject:self.passwordTextField.text forKey:@"password"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/", [[SystemPrefs instance] full_url]];
    urlString = [NSString stringWithFormat:@"%@createCustomer", [[SystemPrefs instance] full_url]];
    
    [self.apiManager POST:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
         [[CreditCardProcessing sharedModel] loadBriantree];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ValetStorage sharedModel].currentDriver = nil;
        NSLog(@"Error: %@", error);
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

-(void) processFinal {
    if ([self.usernameTextField.text length] <= 0) {
        NSString *msg = @"Username must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([self.passwordTextField.text length] <= 0) {
        NSString *msg = @"Password must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([self.firstName.text length] <= 0) {
        NSString *msg = @"First name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    if ([self.lastName.text length] <= 0) {
        NSString *msg = @"Last name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [AccountProfile instance].username  = self.usernameTextField.text;
    [AccountProfile instance].password  = self.passwordTextField.text;
    [AccountProfile instance].firstName  = self.firstName.text;
    [AccountProfile instance].lastName  = self.lastName.text;
    
    [self addUser];
}

- (void)selectPaymentMethodViewController:(myPaymentMethodVC *)viewController
            didSelectPaymentMethodAtIndex:(NSUInteger)index {
    
}

- (void)selectPaymentMethodViewControllerDidRequestNew:(myPaymentMethodVC *)viewController {
    BTMainCC *bt = [[BTMainCC alloc] init];
    [self.navigationController pushViewController:bt animated:TRUE];
    
}
- (IBAction)touchPhoto:(id)sender {
    UserPhoto *up = [[UserPhoto alloc] initWithNibName:@"UserPhoto" bundle:nil];
    [self.navigationController pushViewController:up animated:TRUE];
}

- (void) updatePhoto:(NSNotification *) notification {
    //[self.editLabel setHidden:TRUE];
    self.userImage.image = [AccountProfile instance].driverImage;
    [ValetStorage sharedModel].currentDriver.driverImage = [AccountProfile instance].driverImage;
}

- (void) brainTreeLoaded:(NSNotification *) notification {
    myPaymentMethodVC *selectPaymentMethod = [[myPaymentMethodVC alloc] init];
    selectPaymentMethod.title = @"Test";
    selectPaymentMethod.theme = [BTUI braintreeTheme];
    selectPaymentMethod.paymentMethods = [CreditCardProcessing sharedModel].paymentMethods;
    //selectPaymentMethod.selectedPaymentMethodIndex = 0;
    selectPaymentMethod.delegate = self;
    selectPaymentMethod.client = [CreditCardProcessing sharedModel].braintree.client;
    
    [self.waitView setHidden:TRUE];
    [self.navigationController pushViewController:selectPaymentMethod animated:TRUE];
}

- (void) brainTreeLoadFailed:(NSNotification *) notification {
    [self.waitView setHidden:TRUE];
}


@end
