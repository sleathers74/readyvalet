//
//  Vehicle.h
//  Valet
//
//  Created by Matthew Schmidgall on 1/5/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValetOwner : NSObject

@property (strong,nonatomic) NSString *company_name;
@property (strong,nonatomic) NSString *owner_id;
@property (strong,nonatomic) NSString *last_name;
@property (strong,nonatomic) NSString *first_name;
@property (strong,nonatomic) NSString *phone_number;
@property (strong,nonatomic) UIImage *company_logo;

- (id) initWithName:(NSString*)company_name;



@end
