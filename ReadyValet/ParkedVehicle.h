//
//  ParkedVehicle.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/16/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "WaitView.h"

@interface ParkedVehicle : UIViewController {
    
}
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
- (IBAction)touchCallHiker:(id)sender;

@property (strong, nonatomic) IBOutlet UIStepper *tipStepper;
- (IBAction)tipTouched:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *tipLabel;
@property (strong, nonatomic) IBOutlet UILabel *valetLabel;
@property (strong, nonatomic) IBOutlet UILabel *carLabel;
@property (strong, nonatomic) IBOutlet UILabel *valetServiceCharge;
@property (strong, nonatomic) WaitView *waitView;

@end
