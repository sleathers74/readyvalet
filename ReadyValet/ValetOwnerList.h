//
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ValetOwnerList : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate> {
}

@property (strong, nonatomic) IBOutlet UIToolbar *manageToolbar;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UITableView *valetTable;
@property (nonatomic, strong) NSMutableArray *valetArray;
@property (strong, nonatomic) IBOutlet UIView *waitView;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *editProfileBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *manageUsersBarButton;
@property (nonatomic, strong) NIKFontAwesomeIconFactory *factory;
@property (nonatomic, assign) NSInteger selectedCell;
- (IBAction)touchEditProfile:(id)sender;
- (IBAction)touchManageUsers:(id)sender;

@end
