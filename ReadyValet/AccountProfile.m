//
//
//  Created by Scott Leathers on 3/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "AccountProfile.h"

@implementation AccountProfile

@synthesize firstName;
@synthesize lastName;
@synthesize username;
@synthesize password;
@synthesize creditCardNum;
@synthesize securityCode;
@synthesize expirationDate;
@synthesize make;
@synthesize model;
@synthesize color;
@synthesize license;
@synthesize autoDetect;
@synthesize licenseImage;

static AccountProfile *_instance = nil;

+(AccountProfile *)instance
{
    if (!_instance) {
        @synchronized([AccountProfile class])
        {
            if (!_instance) {
                _instance = [self new];
            }
        }
    }
    return _instance;
}

-(id)init
{
    self = [super init];
    if (self != nil) {
        firstName = @"";
        lastName = @"";
        username = @"";
        lastName = @"";
        creditCardNum = @"";
        securityCode = @"";
        expirationDate = @"";
        make = @"";
        model = @"";
        color = @"";
        license = @"";
        autoDetect = 1;
        licenseImage = nil;
    }
    return self;
}

-(void)dealloc
{
    //[super dealloc];
    self.firstName = nil;
    self.lastName = nil;
    self.username = nil;
    self.password = nil;
    self.creditCardNum = nil;
    self.securityCode = nil;
    self.expirationDate = nil;
    self.make = nil;
    self.model = nil;
    self.color = nil;
    self.license = nil;
    self.licenseImage = nil;
}

@end