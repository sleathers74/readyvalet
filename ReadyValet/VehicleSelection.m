//
//  VehicleSelection.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "VehicleSelection.h"
#import "MakeSearch.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "Vehicle.h"
#import "CreateAccountView4.h"
#import "ValetStorage.h"
#import "SystemPrefs.h"

@interface VehicleSelection ()

@end

@implementation VehicleSelection

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:FALSE];
    
    self.vehiclesArray = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addedVehicle:)
                                                 name:@"addedVehicle"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelNewVehicle:)
                                                 name:@"cancelNewVehicle"
                                               object:nil];
    
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    self.factory.colors = @[[NIKColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    
    //[self.view setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    //[self.vehicleTable setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    
    [self setupLayout];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self getVehicles];
}

- (void) setupLayout {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    if (loggedin == 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    }
    else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"NEXT" style:UIBarButtonItemStylePlain target:self action:@selector(next)];
        
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
        self.navigationItem.leftBarButtonItem = leftBarButton;
    }
    
}

- (void) next {
    if ([self.vehiclesArray count] <= 0) {
        NSString *msg = @"You must add at least 1 vehicle.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    CreateAccountView4 *cav4 = [[CreateAccountView4 alloc] initWithNibName:@"CreateAccountView4" bundle:nil];
    //cav2.title = @"Step 2 - Identification";
    [self.navigationController pushViewController:cav4 animated:TRUE];
    cav4 = nil;
}

- (void) cancel {
    [self.navigationController popToRootViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(__unused UITableView *)tableView numberOfRowsInSection:(__unused NSInteger)section
{
    return (self.vehiclesArray ? [self.vehiclesArray count] + 1 : 1);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@" denotes default vehicle" forState:UIControlStateNormal];
    
    NIKFontAwesomeIcon icon = NIKFontAwesomeIconCheck;
    [button setImage:[self.factory createImageForIcon:icon] forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor clearColor]];
    button.frame = CGRectMake(15, 0, tableView.frame.size.width, 44);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    
    [view addSubview:button];
    [view setBackgroundColor:[UIColor clearColor]];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *paymentMethodCellIdentifier = @"paymentMethodCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:paymentMethodCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:paymentMethodCellIdentifier];
    }
    cell.backgroundColor = [UIColor whiteColor];
    if ([self.vehiclesArray count] == 0 || indexPath.row > [self.vehiclesArray count] - 1) {
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:@"Add Vehicle"];
        [typeWithDescription addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f] range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;

        NIKFontAwesomeIcon icon = NIKFontAwesomeIconPlus;
        UIImage *image = [self.factory createImageForIcon:icon];
        cell.imageView.contentMode = UIViewContentModeCenter;
        cell.imageView.image = image;
        cell.accessoryView = nil;
    }
    else {
        Vehicle *v = [self.vehiclesArray objectAtIndex:indexPath.row];
        if ([v.vehicledefault isEqualToString:@"1"]) {
            NIKFontAwesomeIcon icon = NIKFontAwesomeIconCheck;
            UIImage *image = [self.factory createImageForIcon:icon];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
            button.frame = frame;
            [button setBackgroundImage:image forState:UIControlStateNormal];
            button.backgroundColor = [UIColor clearColor];
            cell.accessoryView = button;
        }
        else {
            cell.accessoryView = nil;
        }

        NSString *vString = [NSString stringWithFormat:@"%@ %@ %@", v.vehicleColor, v.vehicleManufacturer, v.vehicleModel];
        NSMutableAttributedString *typeWithDescription = [[NSMutableAttributedString alloc] initWithString:vString];
        [typeWithDescription addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f] range:NSMakeRange(0, [typeWithDescription length])];
        cell.textLabel.attributedText = typeWithDescription;
        cell.imageView.image = nil;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }*/
}

- (void)tableView:(__unused UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.vehiclesArray count] > 0 && indexPath.row <= ([self.vehiclesArray count] - 1)) {
        self.selectedPaymentMethodIndex = indexPath.row;
        
        for (Vehicle *vehic in self.vehiclesArray) {
            vehic.vehicledefault = @"0";
        }
        
        Vehicle *v = [self.vehiclesArray objectAtIndex:indexPath.row];
        v.vehicledefault = @"1";
        [ValetStorage sharedModel].currentVehicle = v;
        [ValetStorage sharedModel].currentVehicleId = v.vehicleId;
        
        [self.vehicleTable reloadData];
        [self setDefaultVehicle:@"1" vehicleID:v.vehicleId];
    }
    else {
        MakeSearch *ms = [[MakeSearch alloc] initWithNibName:@"MakeSearch" bundle:nil];
        [self.navigationController pushViewController:ms animated:FALSE];
    }
    //[self.vehicleTable reloadData];
}

- (void) addedVehicle:(NSNotification *) notification {
    [self addServerVehicle];
    [self.navigationController popToViewController:self animated:TRUE];
}

- (void) cancelNewVehicle:(NSNotification *) notification {
    [self.navigationController popToViewController:self animated:TRUE];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if (indexPath.row > [self.vehiclesArray count])
        return NO;
    else
        return YES;
}

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 55.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    return [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkmark.png"]];
}*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self deleteVehicle:indexPath];
    }
}

- (void) deleteVehicle:(NSIndexPath *)indexPath {
    Vehicle *v = [self.vehiclesArray objectAtIndex:indexPath.row];
    NSString *vehicleMsg = [NSString stringWithFormat:@"Are you sure you want to delete you vehicle '%@ %@'?", v.vehicleManufacturer, v.vehicleModel];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:vehicleMsg
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Yes",nil];
    actionSheet.tag = indexPath.row;
    [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        Vehicle *v = [self.vehiclesArray objectAtIndex:actionSheet.tag];
        [self removeVehicle:v.vehicleId];
        [self.vehiclesArray removeObjectAtIndex:actionSheet.tag];
        [self.vehicleTable reloadData];
    }
}

-(void) getVehicles {
        self.apiManager = [AFHTTPRequestOperationManager manager];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *username = [defaults stringForKey:@"username"];
        NSString *password = [defaults stringForKey:@"password"];
        
        NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                  username, @"username",
                                  password, @"password",
                                  nil];
    
        NSString *url = [NSString stringWithFormat:@"%@getallVehicles", [SystemPrefs instance].getURL];
        [self.apiManager POST:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            
            [self.vehiclesArray removeAllObjects];
            for (NSString *vehicleID in responseObject) {
                NSDictionary *vehicle = [responseObject objectForKey:vehicleID];
                Vehicle *v = [[Vehicle alloc] initWithVehicle:[vehicle objectForKey:@"make"]
                                                        model:[vehicle objectForKey:@"model"]
                                                        color:[vehicle objectForKey:@"color"]];
                v.vehicleId = [vehicle objectForKey:@"vehicle_id"];
                if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSString class]])
                    v.vehicledefault = [vehicle objectForKey:@"defaultVehicle"];
                else if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSNumber class]])
                    v.vehicledefault = [[vehicle objectForKey:@"defaultVehicle"] stringValue];
                else
                    v.vehicledefault = @"0";
                [self.vehiclesArray addObject:v];
                [self.vehicleTable reloadData];
            }
            [self checkForDefaults];
            [self.waitView setHidden:TRUE];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [self.waitView setHidden:TRUE];
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }];
}

-(void) removeVehicle:(NSString*) vehicleID {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@vehicle/%@", [SystemPrefs instance].getURL, vehicleID];
    [self.apiManager DELETE:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self checkForDefaults];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

-(void) addServerVehicle {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              [AccountProfile instance].make, @"make",
                              [AccountProfile instance].model, @"model",
                              [AccountProfile instance].color, @"color",
                              @"2014", @"year",
                              nil];
    if ([AccountProfile instance].licenseImage) {
        NSData *imageData = UIImageJPEGRepresentation([AccountProfile instance].licenseImage, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"licenseImage"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@vehicle", [SystemPrefs instance].getURL];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.vehiclesArray removeAllObjects];
        NSDictionary *newVehicles = [responseObject objectForKey:@"vehicle"];
        for (NSString *vehicleID in newVehicles) {
            NSDictionary *vehicle = [newVehicles objectForKey:vehicleID];
            Vehicle *v = [[Vehicle alloc] initWithVehicle:[vehicle objectForKey:@"make"]
                                                    model:[vehicle objectForKey:@"model"]
                                                    color:[vehicle objectForKey:@"color"]];
            v.vehicleId = vehicleID;
            
            if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSString class]])
                v.vehicledefault = [vehicle objectForKey:@"defaultVehicle"];
            else if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSNumber class]])
                v.vehicledefault = [[vehicle objectForKey:@"defaultVehicle"] stringValue];
            else
                v.vehicledefault = @"0";
            
            [self.vehiclesArray addObject:v];
            [self.vehicleTable reloadData];
            [self checkForDefaults];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

-(void) setDefaultVehicle:(NSString*) isdefault vehicleID:(NSString*) vehicleID {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              isdefault, @"defaultVehicle",
                              nil];
    
    NSString *urlString = [NSString stringWithFormat:@"%@updatevehicle/%@", [SystemPrefs instance].getURL, vehicleID];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        /*[self.vehiclesArray removeAllObjects];
        NSDictionary *newVehicles = [responseObject objectForKey:@"vehicle"];
        for (NSString *vehicleID in newVehicles) {
            NSDictionary *vehicle = [newVehicles objectForKey:vehicleID];
            Vehicle *v = [[Vehicle alloc] initWithVehicle:[vehicle objectForKey:@"make"]
                                                    model:[vehicle objectForKey:@"model"]
                                                    color:[vehicle objectForKey:@"color"]];
            v.vehicleId = vehicleID;
            
            
            if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSString class]])
                v.vehicledefault = [vehicle objectForKey:@"defaultVehicle"];
            else if ([[vehicle objectForKey:@"defaultVehicle"] isKindOfClass:[NSNumber class]])
                v.vehicledefault = [[vehicle objectForKey:@"defaultVehicle"] stringValue];
            else
                v.vehicledefault = @"0";
            
            [self.vehiclesArray addObject:v];
        }
        [self.vehicleTable reloadData];*/
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}


-(void) checkForDefaults {
    bool bfound = false;
    for (Vehicle *v in self.vehiclesArray) {
        if ([v.vehicledefault isEqualToString:@"1"]) {
            bfound = true;
            break;
        }
    }
    if (bfound == false && [self.vehiclesArray count] > 0) {
        Vehicle *v = [self.vehiclesArray objectAtIndex:0];
        [self setDefaultVehicle:@"1" vehicleID:v.vehicleId];
    }
        
}
@end
