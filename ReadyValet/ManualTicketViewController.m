//
//  ManualTicketViewController.m
//  ReadyValet
//
//  Created by Scott Leathers on 12/30/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ManualTicketViewController.h"
#import "UtilFunctions.h"
#import "AFNetworking.h"
#import "ValetStorage.h"
#import "SystemPrefs.h"

@interface ManualTicketViewController ()

@end

@implementation ManualTicketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SAVE" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    
    [self.paperTicketNumber setText:self.ticketNumber];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[ValetStorage sharedModel].currentValetName length] > 0)
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Manual Ticket", [ValetStorage sharedModel].currentValetName]];
    else
        [[SystemPrefs instance] sendGoogleView:[NSString stringWithFormat:@"%@ - Manual Ticket", [ValetStorage sharedModel].valetOwner.company_name]];
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
    //[self removeFromParentViewController];
}

-(void)save {
    [self processTicket];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) processTicket {
    AFHTTPRequestOperationManager *apiManager;
    apiManager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              [ValetStorage sharedModel].currentValetId, @"valet_id",
                              self.paperTicketNumber.text, @"manual_ticket_number",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@createmanualticket", [[SystemPrefs instance] getURL]];
    [apiManager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self.navigationController popViewControllerAnimated:TRUE];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];

}

@end
