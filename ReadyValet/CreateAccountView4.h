//
//  CreateAccountView2.h
//  ReadyValet
//
//  Created by Scott Leathers on 8/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"


@interface CreateAccountView4 : UIViewController <UIAlertViewDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIButton *touchFinish;
@property (strong, nonatomic) IBOutlet UISwitch *autoCheckingSwitch;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet UILabel *defaultTipLabel;
@property (strong, nonatomic) IBOutlet UIStepper *tipStepper;
- (IBAction)tipTouched:(id)sender;



@end
