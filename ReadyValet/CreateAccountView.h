//
//  CreateAccountView.h
//  ReadyValet
//
//  Created by Scott Leathers on 8/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSKeyboardControls.h"
#import "AFNetworking.h"
#import "myPaymentMethodVC.h"
#import "SHSPhoneLibrary.h"

#import "Braintree-API.h"

#import "BTUI.h"

@interface CreateAccountView : UIViewController <BSKeyboardControlsDelegate, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate, myPaymentMethodVCDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (nonatomic,strong) NSString *currentHeaderText;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
- (IBAction)touchPayment:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *userText;

@property (strong, nonatomic) IBOutlet UIView *waitView;

@property (nonatomic, strong) BTUI *theme;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
- (IBAction)touchPhoto:(id)sender;

@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneNumberText;

@end
