//
//  ReceiptsDetail.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ReceiptsDetail.h"
#import "UtilFunctions.h"
#import "ReceiptsEmail.h"
#import "AFNetworking.h"
#import "SystemPrefs.h"

@interface ReceiptsDetail ()

@end

@implementation ReceiptsDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    /*self.trans_id = [self.detailInfo objectForKey:@"id"];
    
    if ([[self.detailInfo objectForKey:@"valetcompany"] isKindOfClass:[NSString class]])
        self.valetCompanyLabel.text = [self.detailInfo objectForKey:@"valetcompany"];
    
    if ([[self.detailInfo objectForKey:@"amount"] isKindOfClass:[NSString class]])
        self.amountLabel.text = [NSString stringWithFormat:@"$%@", [self.detailInfo objectForKey:@"amount"]];
    
    if ([[self.detailInfo objectForKey:@"vehicle"] isKindOfClass:[NSString class]])
        self.vehicle.text = [self.detailInfo objectForKey:@"vehicle"];
    
    if ([[self.detailInfo objectForKey:@"ticketnumber"] isKindOfClass:[NSString class]])
        self.ticketNumLabel.text = [self.detailInfo objectForKey:@"ticketnumber"];
    
    if ([[self.detailInfo objectForKey:@"created"] isKindOfClass:[NSString class]])
        self.dateLabel.text = [self.detailInfo objectForKey:@"created"];
    
    if ([[self.detailInfo objectForKey:@"status"] isKindOfClass:[NSString class]])
        self.statusLabel.text = [self.detailInfo objectForKey:@"status"];
    
    if ([[self.detailInfo objectForKey:@"cardholderName"] isKindOfClass:[NSString class]])
        self.cardholderLabel.text = [self.detailInfo objectForKey:@"cardholderName"];
    
    if ([[self.detailInfo objectForKey:@"cardType"] isKindOfClass:[NSString class]])
        self.cardtypeLabel.text = [self.detailInfo objectForKey:@"cardType"];
    
    if ([[self.detailInfo objectForKey:@"last4"] isKindOfClass:[NSString class]])
        self.cardnumLabel.text = [self.detailInfo objectForKey:@"last4"];
    
    //if ([[self.detailInfo objectForKey:@"customername"] isKindOfClass:[NSString class]])
    //    self.customerName.text = [self.detailInfo objectForKey:@"customername"];*/
    
    [self getReceipt];
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchEmail:(id)sender {
    ReceiptsEmail *vs = [[ReceiptsEmail alloc] initWithNibName:@"ReceiptsEmail" bundle:nil];
    vs.trans_id = self.trans_id;
    [self.navigationController pushViewController:vs animated:TRUE];
}

- (void) getReceipt {
    [self.waitView setHidden:FALSE];
    AFHTTPRequestOperationManager *apiManager = [AFHTTPRequestOperationManager manager];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              self.trans_id, @"transaction_id",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@hikertransactions", [[SystemPrefs instance] getURL]];
    [apiManager GET:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        for (NSString *id in responseObject) {
            NSDictionary *infoDict = [responseObject objectForKey:id];
            
            self.trans_id = [infoDict objectForKey:@"id"];
            
            if ([[infoDict objectForKey:@"valetcompany"] isKindOfClass:[NSString class]])
                self.valetCompanyLabel.text = [infoDict objectForKey:@"valetcompany"];
            
            if ([[infoDict objectForKey:@"amount"] isKindOfClass:[NSString class]])
                self.amountLabel.text = [NSString stringWithFormat:@"$%@", [infoDict objectForKey:@"amount"]];
            
            if ([[infoDict objectForKey:@"vehicle"] isKindOfClass:[NSString class]])
                self.vehicle.text = [infoDict objectForKey:@"vehicle"];
            
            if ([[infoDict objectForKey:@"ticketnumber"] isKindOfClass:[NSString class]])
                self.ticketNumLabel.text = [infoDict objectForKey:@"ticketnumber"];
            
            if ([[infoDict objectForKey:@"created"] isKindOfClass:[NSString class]])
                self.dateLabel.text = [infoDict objectForKey:@"created"];
            
            if ([[infoDict objectForKey:@"status"] isKindOfClass:[NSString class]])
                self.statusLabel.text = [infoDict objectForKey:@"status"];
            
            if ([[infoDict objectForKey:@"cardholderName"] isKindOfClass:[NSString class]])
                self.cardholderLabel.text = [infoDict objectForKey:@"cardholderName"];
            
            if ([[infoDict objectForKey:@"cardType"] isKindOfClass:[NSString class]])
                self.cardtypeLabel.text = [infoDict objectForKey:@"cardType"];
            
            if ([[infoDict objectForKey:@"last4"] isKindOfClass:[NSString class]])
                self.cardnumLabel.text = [infoDict objectForKey:@"last4"];
            
            //if ([[infoDict objectForKey:@"customername"] isKindOfClass:[NSString class]])
            //    self.customerName.text = [infoDict objectForKey:@"customername"];
        }
        [self.waitView setHidden:TRUE];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
        [self.waitView setHidden:TRUE];
    }];
}

@end
