//
//  ReceiptsDetail.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Ticket;

@interface ValetReceiptsDetail : UIViewController {
    
}
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet UILabel *valetCompanyLabel;
@property (strong, nonatomic) IBOutlet UILabel *amountLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardholderLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardtypeLabel;
@property (strong, nonatomic) IBOutlet UILabel *cardnumLabel;
@property (strong, nonatomic) IBOutlet UILabel *ticketNumLabel;
@property (strong, nonatomic) IBOutlet UILabel *vehicle;
@property (strong, nonatomic) IBOutlet UILabel *customerName;
- (IBAction)touchEmail:(id)sender;
@property (strong, nonatomic) NSString *trans_id;

@property (strong, nonatomic) Ticket *ticket;
@end
