//
//  CreateAccountView2.m
//  ReadyValet
//
//  Created by Scott Leathers on 8/11/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "CreateAccountView4.h"
#import "ValetStorage.h"
#import "SystemPrefs.h"
#import "AccountProfile.h"
#import "UtilFunctions.h"

@interface CreateAccountView4 ()
@end

@implementation CreateAccountView4

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SAVE" style:UIBarButtonItemStylePlain target:self action:@selector(didTapDone)];
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    
    [self.waitView setHidden:TRUE];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    long loggedin = [defaults integerForKey:@"loggedin"];
    if (loggedin == 1) {
        if ([ValetStorage sharedModel].autoDetect == 1)
            [self.autoCheckingSwitch setOn:TRUE];
        else
            [self.autoCheckingSwitch setOn:FALSE];
    }
    else {
        [self.autoCheckingSwitch setOn:TRUE];
    }
    
    self.navigationController.navigationBar.topItem.title = @"";
}


-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didTapDone {
    [self touchFinish:nil];
}

- (IBAction) cancel:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:FALSE];
}

-(void) dealloc {
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchFinish:(id)sender {
    [self.waitView setHidden:FALSE];
    
    if (self.autoCheckingSwitch.isOn) {
        [AccountProfile instance].autoDetect  = 1;
        [ValetStorage sharedModel].autoDetect = 1;
    }
    else {
        [AccountProfile instance].autoDetect = 0;
        [ValetStorage sharedModel].autoDetect = 0;
    }
    
    
    NSNumber *autoD = [NSNumber numberWithInteger:[AccountProfile instance].autoDetect];
    
    /*if ([ValetStorage sharedModel].autoDetect == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startMonitoring" object:self];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopMonitoring" object:self];
    }*/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    self.apiManager = [AFHTTPRequestOperationManager manager];
    
    //Driver *d = [ValetStorage sharedModel].currentDriver;

    NSMutableDictionary *postData = [[NSMutableDictionary alloc] init];
    [postData setObject:[AccountProfile instance].firstName forKey:@"first_name"];
    [postData setObject:[AccountProfile instance].lastName forKey:@"last_name"];
    [postData setObject:[AccountProfile instance].creditCardNum forKey:@"card_number"];
    [postData setObject:[AccountProfile instance].expirationDate forKey:@"card_expiration"];
    [postData setObject:autoD forKey:@"auto_detect"];
    
    if ([AccountProfile instance].driverImage) {
        NSData *imageData = UIImageJPEGRepresentation([AccountProfile instance].driverImage, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"driverImage"];
    }
    
    long loggedin = 1; //[defaults integerForKey:@"loggedin"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/", [[SystemPrefs instance] full_url]];
    if (loggedin == 1) {
        urlString = [NSString stringWithFormat:@"%@updatedriver", [[SystemPrefs instance] full_url]];
        [postData setObject:username forKey:@"username"];
        [postData setObject:password forKey:@"password"];
        [postData setObject:[AccountProfile instance].username forKey:@"newusername"];
        [postData setObject:[AccountProfile instance].password forKey:@"newpassword"];
        [postData setObject:[AccountProfile instance].username forKey:@"email"];
        [postData setObject:[NSString stringWithFormat:@"%.02f", self.tipStepper.value] forKey:@"defaultTip"];
        username = [AccountProfile instance].username;
        password = [AccountProfile instance].password;
    }
    else {
        urlString = [NSString stringWithFormat:@"%@driver", [[SystemPrefs instance] full_url]];
        NSString *newuser = [AccountProfile instance].username;
        NSString *newpass = [AccountProfile instance].password;
        [postData setObject:newuser forKey:@"username"];
        [postData setObject:newpass forKey:@"password"];
        [postData setObject:newuser forKey:@"email"];
        username = newuser;
        password = newpass;
    }
    
    [self.apiManager POST:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //Driver *d = [ValetStorage sharedModel].currentDriver;
        self.apiManager = [AFHTTPRequestOperationManager manager];
        //[self.apiManager.requestSerializer setAuthorizationHeaderFieldWithUsername:d.emailAddress password:d.password];
        
            //Vehicle* v = [ValetStorage sharedModel].currentVehicle;
            NSMutableDictionary *oldpostData = [[NSMutableDictionary alloc] init];
            [oldpostData setObject:[AccountProfile instance].make forKey:@"make"];
            [oldpostData setObject:[AccountProfile instance].model forKey:@"model"];
            [oldpostData setObject:[AccountProfile instance].color forKey:@"color"];
            if ([[AccountProfile instance].license length] > 0)
                [oldpostData setObject:[AccountProfile instance].license forKey:@"license"];
            [oldpostData setObject:@"2014" forKey:@"year"];
            [oldpostData setObject:[AccountProfile instance].username forKey:@"username"];
            [oldpostData setObject:[AccountProfile instance].password forKey:@"password"];
        
            NSString *vehicleURL;
            vehicleURL = [NSString stringWithFormat:@"%@vehicle", [[SystemPrefs instance] full_url]];
            
            [self.apiManager POST:vehicleURL parameters:oldpostData success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
                
                
                [ValetStorage sharedModel].profileId = [[responseObject objectForKey:@"driver"] objectForKey:@"driver_id"];
                [ValetStorage sharedModel].currentVehicleId = [[[responseObject objectForKey:@"vehicle"] allKeys] objectAtIndex:0];
                Driver *d = [[Driver alloc] init];
                d.emailAddress = [[responseObject objectForKey:@"driver"] objectForKey:@"email"];
                d.firstName = [[responseObject objectForKey:@"driver"] objectForKey:@"first_name"];
                d.lastName = [[responseObject objectForKey:@"driver"] objectForKey:@"last_name"];
                d.phone = [[responseObject objectForKey:@"driver"] objectForKey:@"phone"];
                d.creditCardNumber = [[responseObject objectForKey:@"driver"] objectForKey:@"card_number"];
                d.creditCardExpiration = [[responseObject objectForKey:@"driver"] objectForKey:@"card_expiration"];
                d.defaultTip = [[responseObject objectForKey:@"driver"] objectForKey:@"defaultTip"];
                
                NSString *driverImage = [[responseObject objectForKey:@"driver"] objectForKey:@"driverImage"];
                if ([driverImage length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:driverImage options:0];
                    d.driverImage = [UIImage imageWithData:imageData];
                }

                
                [ValetStorage sharedModel].autoDetect = [[[responseObject objectForKey:@"driver"] objectForKey:@"auto_detect"] integerValue];
                NSString *vehicleId;
                NSDictionary *v;
                [ValetStorage sharedModel].currentVehicle = nil;
                for (vehicleId in [responseObject objectForKey:@"vehicle"]) {
                    v = [[responseObject objectForKey:@"vehicle"] objectForKey:vehicleId];
                    Vehicle *newVehicle =[[Vehicle alloc] initWithVehicle:[v objectForKey:@"make"] model:[v objectForKey:@"model"] color:[v objectForKey:@"color"]];
                    newVehicle.license = [v objectForKey:@"license"];
                    newVehicle.vehicleId = vehicleId;
                    [d.vehicles addObject:newVehicle];
                    [ValetStorage sharedModel].currentVehicle = newVehicle;
                }
                
                if ([responseObject objectForKey:@"valet"]) {
                    NSDictionary *valetDict = [responseObject objectForKey:@"valet"];
                    [ValetStorage sharedModel].currentValetId = [valetDict objectForKey:@"valet_id"];
                    [ValetStorage sharedModel].currentValetName = [valetDict objectForKey:@"company"];
                    [ValetStorage sharedModel].currentValetFee = [valetDict objectForKey:@"valet_fee"];
                    [ValetStorage sharedModel].currentServiceFee = [valetDict objectForKey:@"service_fee"];
                    [ValetStorage sharedModel].currentMerchantId = [valetDict objectForKey:@"merchant_id"];
                    [ValetStorage sharedModel].currentMerchantStatus = [valetDict objectForKey:@"merchant_status"];
                }
                [ValetStorage sharedModel].currentDriver = d;
                [self.waitView setHidden:TRUE];
                
                //[self dismissViewControllerAnimated:YES completion:nil];
                NSString *message = @"Congratulations your Hiker Valet account has been created.";
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                long loggedin = [defaults integerForKey:@"loggedin"];
                if (loggedin == 1) {
                    message = @"Your Hiker Valet profile has been updated.";
                }
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                alert.delegate = self;
                alert.tag = 100;
                [alert show];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self.waitView setHidden:TRUE];
                NSLog(@"Error: %@", error);
                [UtilFunctions displayError:self msg:error.localizedDescription];
            }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [ValetStorage sharedModel].currentDriver = nil;
        NSLog(@"Error: %@", error);
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 100) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            long loggedin = [defaults integerForKey:@"loggedin"];
            // send your notification here instead of in updateFunction
            [self.navigationController popToRootViewControllerAnimated:TRUE];
            

            if (loggedin == 1)
                [[NSNotificationCenter defaultCenter] postNotificationName:@"changeControllers" object:self];
            else
                [[NSNotificationCenter defaultCenter] postNotificationName:@"login" object:self];
        });
    }
}
- (IBAction)tipTouched:(id)sender {
    NSString *paidAmt =  [NSString stringWithFormat:@"$%.02f", self.tipStepper.value];
    [self.defaultTipLabel setText:paidAmt];
}
@end
