#import <Foundation/Foundation.h>

@interface UtilFunctions : NSObject {}

+ (UILabel*)buildCustomNav;
+ (UIView*) buildCustomNav:(NSString*) statusString;
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;
+ (void) displayError:(NSObject*) delegate msg:(NSString*) msg;
+ (void) addLeftPrefix:(UITextField*) tv prefix:(NSString*) prefix;

@end