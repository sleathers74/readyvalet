//
//  Vehicle.m
//  Valet
//
//  Created by Matthew Schmidgall on 1/5/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import "Vehicle.h"
#import "AFNetworking.h"
#import "SystemPrefs.h"

@implementation Vehicle

- (id) initWithName:(NSString*)vehicleName {
    self = [super init];
//    self.vehicleName = [[NSString alloc] initWithString:vehicleName];
    self.vehicleName = vehicleName;
    return self;
}

- (id) initWithVehicle:(NSString*)vehicleManufacturer model:(NSString*)vehicleModel color:(NSString*)vehicleColor {
    self = [super init];
    self.vehicleManufacturer = vehicleManufacturer;
    self.vehicleColor = vehicleColor;
    self.vehicleModel = vehicleModel;
    return self;
}

- (void) StringToImage:(NSString*) imageString {
    if ([imageString isKindOfClass:[NSString class]]) {
        if ([imageString length] > 0) {
            NSData* imageData = [[NSData alloc] initWithBase64EncodedString:imageString options:0];
            [self setDriverImage:[UIImage imageWithData:imageData]];
        }
    }
}

- (NSString*) ImageToString {
    NSString *baseString = @"";
    if (self.driverImage) {
        NSData *imageData = UIImageJPEGRepresentation(self.driverImage, 0.1);
        baseString = [imageData base64Encoding];
    }
    return baseString;
}

-(void) getDriverImage {
    AFHTTPRequestOperationManager *apiManager;
    apiManager = [AFHTTPRequestOperationManager manager];
    
    NSString *url = [NSString stringWithFormat:@"%@driverimage/%@", [SystemPrefs instance].getURL, self.vehicleId];
    
    [apiManager POST:url parameters:nil   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if (![[responseObject objectForKey:@"driverImage"] isKindOfClass:[NSNull class]])
            [self StringToImage:[responseObject objectForKey:@"driverImage"]];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
@end
