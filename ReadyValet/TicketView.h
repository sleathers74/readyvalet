//
//  TicketView.h
//  ReadyValet
//
//  Created by Scott Leathers on 8/12/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketView : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *vehicleLabel;
@property (strong, nonatomic) IBOutlet UILabel *ticketLabel;
@property (strong, nonatomic) IBOutlet UIView *waitView;
- (IBAction)touchDone:(id)sender;

@end
