//
//  ReceiptsEmail.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/15/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ReceiptsEmail.h"
#import "AFNetworking.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

@interface ReceiptsEmail ()

@end

@implementation ReceiptsEmail

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:TRUE];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    
    self.emailTextField.text = username;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.emailTextField.leftView = paddingView;
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    paddingView = nil;
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchSend:(id)sender {
    if ([UtilFunctions NSStringIsValidEmail:self.emailTextField.text]) {
        [self.waitView setHidden:FALSE];
        [self send];
    }
    else {
        [UtilFunctions displayError:self msg:@"Invalid email address."];
    }
}

- (void) send {
    AFHTTPRequestOperationManager *apiManager = [AFHTTPRequestOperationManager manager];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              self.emailTextField.text, @"email",
                              nil];
    
    NSString *url = [NSString stringWithFormat:@"%@email/%@", [[SystemPrefs instance] getURL], self.trans_id];
    [apiManager POST:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [self.waitView setHidden:TRUE];
        [self cancel];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}
@end
