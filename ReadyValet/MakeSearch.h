//
//  MakeSearch.h
//
//  Created by Scott Leathers on 7/25/13.
//  Copyright (c) 2013 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MakeSearch : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate> {
    BOOL            isSearching;
    NSMutableArray  *filteredList;
    NSArray         *items;
}
@property (strong, nonatomic) IBOutlet UITableView *myTable;

@end
