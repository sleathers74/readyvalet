//
//  UserPhoto.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "UserPhoto.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface UserPhoto ()

@end

@implementation UserPhoto

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SUBMIT" style:UIBarButtonItemStylePlain target:self action:@selector(submit)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void) submit {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatePhoto" object:self];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.navigationItem.rightBarButtonItem.title= @"SUBMIT";
    bPicture = true;
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (chosenImage == nil)
        chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];

    if ([[ValetStorage sharedModel].role isEqualToString:@"valet_owner"])
        [ValetStorage sharedModel].valetOwner.company_logo = chosenImage;
    else
        [AccountProfile instance].driverImage = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.driverImage.image = chosenImage;
    self.driverImage.hidden = FALSE;
    [self.photoButton setTitle:@"" forState:UIControlStateNormal];
    
}

- (void)snapPicture {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.mediaTypes = @[(NSString *)kUTTypeImage];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    //picker.sourceType = UIImagePickerControllerEditedImage;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)fakeButton:(id)sender {
    [self snapPicture];
}

@end
