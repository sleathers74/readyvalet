//
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myPaymentMethodVC.h"

@interface ValetProfile : UIViewController <myPaymentMethodVCDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIButton *buttonBackground;

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
- (IBAction)touchValet:(id)sender;
- (IBAction)touchManage:(id)sender;
- (IBAction)touchProfile:(id)sender;
- (IBAction)touchBottomButton:(id)sender;
- (IBAction)touchReceipts:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *findValetButton;
@property (strong, nonatomic) IBOutlet UIButton *manageButton;
@property (strong, nonatomic) IBOutlet UIButton *receiptsButton;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;
- (IBAction)touchSettings:(id)sender;

@end
