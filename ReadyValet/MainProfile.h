//
//  MainProfile.h
//  ReadyValet
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myPaymentMethodVC.h"

@interface MainProfile : UIViewController <myPaymentMethodVCDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIButton *buttonBackground;

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
- (IBAction)touchValet:(id)sender;
- (IBAction)touchPayment:(id)sender;
- (IBAction)touchVehicle:(id)sender;
- (IBAction)touchProfile:(id)sender;
- (IBAction)touchBottomButton:(id)sender;
- (IBAction)touchReceipts:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *findValetButton;
@property (strong, nonatomic) IBOutlet UIButton *paymentButton;
@property (strong, nonatomic) IBOutlet UIButton *vehicleButton;
@property (strong, nonatomic) IBOutlet UIButton *receiptsButton;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;
- (IBAction)touchSettings:(id)sender;

@end
