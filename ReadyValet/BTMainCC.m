//
//  BTMainCC.m
//  BraintreeTest
//
//  Created by Scott Leathers on 11/5/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "BTMainCC.h"

// Braintree
#import "CreditCardProcessing.h"
#import "Braintree.h"
#import "CardIOPaymentViewController.h"
#import "CardIOCreditCardInfo.h"
#import "myPaymentMethodVC.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "AFNetworking.h"

@interface BTMainCC ()

@end

@implementation BTMainCC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:TRUE];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(noncePostedToServer:)
                                                 name:@"noncePostedToServer"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideWait:)
                                                 name:@"hideWait"
                                               object:nil];

    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelCardVC)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                                             target:self
                                                                                                             action:@selector(saveCardVC)];
    self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleDone;
    
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    
    [self.cardFormView setOptionalFields:BTUICardFormOptionalFieldsNone];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
    [self.cardFormView setBackgroundColor:[UIColor colorWithRed:0.937255f green:0.937255f blue:0.956863f alpha:1]];
   
    [CardIOPaymentViewController preload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cardFormViewDidChange:(BTUICardFormView *)cardFormView {
    if (cardFormView.valid) {
        self.number = cardFormView.number;
        self.expirationMonth = cardFormView.expirationMonth;
        self.expirationYear = cardFormView.expirationYear;
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }
}

- (IBAction)touchScanYourCard:(id)sender {
    CardIOPaymentViewController *cardIO = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self scanningEnabled:TRUE];
    cardIO.navigationBarTintColor = [UIColor colorWithRed: 0.0f/255.0f green:34.0f/255.0f blue:85.0f/255.0f alpha:1.0];
    //cardIO.keepStatusBarStyle = NO;
    cardIO.collectExpiry = YES;
    cardIO.collectCVV = YES;
    cardIO.useCardIOLogo = NO;
    cardIO.disableManualEntryButtons = YES;
    cardIO.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:cardIO animated:YES completion:nil];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)cardInfo inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    
    [self.waitView setHidden:FALSE];
    
    BTClientCardRequest *request = [BTClientCardRequest new];
    
    request.number = cardInfo.cardNumber;
    request.expirationMonth = [NSString stringWithFormat:@"%li", cardInfo.expiryMonth];
    request.expirationYear = [NSString stringWithFormat:@"%li", cardInfo.expiryYear];
    
    [[CreditCardProcessing sharedModel].braintree.client saveCardWithRequest:request
                                       success:^(BTCardPaymentMethod *card) {
                                            NSMutableArray *newPaymentMethods = [NSMutableArray arrayWithArray:self.paymentMethods];
                                            [newPaymentMethods insertObject:card atIndex:0];
                                            self.paymentMethods = newPaymentMethods;
                                           //[AccountProfile instance].creditCardNum = cardInfo.redactedCardNumber;
                                           //[AccountProfile instance].expirationDate = @"XX/XX";
                                       }
                                       failure:^(NSError *error) {
                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                           [alert show];
                                       }];
    
    [[CreditCardProcessing sharedModel].braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error){
        [[CreditCardProcessing sharedModel] postNonceToServer:nonce];
    }];
    
    [paymentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)cancelCardVC {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)saveCardVC {
    [self.waitView setHidden:FALSE];
    
    BTClientCardRequest *request = [BTClientCardRequest new];
    request.number = self.number;
    request.expirationMonth = self.expirationMonth;
    request.expirationYear = self.expirationYear;
    
    [[CreditCardProcessing sharedModel].braintree.client saveCardWithRequest:request
                                       success:^(BTCardPaymentMethod *card) {
                                           NSMutableArray *newPaymentMethods = [NSMutableArray arrayWithArray:self.paymentMethods];
                                           [newPaymentMethods insertObject:card atIndex:0];
                                           self.paymentMethods = newPaymentMethods;
                                           //[AccountProfile instance].creditCardNum = [NSString stringWithFormat:@"XXXXXXXXXXXXXX%@", card.lastTwo];
                                           //[AccountProfile instance].expirationDate = @"XX/XX";
                                       }
                                       failure:^(NSError *error) {
                                           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error.localizedDescription message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                           [alert show];
                                       }];
    
    [[CreditCardProcessing sharedModel].braintree tokenizeCard:request completion:^(NSString *nonce, NSError *error){
        [[CreditCardProcessing sharedModel] postNonceToServer:nonce];
    }];
}

- (void) noncePostedToServer:(NSNotification *) notification {
     [self.waitView setHidden:TRUE];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedCard" object:nil];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void) hideWait:(NSNotification *) notification {
    [self.waitView setHidden:TRUE];
}

@end
