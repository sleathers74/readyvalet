@import UIKit;

#import "Braintree-API.h"

#import "BTUI.h"
#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@protocol myPaymentMethodVCDelegate;

/// Drop In's payment method selection flow.
@interface myPaymentMethodVC : UITableViewController

@property (nonatomic, strong) BTClient *client;
@property (nonatomic, weak) id<myPaymentMethodVCDelegate> delegate;
@property (nonatomic, strong) NSArray *paymentMethods;
@property (nonatomic, assign) NSInteger selectedPaymentMethodIndex;

@property (nonatomic, strong) BTUI *theme;
@property (nonatomic, strong) NIKFontAwesomeIconFactory *factory;
@end

@protocol myPaymentMethodVCDelegate

- (void)selectPaymentMethodViewController:(myPaymentMethodVC *)viewController
            didSelectPaymentMethodAtIndex:(NSUInteger)index;

- (void)selectPaymentMethodViewControllerDidRequestNew:(myPaymentMethodVC *)viewController;

@end