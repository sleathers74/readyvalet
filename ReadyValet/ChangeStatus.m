//
//  ChangeStatus.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/1/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ChangeStatus.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "SystemPrefs.h"

@interface ChangeStatus ()

@end

@implementation ChangeStatus

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.waitView setHidden:FALSE];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    //self.view.backgroundColor = [UIColor colorWithRed:(37/255.0) green:(36/255.0) blue:(36/255.0) alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    if (self.ticket) {
        Vehicle *vehicle = self.ticket.vehicle;
        NSString *vehicleString = [NSString stringWithFormat:@"%@ %@ %@",vehicle.vehicleColor, vehicle.vehicleManufacturer, vehicle.vehicleModel];
        
        self.vehicleDesc.text = vehicleString;
        self.ticketId.text = self.ticket.ticketNumber;
        
        [self loadImage];
    }
    
    if ([self.status isEqualToString:@"1"])
        [self.statusButton setTitle:@"Move to Requested" forState:UIControlStateNormal];
    else if ([self.status isEqualToString:@"2"])
        [self.statusButton setTitle:@"Move to Retrieved" forState:UIControlStateNormal];
    else if ([self.status isEqualToString:@"3"])
        [self.statusButton setTitle:@"Move to Completed" forState:UIControlStateNormal];
    
    
    [self.segmentedControl addTarget:self
                         action:@selector(touchSegment:)
               forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
}

- (void) cancel {
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void) loadImage {
        if ([[ValetStorage sharedModel].role isEqualToString:@"valet"]) {
            self.apiManager = [AFHTTPRequestOperationManager manager];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *username = [defaults stringForKey:@"username"];
            NSString *password = [defaults stringForKey:@"password"];
            
            NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                                      username, @"username",
                                      password, @"password",
                                      nil];
            
            NSString *url = [NSString stringWithFormat:@"%@ticket/image/%@", [[SystemPrefs instance] getURL], self.ticket.ticketNumber];
            [self.apiManager GET:url parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
                
                if ([[responseObject objectForKey:@"image"] length] > 0) {
                    NSData* imageData = [[NSData alloc] initWithBase64EncodedString:[responseObject objectForKey:@"image"] options:0];
                    self.vehicleTicketImage.image = [UIImage imageWithData:imageData];
                    [self.vehicleTicketImage setHidden:FALSE];
                    [self.waitView setHidden:TRUE];
                }
                else {
                    [self.vehicleTicketImage setHidden:TRUE];
                    [self.waitView setHidden:TRUE];
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                [self.waitView setHidden:TRUE];
                [UtilFunctions displayError:self msg:error.localizedDescription];
            }];
        }
}

- (void)touchSegment:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    long changeStatus = selectedSegment;
    [self changeVehicleStatus:changeStatus+1];
    
}

- (void) changeVehicleStatus:(long) currentStatus {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    //[self.apiManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[ValetStorage sharedModel].username password:[ValetStorage sharedModel].password];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSDictionary *postData = [NSDictionary dictionaryWithObjectsAndKeys:
                              username, @"username",
                              password, @"password",
                              nil];
    
    [[[ValetStorage sharedModel].ticketQueues objectForKey:@"1"] removeObjectForKey:self.ticket.ticketNumber];
    
    if ([[ValetStorage sharedModel].ticketQueues objectForKey:[NSString stringWithFormat:@"%ld", currentStatus]] == nil) {
        [[ValetStorage sharedModel].ticketQueues setValue:[[NSMutableDictionary alloc] init] forKey:[NSString stringWithFormat:@"%ld", currentStatus+1]];
    }
    [[[ValetStorage sharedModel].ticketQueues objectForKey:[NSString stringWithFormat:@"%ld", currentStatus]] setValue:self.ticket forKey:self.ticket.ticketNumber];
    
    /*NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%ld", currentStatus] forKey:@"status"];
    [dict setObject:self.ticket.ticketNumber forKey:@"ticketid"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateStatus" object:nil userInfo:dict];
    [self.navigationController popViewControllerAnimated:TRUE];*/
    
    NSString *url = [NSString stringWithFormat:@"%@ticket/%@/%ld", [[SystemPrefs instance] getURL], self.ticket.ticketNumber, currentStatus];
    [self.apiManager PUT:url parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [self.navigationController popViewControllerAnimated:TRUE];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
        [self.navigationController popToRootViewControllerAnimated:true];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)touchedSubmit:(id)sender {
    if ([self.status isEqualToString:@"0"])
        [self changeVehicleStatus:1];
    else if ([self.status isEqualToString:@"1"])
        [self changeVehicleStatus:2];
    else if ([self.status isEqualToString:@"2"])
        [self changeVehicleStatus:3];
    else if ([self.status isEqualToString:@"3"])
        [self changeVehicleStatus:4];
    
}
@end
