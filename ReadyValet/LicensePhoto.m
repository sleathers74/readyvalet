//
//  ValetSelectionVC.m
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "LicensePhoto.h"
#import "ValetStorage.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface LicensePhoto ()

@end

@implementation LicensePhoto

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SKIP" style:UIBarButtonItemStylePlain target:self action:@selector(skip)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) cancel {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelNewVehicle" object:self];
}

- (void) skip {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedVehicle" object:self];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.navigationItem.rightBarButtonItem.title= @"Submit";
    bPicture = true;
    
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (chosenImage == nil)
        chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [AccountProfile instance].licenseImage = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    self.ticketImage.image = chosenImage;
    self.ticketImage.hidden = FALSE;
    [self.photoButton setTitle:@"" forState:UIControlStateNormal];
    
}

- (void)snapPicture {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.mediaTypes = @[(NSString *)kUTTypeImage];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    //picker.sourceType = UIImagePickerControllerEditedImage;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)fakeButton:(id)sender {
    [self snapPicture];
}

@end
