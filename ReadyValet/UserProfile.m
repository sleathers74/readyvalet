//
//  UserProfile.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/8/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "UserProfile.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "ValetStorage.h"
#import "Driver.h"
#import "AccountProfile.h"
#import "SystemPrefs.h"

@interface UserProfile ()

@end

@implementation UserProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:TRUE];
    
    self.valet_owner = [[ValetStorage sharedModel].role isEqualToString:@"valet_owner"];
    
    [self.phoneNumberText.formatter setDefaultOutputPattern:@"(###) ###-####"];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    [UtilFunctions addLeftPrefix:self.firstTextfield prefix:@"First"];
    [UtilFunctions addLeftPrefix:self.lastTextfield prefix:@"Last"];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"CANCEL" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SAVE" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    self.usernameTextfield.text = username;
    self.passwordTextfield.text = password;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.firstTextfield.leftView = paddingView;
    self.firstTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView = nil;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.lastTextfield.leftView = paddingView1;
    self.lastTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView1 = nil;
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.usernameTextfield.leftView = paddingView2;
    self.usernameTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView2 = nil;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.passwordTextfield.leftView = paddingView3;
    self.passwordTextfield.leftViewMode = UITextFieldViewModeAlways;
    paddingView3 = nil;
    
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.phoneNumberText.leftView = paddingView4;
    self.phoneNumberText.leftViewMode = UITextFieldViewModeAlways;
    paddingView4 = nil;
    
    [self loadData];
}

-(void) loadData {
    if (self.valet_owner) {
        ValetOwner *v = [ValetStorage sharedModel].valetOwner;
        self.firstTextfield.text = v.first_name;
        self.lastTextfield.text = v.last_name;
        
        if (v.company_logo)
            self.userImage.image = v.company_logo;
        if (v.phone_number)
            self.phoneNumberText.text = v.phone_number;
    }
    else {
        Driver *d = [ValetStorage sharedModel].currentDriver;
        if (d) {
            self.firstTextfield.text = d.firstName;
            self.lastTextfield.text = d.lastName;
            
            if (d.driverImage)
                self.userImage.image = d.driverImage;
            if (d.phone)
                self.phoneNumberText.text = d.phone;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE]; 
}

-(void)save {
    [self.waitView setHidden:FALSE];
    if ([UtilFunctions NSStringIsValidEmail:self.usernameTextfield.text]) {
        [self validateUsername];
    }
    else {
        [self.waitView setHidden:TRUE];
        [UtilFunctions displayError:self msg:@"Invalid email address."];
    }
}

-(void) processFinal {
    if ([self.usernameTextfield.text length] <= 0) {
        NSString *msg = @"Username must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.passwordTextfield.text length] <= 0) {
        NSString *msg = @"Password must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.firstTextfield.text length] <= 0) {
        NSString *msg = @"First name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.lastTextfield.text length] <= 0) {
        NSString *msg = @"Last name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    
    if (self.valet_owner) {
        [self updateOwner];
    }
    else {
        [self updateDriver];
    }
}

- (IBAction)touchPhoto:(id)sender {
    [self displayChooserActionSheeet];
}

-(void) updateOwner {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     username, @"username",
                                     password, @"password",
                                     self.firstTextfield.text, @"first_name",
                                     self.lastTextfield.text, @"last_name",
                                     self.usernameTextfield.text, @"newusername",
                                     self.passwordTextfield.text, @"newpassword",
                                     nil];
    
    if ([self.phoneNumberText.text length] > 0) {
        [postData setObject:self.phoneNumberText.text forKey:@"phone"];
    }
    
    if (self.userImage.image) {
        NSData *imageData = UIImageJPEGRepresentation(self.userImage.image, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"company_logo"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@updatevaletowner/%@", [SystemPrefs instance].getURL, [ValetStorage sharedModel].valetOwner.owner_id];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
        NSLog(@"JSON: %@", responseObject);
#endif
        
        [self.waitView setHidden:TRUE];
        
        ValetOwner *v = [ValetStorage sharedModel].valetOwner;
        v.first_name = self.firstTextfield.text;
        v.last_name = self.lastTextfield.text;
        
        if (self.userImage.image)
            v.company_logo = self.userImage.image;

        if ([self.phoneNumberText.text length] > 0)
            v.phone_number = self.phoneNumberText.text;
        
        [[NSUserDefaults standardUserDefaults] setObject:self.usernameTextfield.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:self.passwordTextfield.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"profileUpdated" object:nil];
        
        NSString *msg = @"Your account has been updated.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Account Update" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.delegate = self;
        alert.tag = 10;
        [alert show];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.waitView setHidden:TRUE];
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

-(void) updateDriver {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];
    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     username, @"username",
                                     password, @"password",
                                     self.firstTextfield.text, @"first_name",
                                     self.lastTextfield.text, @"last_name",
                                     self.usernameTextfield.text, @"newusername",
                                     self.passwordTextfield.text, @"newpassword",
                                     nil];
    
    if ([self.phoneNumberText.text length] > 0) {
        [postData setObject:self.phoneNumberText.text forKey:@"phone"];
    }
    
    if (self.userImage.image) {
        NSData *imageData = UIImageJPEGRepresentation(self.userImage.image, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"driverImage"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@updatedriver", [SystemPrefs instance].getURL];
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
        #ifdef DEBUG
            NSLog(@"JSON: %@", responseObject);
        #endif
        
        [self.waitView setHidden:TRUE];
        
        Driver *d = [ValetStorage sharedModel].currentDriver;
        d.firstName = self.firstTextfield.text;
        d.lastName = self.lastTextfield.text;
        d.emailAddress = self.usernameTextfield.text;
        d.password = self.passwordTextfield.text;
        d.phone = self.phoneNumberText.text;
        if (self.userImage.image)
            d.driverImage = self.userImage.image;
        
        [[NSUserDefaults standardUserDefaults] setObject:self.usernameTextfield.text forKey:@"username"];
        [[NSUserDefaults standardUserDefaults] setObject:self.passwordTextfield.text forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [ValetStorage sharedModel].currentDriver = d;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"profileUpdated" object:nil];
        
        NSString *msg = @"Your account has been updated.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Account Update" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.delegate = self;
        alert.tag = 10;
        [alert show];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.waitView setHidden:TRUE];
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

- (IBAction)touchSignout:(id)sender {
    NSString *title = @"SIGN OUT";
    NSString *header = @"Are you sure you want to sign out?";
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:header
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:title, nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Account Updated
    if (alertView.tag == 10) {
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

-(bool) validateUsername {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    
    if (![username isEqualToString:self.usernameTextfield.text]) {
        self.apiManager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         self.usernameTextfield.text, @"username", nil];
        NSString *urlString = [NSString stringWithFormat:@"%@/checkUsername", [[SystemPrefs instance] full_url]];
        [self.apiManager POST:urlString parameters:postData success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"JSON: %@", responseObject);
            //[self.waitView setHidden:TRUE];
            if ([[responseObject objectForKey:@"username_exists"] intValue] == 1) {
                NSString *msg = @"The email address you have chosen already exists in our system. Please choose a different email address.";
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
                return;
            }
            [self processFinal];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            [self.waitView setHidden:TRUE];
            
            [UtilFunctions displayError:self msg:error.localizedDescription];
        }];
    }
    else {
        [self processFinal];
    }
    return true;
}

// Photo Chooser
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.navigationItem.rightBarButtonItem.title= @"SAVE";
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (chosenImage == nil)
        chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.userImage.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void) displayChooserActionSheeet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Photo Selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        [actionSheet addButtonWithTitle:@"Camera"];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        [actionSheet addButtonWithTitle:@"Photo Library"];
    
    actionSheet.tag = 10;
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Camera
    if (actionSheet.tag == 10) {
        UIImagePickerControllerSourceType srcType;
        
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Camera"]) {
            srcType = UIImagePickerControllerSourceTypeCamera;
        }
        else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Photo Library"]) {
            srcType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self openPhotoPicker:srcType];
        }];
    }
    else {
        if (buttonIndex == 0) { // signout
            [ValetStorage sharedModel].currentDriver = nil;
            [ValetStorage sharedModel].role = nil;
            [ValetStorage sharedModel].password = nil;
            [[ValetStorage sharedModel] saveLogin];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"loggedin"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"role"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"username"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"password"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"logout" object:self];
        }

    }
}

-(void) openPhotoPicker:(UIImagePickerControllerSourceType) sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.mediaTypes = @[(NSString *)kUTTypeImage];
    picker.sourceType = sourceType;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


@end
