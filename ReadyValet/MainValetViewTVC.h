//
//  MainValetViewTVC.h
//  ReadyValet
//
//  Created by Scott Leathers on 7/29/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "AFNetworking.h"

@interface MainValetViewTVC : UITabBarController <UITabBarControllerDelegate,CBPeripheralManagerDelegate,CLLocationManagerDelegate> {
    
}

@property (strong, nonatomic) NSTimer *refreshTimer;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) NSDictionary *myBeaconData;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CBPeripheralManager *peripheralManager;
@property int lastCheckIn;

@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@end
