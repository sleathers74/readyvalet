//
//
//  Created by Scott Leathers on 11/7/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "ValetCompany.h"

#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"

@interface ValetUsersList : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate> {
}

@property (nonatomic, strong) ValetCompany  *valetCompany;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
@property (strong, nonatomic) IBOutlet UITableView *valetTable;
@property (nonatomic, strong) NSMutableArray *userArray;
@property (strong, nonatomic) IBOutlet UIView *waitView;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *editProfileBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *manageUsersBarButton;
@property (nonatomic, strong) NIKFontAwesomeIconFactory *factory;
@property (nonatomic, assign) NSInteger selectedCell;

@end
