//
//  UserProfile.m
//  ReadyValet
//
//  Created by Scott Leathers on 11/8/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "ValetCompanyProfile.h"
#import "UtilFunctions.h"
#import "AccountProfile.h"
#import "ValetStorage.h"
#import "Driver.h"
#import "AccountProfile.h"
#import "SystemPrefs.h"
#import "ValetOwner.h"
#import "BSKeyboardControls.h"
#import "TermsOfService.h"

@interface ValetCompanyProfile ()
@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end

@implementation ValetCompanyProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.waitView setHidden:TRUE];
    
    [self.phoneNumberText.formatter setDefaultOutputPattern:@"(###) ###-####"];
    [self.valetFeeTF.formatter setDefaultOutputPattern:@"$###"];
    [self.taxidTF.formatter setDefaultOutputPattern:@"##-#######"];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.titleView = [UtilFunctions buildCustomNav];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"BACK" style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"SAVE" style:UIBarButtonItemStylePlain target:self action:@selector(save)];
    
    self.scrollView.scrollEnabled = TRUE;
    self.scrollView.contentSize = CGSizeMake(308, 768);
    
    NSArray *fields = @[ self.legalCompanyName, self.dbaName, self.emailTextField, self.phoneNumberText, self.valetFeeTF, self.contactInfoTF, self.addressTF, self.cityTF, self.stateTF, self.zipTF, self.taxidTF, self.bankAccountTF, self.routingNumberTF, self.merchantidTF, self.merchantstatusTF ];
    
    long total = [fields count];
    for (int ct = 0; ct < total; ct++) {
        [self handlePadding:[fields objectAtIndex:ct]];
    }
    
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:fields]];
    [self.keyboardControls setDelegate:self];
    
    [self loadData];
}

-(void) handlePadding:(UITextField*) tf {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    tf.leftView = paddingView;
    tf.leftViewMode = UITextFieldViewModeAlways;
    paddingView = nil;
}

-(void) loadData {
    if (self.valetCompany) {
        self.legalCompanyName.text = self.valetCompany.company;
        self.dbaName.text = self.valetCompany.dbaName;
        self.emailTextField.text = self.valetCompany.email;
        [self.valetFeeTF setFormattedText:self.valetCompany.valet_fee];
        self.contactInfoTF.text = self.valetCompany.contact_info;
        self.addressTF.text = self.valetCompany.address;
        self.cityTF.text = self.valetCompany.city;
        self.stateTF.text = self.valetCompany.state;
        self.zipTF.text = self.valetCompany.zip;
        [self.taxidTF setFormattedText:self.valetCompany.tax_id];
        self.bankAccountTF.text = self.valetCompany.account_number;
        self.routingNumberTF.text = self.valetCompany.routing_number;
        self.merchantidTF.text = self.valetCompany.merchant_id;
        self.merchantstatusTF.text = self.valetCompany.merchant_status;
        self.userImage.image = self.valetCompany.profile_image;
        
        [self.termsSwitch setOn:[self.valetCompany.terms isEqualToString:@"1"]];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.valetCompany.terms forKey:@"acceptedTerms"];
        [defaults synchronize];
    
        if ([self.valetCompany.phone length] > 0)
            [self.phoneNumberText setFormattedText:self.valetCompany.phone];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancel {
    [self.navigationController popViewControllerAnimated:TRUE]; 
}

-(void)save {
    [self.waitView setHidden:FALSE];
    if ([self.legalCompanyName.text length] <= 0) {
        NSString *msg = @"Legal Company Name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.dbaName.text length] <= 0) {
        NSString *msg = @"DBA Name must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    if ([self.emailTextField.text length] <= 0) {
        NSString *msg = @"Email must not be empty.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *acceptedTerms = [defaults valueForKey:@"acceptedTerms"];
    if (![acceptedTerms isEqualToString:@"1"]) {
        NSString *msg = @"You must view the terms of service to continue.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [self.waitView setHidden:TRUE];
        [alert show];
        return;
    }
    
    [self updateValetCompany];
}


- (IBAction)touchPhoto:(id)sender {
    [self displayChooserActionSheeet];
}

-(void) updateValetCompany {
    self.apiManager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *username = [defaults stringForKey:@"username"];
    NSString *password = [defaults stringForKey:@"password"];

    
    NSMutableDictionary *postData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     username, @"username",
                                     password, @"password",
                                     self.emailTextField.text, @"email",
                                     self.phoneNumberText.phoneNumber, @"phone",
                                     self.legalCompanyName.text, @"company",
                                     self.dbaName.text, @"dbaName",
                                     self.valetFeeTF.phoneNumber, @"fee",
                                     self.addressTF.text, @"address",
                                     self.cityTF.text, @"city",
                                     self.stateTF.text, @"state",
                                     self.zipTF.text, @"zip",
                                     self.contactInfoTF.text, @"contact_info",
                                     self.merchantidTF.text, @"merchant_id",
                                     self.bankAccountTF.text, @"account_number",
                                     self.routingNumberTF.text, @"routing_number",
                                     self.taxidTF.phoneNumber, @"tax_id",
                                     @"1", @"terms",
                                     nil];
    
    if (self.userImage.image) {
        NSData *imageData = UIImageJPEGRepresentation(self.userImage.image, 0.1);
        NSString *baseString = [imageData base64Encoding];
        [postData setObject:baseString forKey:@"profile_image"];
    }
    
    NSString *urlString;
    if (self.valetCompany) {
        urlString = [NSString stringWithFormat:@"%@updatevaletcompany/%@", [SystemPrefs instance].getURL,self.valetCompany.valet_id];
    }
    else {
        urlString = [NSString stringWithFormat:@"%@addvaletcompany/%@", [SystemPrefs instance].getURL, [ValetStorage sharedModel].valetOwner.owner_id];
    }
    
    [self.apiManager POST:urlString parameters:postData   success:^(AFHTTPRequestOperation *operation, id responseObject) {
#ifdef DEBUG
        NSLog(@"JSON: %@", responseObject);
#endif
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        alert.delegate = self;
        
        if ([[responseObject objectForKey:@"success"] boolValue] == TRUE) {
            [alert setTitle:@"Account Information"];
            [alert setMessage:[responseObject objectForKey:@"message"]];
            alert.tag = 100;
        }
        else {
            [alert setTitle:@"Account Information"];
            [alert setMessage:[responseObject objectForKey:@"message"]];
            alert.tag = 1;
        }
        
        [self.waitView setHidden:TRUE];
        
        [alert show];
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.waitView setHidden:TRUE];
        NSLog(@"Error: %@", error);
        [UtilFunctions displayError:self msg:error.localizedDescription];
    }];
}

- (void)alertView:(UIAlertView *)alert didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([alert tag] == 100) {
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

// Photo Chooser
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    self.navigationItem.rightBarButtonItem.title= @"SAVE";
    UIImage *chosenImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if (chosenImage == nil)
        chosenImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.userImage.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void) displayChooserActionSheeet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Photo Selection"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        [actionSheet addButtonWithTitle:@"Camera"];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        [actionSheet addButtonWithTitle:@"Photo Library"];
    
    actionSheet.tag = 0;
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerControllerSourceType srcType;
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Camera"]) {
        srcType = UIImagePickerControllerSourceTypeCamera;
    }
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Photo Library"]) {
        srcType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self openPhotoPicker:srcType];
    }];
}

-(void) openPhotoPicker:(UIImagePickerControllerSourceType) sourceType {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.mediaTypes = @[(NSString *)kUTTypeImage];
    picker.sourceType = sourceType;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

// Keyboard Controls
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.keyboardControls setActiveField:textField];
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [self.view endEditing:TRUE];
}

- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view = keyboardControls.activeField.superview.superview;
    [self.scrollView scrollRectToVisible:view.frame animated:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)termsTouched:(id)sender {
    TermsOfService *tos = [[TermsOfService alloc] initWithNibName:@"TermsOfService" bundle:nil];
    [self.navigationController pushViewController:tos animated:TRUE];
    [self.termsSwitch setOn:TRUE];
}

- (IBAction)termsValueChanged:(id)sender {
    TermsOfService *tos = [[TermsOfService alloc] initWithNibName:@"TermsOfService" bundle:nil];
    [self.navigationController pushViewController:tos animated:TRUE];
    [self.termsSwitch setOn:TRUE];
}

@end
