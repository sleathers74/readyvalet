//
//  ValetSelectionVC.h
//  ReadyValet
//
//  Created by Scott Leathers on 9/24/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"

@interface LicensePhoto : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    bool bPicture;
}
@property (strong, nonatomic) IBOutlet UIImageView *ticketImage;
@property (strong, nonatomic) AFHTTPRequestOperationManager *apiManager;
- (IBAction)fakeButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *photoButton;



@end
