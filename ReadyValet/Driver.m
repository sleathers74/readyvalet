//
//  Driver.m
//  Valet
//
//  Created by Matthew Schmidgall on 2/15/14.
//  Copyright (c) 2014 Matthew Schmidgall. All rights reserved.
//

#import "Driver.h"

@implementation Driver

- (id)init
{
    self = [super init];
    if (self) {
        self.vehicles = [[NSMutableArray alloc] init];
        self.driverImage = nil;
    }
    return self;
}

@end
