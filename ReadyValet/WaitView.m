//
//  WaitView.m
//  ReadyValet
//
//  Created by Scott Leathers on 7/21/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import "WaitView.h"

@interface WaitView ()

@end

@implementation WaitView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
