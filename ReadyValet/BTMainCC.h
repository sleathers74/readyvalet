//
//  BTMainCC.h
//  BraintreeTest
//
//  Created by Scott Leathers on 11/5/14.
//  Copyright (c) 2014 Scott Leathers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Braintree.h"
#import "BTUICardFormView.h"
#import "CardIOPaymentViewController.h"
#import "myPaymentMethodVC.h"

@interface BTMainCC : UIViewController <BTUICardFormViewDelegate, CardIOPaymentViewControllerDelegate> {
    
}
@property (weak, nonatomic) IBOutlet BTUICardFormView *cardFormView;

@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *cvv;
@property (strong, nonatomic) NSString *expirationMonth;
@property (strong, nonatomic) NSString *expirationYear;
@property (strong, nonatomic) NSString *postalCode;

@property (strong, nonatomic) NSString *nonce;
@property (strong, nonatomic) NSArray *paymentMethods;
- (IBAction)touchScanYourCard:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *waitView;


@end
